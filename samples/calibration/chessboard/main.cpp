#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * main.cpp created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * main.cpp is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

#include "gedeon/calibration/tools.hpp"
#include "gedeon/core/core.hpp"
#include <cstdlib>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace gedeon;

int main(int argc, char **argv) {

	if(2 != argc){
		Log::add().error("main","Please give an image as a parameter");
		return EXIT_FAILURE;
	}

	cv::Mat input = cv::imread(argv[1]);
	cv::Size pattern(4,7);
	std::vector<cv::Point2f> corners;
	bool found = Tools::OpenCV::detectChessboard(corners, input, pattern);

	if(true == found){

		cv::drawChessboardCorners(input, pattern, corners, found);

		bool running = true;

		while (true == running) {

			char k = cv::waitKey(40);
			if (k == 27 || k == 'q') {
				running = false;
			}

			cv::imshow("Detected chessboard",input);
		}

		cv::destroyAllWindows();
	}else{
		Log::add().error("main","Chessboard grid not detected");
	}

	return EXIT_SUCCESS;
}

