/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opennigrabber.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opennigrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/opennidriver.hpp"
#include "gedeon/grabber/opennigrabber.hpp"

#include <boost/algorithm/string/predicate.hpp>

#include <XnCppWrapper.h>

namespace gedeon {

class OpenNIGrabberParams {
public:
	OpenNIGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~OpenNIGrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;
	int principal_point_depth_x;
	int principal_point_depth_y;
	float focal_depth;

};

class OpenNIGrabberIO {
public:
	OpenNIGrabberIO(void) :
			local_streaming(false), stream_filename("openni-output.oni") {
	}

	~OpenNIGrabberIO(void) {
		player.Release();
		recorder.Release();
	}

	bool local_streaming;
	std::string stream_filename;

	xn::Recorder recorder;
	xn::Player player;
};

class OpenNIGrabberCapture {
public:
	OpenNIGrabberCapture(void) :
			devicename(""), device_id(0) {
	}
	~OpenNIGrabberCapture(void) {
		depth.Release();
		color.Release();
		ir.Release();
	}

	xn::DepthGenerator depth;
	xn::IRGenerator ir;
	xn::ImageGenerator color;
	xn::Context context;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

};

class OpenNIGrabberThread {
public:
	OpenNIGrabberThread(void) :
			pthread(0) {
	}
	~OpenNIGrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

OpenNIGrabber::OpenNIGrabber(void) :
		Grabber(), capturepimpl(new OpenNIGrabberCapture()), paramspimpl(
				new OpenNIGrabberParams()), threadpimpl(
				new OpenNIGrabberThread()), iopimpl(new OpenNIGrabberIO()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool OpenNIGrabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("OpenNIGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	OpenNIDriver *driver = 0;

	try {
		driver = dynamic_cast<OpenNIDriver *>(d);
	} catch (const std::bad_cast& e) {
		Log::add().error("OpenNIGrabber::init", "The driver is not compatible");
		return false;
	}

	this->capturepimpl->context = driver->getContext();

	if (0 != driver->getCount() && id < driver->getCount()) {

		/*
		 * Find the device in the list of devices
		 */
		XnStatus nRetVal = XN_STATUS_OK;
		unsigned int i = 0;
		bool created = false;
		xn::NodeInfoList list;
		xn::EnumerationErrors errors;
		nRetVal = this->capturepimpl->context.EnumerateProductionTrees(
				XN_NODE_TYPE_DEVICE, NULL, list, &errors);
		xn::Device device;
		for (xn::NodeInfoList::Iterator it = list.Begin();
				it != list.End() && created == false; ++it, ++i) {
			if (id == i) {
				this->capturepimpl->devicename = driver->populate()[i];
			}
			xn::NodeInfo deviceNodeInfo = *it;
			this->capturepimpl->context.CreateProductionTree(deviceNodeInfo,
					device);
			xn::Query query;
			query.AddNeededNode(deviceNodeInfo.GetInstanceName());
			nRetVal = this->capturepimpl->depth.Create(
					this->capturepimpl->context, &query);
			if (XN_STATUS_OK == nRetVal) {
				this->paramspimpl->hasdepth = true;
			}

			this->capturepimpl->rgbdi.reset(new RGBDIImage);

			/*
			 * Set to default resolution
			 *
			 */
			XnMapOutputMode outputMode;
			outputMode.nXRes = XN_VGA_X_RES;
			outputMode.nYRes = XN_VGA_Y_RES;
			outputMode.nFPS = 30;
			int width, height;
			this->paramspimpl->framerate = 0.0f;

			if (true == this->paramspimpl->hasdepth) {
				this->capturepimpl->depth.SetMapOutputMode(outputMode);
				xn::DepthMetaData depthmetadata;
				this->capturepimpl->depth.GetMetaData(depthmetadata);
				width = depthmetadata.FullXRes();
				height = depthmetadata.FullYRes();
				this->capturepimpl->rgbdi.get()->depth.setSize(
						Size(width, height));
				this->paramspimpl->framerate = depthmetadata.FPS();
			}

			if (true == this->paramspimpl->hascolor) {
				this->paramspimpl->hasintensity = false;
			}

			setColorMode();

			/*
			 * Set the viewpoint to the color image
			 */
			if (true == this->paramspimpl->hascolor
					&& true == this->paramspimpl->hasdepth) {
				if (this->capturepimpl->depth.IsCapabilitySupported(
						XN_CAPABILITY_ALTERNATIVE_VIEW_POINT)) {
					this->capturepimpl->depth.GetAlternativeViewPointCap().SetViewPoint(
							this->capturepimpl->color);
				}

				this->capturepimpl->color.GetAlternativeViewPointCap().SetViewPoint(
						this->capturepimpl->color);
			}

			Size s = this->capturepimpl->rgbdi.get()->depth.getSize();
			this->paramspimpl->principal_point_depth_x = s.width / 2;
			this->paramspimpl->principal_point_depth_y = s.height / 2;

			XnUInt64 F;
			XnDouble pixel_size;
			// get the focal length in mm (ZPD = zero plane distance)
			this->capturepimpl->depth.GetIntProperty("ZPD", F);
			this->capturepimpl->depth.GetRealProperty("ZPPS", pixel_size);

			pixel_size *= 2.0;
			this->paramspimpl->focal_depth = static_cast<float>(F) / pixel_size;
			created = true;
		}
		if (created == false) {
			Log::add().error("OpenNIGrabber::init",
					"Failed finding the openni device "
							+ IO::numberToString(id));
			return false;
		}
		this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		this->paramspimpl->initialized = true;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("OpenNIGrabber::init", "No device found");
	return false;
}

bool OpenNIGrabber::init(const std::string& source, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("DepthSenseGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->paramspimpl->hascolor = false;
	this->paramspimpl->hasdepth = false;
	this->paramspimpl->hasintensity = false;

	this->capturepimpl->rgbdi.reset(new RGBDIImage);

	std::string namelower = source;
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (true == boost::algorithm::ends_with(namelower, ".oni")) {
		OpenNIDriver *driver = 0;
		try {
			driver = dynamic_cast<OpenNIDriver *>(d);
		} catch (const std::bad_cast& e) {
			Log::add().error("OpenNIGrabber::init",
					"The driver is not compatible");
			return false;
		}
		this->capturepimpl->context = driver->getContext();

		if (XN_STATUS_OK
				!= this->capturepimpl->context.OpenFileRecording(source.c_str(),
						this->iopimpl->player)) {
			Log::add().error("OpenNIGrabber::init",
					"Unable to create the player");
			return false;
		}

		xn::NodeInfoList list;
		if (XN_STATUS_OK != this->iopimpl->player.EnumerateNodes(list)) {
			Log::add().error("OpenNIGrabber::init",
					"Unable to get player's information");
			return false;
		}
		if (true == list.IsEmpty()) {
			Log::add().error("OpenNIGrabber::init", "The player is empty");
			return false;
		}

		for (xn::NodeInfoList::Iterator it = list.Begin(); it != list.End();
				++it) {

			if (XN_NODE_TYPE_DEPTH == (*it).GetDescription().Type) {
				(*it).GetInstance(this->capturepimpl->depth);
				this->paramspimpl->hasdepth = true;
			}
			if (XN_NODE_TYPE_IMAGE == (*it).GetDescription().Type) {
				(*it).GetInstance(this->capturepimpl->color);
				this->paramspimpl->hascolor = true;
			}
			if (XN_NODE_TYPE_IR == (*it).GetDescription().Type) {
				(*it).GetInstance(this->capturepimpl->ir);
				this->paramspimpl->hasintensity = true;
			}
		}

		if (true == this->paramspimpl->hasdepth) {
			xn::DepthMetaData depthmetadata;
			this->capturepimpl->depth.GetMetaData(depthmetadata);
			unsigned int width = depthmetadata.FullXRes();
			unsigned int height = depthmetadata.FullYRes();
			this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));
			this->paramspimpl->framerate = depthmetadata.FPS();
		}

		if (true == this->paramspimpl->hascolor) {
			xn::ImageMetaData colormetadata;
			this->capturepimpl->color.GetMetaData(colormetadata);
			unsigned int width = colormetadata.FullXRes();
			unsigned int height = colormetadata.FullYRes();
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
			if (0.0f == this->paramspimpl->framerate) {
				this->paramspimpl->framerate = colormetadata.FPS();
			}
		}

		if (true == this->paramspimpl->hasintensity) {
			xn::IRMetaData irmetadata;
			this->capturepimpl->ir.GetMetaData(irmetadata);
			unsigned int width = irmetadata.FullXRes();
			unsigned int height = irmetadata.FullYRes();
			this->capturepimpl->rgbdi.get()->intensity.setSize(
					Size(width, height));
			if (0.0f == this->paramspimpl->framerate) {
				this->paramspimpl->framerate = irmetadata.FPS();
			}
		}

		//Set the viewpoint to the color image

		if (true == this->paramspimpl->hascolor
				&& true == this->paramspimpl->hasdepth) {
			if (this->capturepimpl->depth.IsCapabilitySupported(
					XN_CAPABILITY_ALTERNATIVE_VIEW_POINT)) {
				this->capturepimpl->depth.GetAlternativeViewPointCap().SetViewPoint(
						this->capturepimpl->color);
			}

			this->capturepimpl->color.GetAlternativeViewPointCap().SetViewPoint(
					this->capturepimpl->color);
		}

		Size s = this->capturepimpl->rgbdi.get()->depth.getSize();
		this->paramspimpl->principal_point_depth_x = s.width / 2;
		this->paramspimpl->principal_point_depth_y = s.height / 2;

		XnUInt64 F;
		XnDouble pixel_size;
		// get the focal length in mm (ZPD = zero plane distance)
		this->capturepimpl->depth.GetIntProperty("ZPD", F);
		this->capturepimpl->depth.GetRealProperty("ZPPS", pixel_size);

		pixel_size *= 2.0;
		this->paramspimpl->focal_depth = static_cast<float>(F) / pixel_size;

	} else {
		Log::add().warning("OpenNIGrabber::init", "Format not supported");
		return false;
	}

	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
	this->iopimpl->local_streaming = true;

	this->capturepimpl->devicename = std::string("File (" + source + ")");

	this->threadpimpl->updated->sender = this;
	this->threadpimpl->ready->sender = this;
	this->threadpimpl->stopped->sender = this;
	this->threadpimpl->playing->sender = this;
	this->threadpimpl->paused->sender = this;
	this->paramspimpl->initialized = true;
	emitEvent(this->threadpimpl->ready);

	return true;
}

OpenNIGrabber::~OpenNIGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
	this->iopimpl->recorder.RemoveNodeFromRecording(this->capturepimpl->depth);
	this->iopimpl->recorder.RemoveNodeFromRecording(this->capturepimpl->ir);
	this->iopimpl->recorder.RemoveNodeFromRecording(this->capturepimpl->color);

	delete paramspimpl;
	delete threadpimpl;
	delete iopimpl;
	delete capturepimpl;

}

std::string OpenNIGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int OpenNIGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& OpenNIGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool OpenNIGrabber::getParameter(const GrabberParameter& param, float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (OPENNI_FRAMERATE == param) {
		value = this->paramspimpl->framerate;
		return true;
	}

	if (OPENNI_FOCAL_DEPTH == param) {
		value = this->paramspimpl->focal_depth;
		return true;
	}

	if (OPENNI_PP_X_DEPTH == param) {
		value = this->paramspimpl->principal_point_depth_x;
		return true;
	}

	if (OPENNI_PP_Y_DEPTH == param) {
		value = this->paramspimpl->principal_point_depth_y;
		return true;
	}

	return false;
}

bool OpenNIGrabber::getParameter(const GrabberParameter& param, float* values,
		unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool OpenNIGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool OpenNIGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (this->iopimpl->local_streaming == false) {
		if (OPENNI_GRAB_INTENSITY == trig) {
			return setIntensityMode();
		}

		if (OPENNI_GRAB_COLOR == trig) {
			return setColorMode();
		}
	}

	if (OPENNI_INTERNAL_RECORD == trig) {
		if (false == this->iopimpl->recorder.IsValid()) {
			this->iopimpl->recorder.Create(this->capturepimpl->context);
			std::string namelower = this->iopimpl->stream_filename.c_str();
			std::transform(namelower.begin(), namelower.end(),
					namelower.begin(), ::tolower);
			if (true == boost::algorithm::ends_with(namelower, ".oni")) {
				this->iopimpl->recorder.SetDestination(XN_RECORD_MEDIUM_FILE,
						this->iopimpl->stream_filename.c_str());
			} else {
				this->iopimpl->recorder.SetDestination(XN_RECORD_MEDIUM_FILE,
						(this->iopimpl->stream_filename + ".oni").c_str());
			}
			this->iopimpl->recorder.AddNodeToRecording(
					this->capturepimpl->depth);
			this->iopimpl->recorder.AddNodeToRecording(this->capturepimpl->ir);
			this->iopimpl->recorder.AddNodeToRecording(
					this->capturepimpl->color);

		} else {
			this->iopimpl->recorder.RemoveNodeFromRecording(
					this->capturepimpl->depth);
			this->iopimpl->recorder.RemoveNodeFromRecording(
					this->capturepimpl->ir);
			this->iopimpl->recorder.RemoveNodeFromRecording(
					this->capturepimpl->color);
			this->iopimpl->recorder.Release();

		}
		return true;
	}

	return false;
}

bool OpenNIGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool OpenNIGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool OpenNIGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool OpenNIGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void OpenNIGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void OpenNIGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&OpenNIGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void OpenNIGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		this->iopimpl->recorder.RemoveNodeFromRecording(
				this->capturepimpl->depth);
		this->iopimpl->recorder.RemoveNodeFromRecording(this->capturepimpl->ir);
		this->iopimpl->recorder.RemoveNodeFromRecording(
				this->capturepimpl->color);
		this->iopimpl->recorder.Release();
		emitEvent(this->threadpimpl->stopped);
	}
}

void OpenNIGrabber::grabOneFrame(void) {
	Log::add().warning("OpenNIGrabber::grabOneFrame", "Not available");
}

void OpenNIGrabber::run(void) {
	XnStatus rc;

	if (true == this->paramspimpl->hascolor) {
		this->capturepimpl->color.StartGenerating();
	}
	if (true == this->paramspimpl->hasdepth) {
		this->capturepimpl->depth.StartGenerating();
	}
	if (true == this->paramspimpl->hasintensity) {
		this->capturepimpl->ir.StartGenerating();
	}

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();

		if (this->iopimpl->player.IsValid()) {
			this->iopimpl->player.ReadNext();
		}

		if (false == this->paramspimpl->paused) {
			if (true == this->paramspimpl->hascolor) {
				if (this->capturepimpl->color.IsNewDataAvailable()) {
					this->capturepimpl->color.WaitAndUpdateData();
					boost::mutex::scoped_lock l(this->threadpimpl->mutex);
					this->capturepimpl->rgbdi.get()->color.setData(
							this->capturepimpl->color.GetImageMap());
				}
			}

			if (true == this->paramspimpl->hasintensity) {
				if (this->capturepimpl->ir.IsNewDataAvailable()) {
					this->capturepimpl->ir.WaitAndUpdateData();

					unsigned char* ref_dst =
							this->capturepimpl->rgbdi.get()->intensity.getData().get();
					unsigned int s =
							this->capturepimpl->rgbdi.get()->intensity.getSize().height
									* this->capturepimpl->rgbdi.get()->intensity.getSize().width;
					const XnIRPixel * ref_src =
							this->capturepimpl->ir.GetIRMap();
					boost::mutex::scoped_lock l(this->threadpimpl->mutex);
					for (unsigned int i = 0; i < s; ++i) {
						ref_dst[i] = static_cast<unsigned char>(ref_src[i]
								* 0.2491234275);
					}
				}
			}
			if (true == this->paramspimpl->hasdepth) {
				if (this->capturepimpl->depth.IsNewDataAvailable()) {
					this->capturepimpl->depth.WaitAndUpdateData();

					const XnDepthPixel * ref_src =
							this->capturepimpl->depth.GetDepthMap();
					unsigned int s =
							this->capturepimpl->rgbdi.get()->depth.getSize().height
									* this->capturepimpl->rgbdi.get()->depth.getSize().width;
					float* ref_dst =
							this->capturepimpl->rgbdi->depth.getData().get();

					boost::mutex::scoped_lock l(this->threadpimpl->mutex);
					for (unsigned int i = 0; i < s; ++i) {
						ref_dst[i] = static_cast<float>(ref_src[i] * 0.001f);
					}
				}
			}

			if (true == this->iopimpl->recorder.IsValid()) {
				this->iopimpl->recorder.Record();
			}

			/*
			 * Emit a message to confirm the update
			 */
			emitEvent(this->threadpimpl->updated);
		}
		current_time = (1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	if (false == this->iopimpl->player.IsValid()) {
		if (true == this->paramspimpl->hascolor) {
			rc = this->capturepimpl->color.StopGenerating();
			if (rc != XN_STATUS_OK) {
				Log::add().error("OpenNISensor::run",
						"Error while stopping color generator: "
								+ std::string(xnGetStatusString(rc)));
			}
		}
		if (true == this->paramspimpl->hasintensity) {
			rc = this->capturepimpl->ir.StopGenerating();
			if (rc != XN_STATUS_OK) {
				Log::add().error("OpenNISensor::run",
						"Error while stopping ir generator: "
								+ std::string(xnGetStatusString(rc)));
			}
		}
		if (true == this->paramspimpl->hasdepth) {
			rc = this->capturepimpl->depth.StopGenerating();
			if (rc != XN_STATUS_OK) {
				Log::add().error("OpenNISensor::run",
						"Error while stopping depth generator: "
								+ std::string(xnGetStatusString(rc)));
			}
		}
	}

	timer.stop();
	this->paramspimpl->finished = true;

}

bool OpenNIGrabber::setIntensityMode(void) {
	if (true == this->paramspimpl->hasintensity) {
		return true;
	}
	xn::Query query;
	XnStatus nRetVal = this->capturepimpl->ir.Create(
			this->capturepimpl->context, &query);
	if (XN_STATUS_OK == nRetVal) {

		boost::mutex::scoped_lock l(this->threadpimpl->mutex);
		if (this->paramspimpl->hascolor == true) {
			this->paramspimpl->hascolor = false;
			this->capturepimpl->color.StopGenerating();
			this->capturepimpl->color.Release();
			this->capturepimpl->rgbdi.get()->color.release();
			millisecSleep(20);
		}

		XnMapOutputMode outputMode;
		outputMode.nXRes = XN_VGA_X_RES;
		outputMode.nYRes = XN_VGA_Y_RES;
		outputMode.nFPS = 30;
		this->capturepimpl->ir.SetMapOutputMode(outputMode);
		xn::IRMetaData irmetadata;
		this->capturepimpl->ir.GetMetaData(irmetadata);
		unsigned int width = irmetadata.FullXRes();
		unsigned int height = irmetadata.FullYRes();
		this->capturepimpl->rgbdi.get()->intensity.setSize(Size(width, height));

		if (true == this->paramspimpl->running) {
			nRetVal = this->capturepimpl->ir.StartGenerating();
			if (XN_STATUS_OK != nRetVal) {
				Log::add().error("OpenNIGrabber::setIntensityMode",
						std::string("Cannot start generating ")
								+ xnGetStatusString(nRetVal));
				return false;
			}
		}
		this->paramspimpl->hasintensity = true;

		return true;
	}
	Log::add().error("OpenNIGrabber::setIntensityMode",
			std::string("Cannot create ") + xnGetStatusString(nRetVal));
	return false;
}

bool OpenNIGrabber::setColorMode(void) {
	if (true == this->paramspimpl->hascolor) {
		return true;
	}
	xn::Query query;
	XnStatus nRetVal = this->capturepimpl->color.Create(
			this->capturepimpl->context, &query);
	if (XN_STATUS_OK == nRetVal) {

		if (this->paramspimpl->hasintensity == true) {
			this->paramspimpl->hasintensity = false;
			this->capturepimpl->ir.StopGenerating();
			this->capturepimpl->ir.Release();
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			this->capturepimpl->rgbdi.get()->intensity.release();
			millisecSleep(20);
		}

		XnMapOutputMode outputMode;
		outputMode.nXRes = XN_VGA_X_RES;
		outputMode.nYRes = XN_VGA_Y_RES;
		outputMode.nFPS = 30;
		this->capturepimpl->color.SetMapOutputMode(outputMode);
		this->capturepimpl->color.GetAlternativeViewPointCap().SetViewPoint(
				this->capturepimpl->color);
		xn::ImageMetaData colormetadata;
		this->capturepimpl->color.GetMetaData(colormetadata);
		unsigned int width = colormetadata.FullXRes();
		unsigned int height = colormetadata.FullYRes();
		this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));

		if (true == this->paramspimpl->running) {
			nRetVal = this->capturepimpl->color.StartGenerating();
			if (XN_STATUS_OK != nRetVal) {
				Log::add().error("OpenNIGrabber::setColorMode",
						std::string("Cannot start generating ")
								+ xnGetStatusString(nRetVal));
				return false;
			}
		}
		this->paramspimpl->hascolor = true;

		return true;
	}
	Log::add().error("OpenNIGrabber::setColorMode",
			std::string("Cannot create ") + xnGetStatusString(nRetVal));
	return false;
}

void OpenNIGrabber::setStreamFilename(const std::string& filename) {
	this->iopimpl->stream_filename = filename;
}

std::string OpenNIGrabber::getStreamFilename(void) const {
	return this->iopimpl->stream_filename;
}

}
