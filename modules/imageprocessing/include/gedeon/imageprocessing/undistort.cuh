/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * undistort.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * undistort.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__UNDISTORT_CUH__
#define GEDEON_IMAGE_PROCESSING__UNDISTORT_CUH__

#include "gedeon/datatypes/size.hpp"

namespace gedeon {

namespace GPU {

namespace Undistort {

/**
 * \brief Copy the undistortion map onto the device
 *
 * \param d_undistort_map The output undistoted map on the device with the data from the host
 * \param input_undistort_map The input distortion map on the host
 * \param s the size of the map
 *
 * \return true if everything went right, false otherwise
 *
 * \author Francois de Sorbier
 */
bool updateUndistortMap(float** d_undistort_map, const float* input_undistort_map, const Size& s);

/**
 * \brief Copy the bilinear interpolation map onto the device
 *
 * \param d_bilinear_map The output bilinear interpolation map on the device with the data from the host
 * \param input_bilinear_map The input bilinear interpolation map on the host
 * \param s the size of the map
 *
 * \return true if everything went right, false otherwise
 *
 * \author Francois de Sorbier
 */
bool updateBilinearInterpolationMap(float** d_bilinear_map, const float* input_bilinear_map, const Size& s);

/**
 * \brief Apply the undistortion map
 *
 * \param d_output The undistorted input on the device
 * \param d_input The input RGB image that will be undistorted on the device
 * \param d_undistort_map The map for the undistortion on the device
 * \param d_width the width of the image
 * \param d_height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 *
 * The undistort map should be of the same size than the input image
 *
 * \sa computeDistortionMap
 * \sa undistortBilinear
 *
 * \return true if everything went right, false otherwise
 *
 * \author Francois de Sorbier
 */
bool undistort(unsigned char *d_output, const unsigned char *d_input, const float* d_undistort_map,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y);

/**
 * \brief Apply the undistortion map with bilinear interpolation
 *
 * \param d_output The undistorted input on the device
 * \param d_input The input RGB image that will be undistorted on the device
 * \param d_undistort_map The map for the undistortion on the device
 * \param d_bilinear_map The map for the bilinear interpolation on the device
 * \param d_width the width of the image
 * \param d_height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 *
 * The undistort map should be of the same size than the input image
 *
 * \sa computeDistortionMap
 * \sa computeBilinearInterpolation
 * \sa undistort
 *
 * \return true if everything went right, false otherwise
 *
 * \author Francois de Sorbier
 */
bool undistortBilinear(unsigned char *d_output, const unsigned char *d_input,
		const float* d_undistort_map, const float* d_bilinear_map,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y);

}

}

}

#endif
