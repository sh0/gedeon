/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opencvdriver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opencvdriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/opencvdriver.hpp"
#define HAVE_CMU1394 1
#include <opencv2/highgui/highgui.hpp>

namespace gedeon {

OpenCVDriver OpenCVDriver::driver_singleton;

OpenCVDriver::OpenCVDriver(void) :
		initialized(false) {
}

OpenCVDriver::~OpenCVDriver(void) {

	this->names.clear();

}

bool OpenCVDriver::refresh(void) {

	this->initialized = false;

	this->names.clear();

	/*
	 * Try to open the cameras with an id
	 */
	int id = 0;
	bool found = false;
	const int max_cam = 5;
	cv::VideoCapture vcarray[max_cam];
	do{
		found = false;

		if(false == vcarray[id].isOpened() && true == vcarray[id].open(id)){
			
			cv::Mat img;
			vcarray[id] >> img;

			if(true == img.empty() || img.size().width == 0 || img.size().height == 0){
				found = false;
			}else{
				found = true;
			}
			
			if(true == found){
				this->names.push_back(std::string("ID ") + IO::numberToString(id));
				++id;
			}
		}
	}while(true == found && id < max_cam);

	for(int i = 0; i < id; ++i){
		 vcarray[i].release();
	}

	this->initialized = true;

	return true;
}

}
