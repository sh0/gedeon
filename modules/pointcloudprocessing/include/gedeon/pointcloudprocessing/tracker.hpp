/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tracker.hpp created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tracker.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_POINTCLOUDPROCESSING_TRACKER_HPP__
#define GEDEON_POINTCLOUDPROCESSING_TRACKER_HPP__

#include <opencv2/core/core.hpp>

namespace gedeon {

namespace GPU {

class Tracker{

public:

	Tracker(const unsigned int& width, const unsigned int& height, float intrinsic[9]);

	~Tracker();

	bool estimatePose(float* d_depthmap);

private:

	float *d_rotation;
	float *d_translation;
	float **d_intrinsic;
	float **intrinsic;

	cv::Mat rotation;
	cv::Mat translation;

	cv::Mat rotation_inc;
	cv::Mat translation_inc;

	cv::Mat res;
	cv::Mat Amat;
	cv::Mat bmat;

	bool firstframe;

	int width;
	int height;

	float **d_p_vertexmaps;
	float **d_p_normalmaps;
	float **d_c_vertexmaps;
	float **d_c_normalmaps;
	float **d_depthmaps;

};

namespace Calibration3D3D {

bool performICP(float* output_rotation, float* output_translation, float* d_previousvertexmap,
		float* d_previousnormalmap, float* d_inputvertexmap, float* d_inputnormalmap,
		float *intrinsic, const int& width, const int& height,
		const unsigned int& block_x, const unsigned int& block_y);


}

}

}

#endif
