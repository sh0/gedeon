#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 05 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * This file is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The HVRL Engine Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/


SET(localname core)

PROJECT(${PROJECT_NAME}_${localname})

SET(the_target "${PROJECT_NAME_SMALL}_${localname}")

INCLUDE_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/include")
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
	
SET(local_include_dir "${GEDEON_SOURCE_DIR}/modules/${localname}/include/${PROJECT_NAME_SMALL}/${localname}")

CONFIGURE_FILE(${local_include_dir}/config.hpp.in ${local_include_dir}/config.hpp)

SET(lib_srcs src/log.cpp src/exception.cpp src/event.cpp src/converter.cpp)
SET(lib_hdrs ${local_include_dir}/log.hpp ${local_include_dir}/exception.hpp 
	     ${local_include_dir}/event.hpp ${local_include_dir}/io.hpp 
	     ${local_include_dir}/maths.hpp ${local_include_dir}/time.hpp
	     ${local_include_dir}/core.hpp ${local_include_dir}/converter.hpp 
	     ${local_include_dir}/config.hpp)

IF(GEDEON_BUILD_WITH_CUDA)

	INCLUDE_DIRECTORIES(${CUDA_CUT_INCLUDE_DIR})
	CUDA_INCLUDE_DIRECTORIES(${CUDA_INCLUDE_DIRS})
	
	SET(lib_srcs_cuda src/maths.cu src/tools.cu)
	SET(lib_hdrs_cuda ${local_include_dir}/maths.cuh ${local_include_dir}/tools.cuh ${local_include_dir}/core.cuh)
	SET(lib_hdrs ${lib_hdrs} ${local_include_dir}/cudaio.hpp)
	
	if (UNIX OR APPLE)
	    set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS}  "-Xcompiler;-fPIC;")
	endif()
	
	IF(GEDEON_BUILD_IN_DEBUG_MODE)
		set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "-g;-G;")
	ENDIF(GEDEON_BUILD_IN_DEBUG_MODE)
	
	set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "-arch;sm_20")
ENDIF(GEDEON_BUILD_WITH_CUDA)


IF(lib_srcs OR lib_srcs_cuda)

	SOURCE_GROUP("sources" FILES ${lib_srcs} ${lib_srcs_cuda})
	
	IF(GEDEON_BUILD_WITH_CUDA)
		IF(GEDEON_BUILD_SHARED_LIBS)
			CUDA_ADD_LIBRARY(${the_target} ${lib_srcs} ${lib_hdrs} ${lib_srcs_cuda} ${lib_hdrs_cuda})
		ELSE(GEDEON_BUILD_SHARED_LIBS)
			CUDA_ADD_LIBRARY(${the_target} STATIC ${lib_srcs} ${lib_hdrs} ${lib_srcs_cuda} ${lib_hdrs_cuda})
		ENDIF(GEDEON_BUILD_SHARED_LIBS)
	ELSE(GEDEON_BUILD_WITH_CUDA)
		IF(GEDEON_BUILD_SHARED_LIBS)
			ADD_LIBRARY(${the_target} SHARED ${lib_srcs} ${lib_hdrs})
		ELSE(GEDEON_BUILD_SHARED_LIBS)
			ADD_LIBRARY(${the_target} STATIC ${lib_srcs} ${lib_hdrs})
		ENDIF(GEDEON_BUILD_SHARED_LIBS)
	ENDIF(GEDEON_BUILD_WITH_CUDA)
	
	IF(GEDEON_BUILD_WITH_CUDA)
		TARGET_LINK_LIBRARIES(${the_target} ${CUDA_LIBRARIES} ${CUDA_CUT_LIBRARIES})
	ELSE(GEDEON_BUILD_WITH_CUDA)
		TARGET_LINK_LIBRARIES(${the_target})
	ENDIF(GEDEON_BUILD_WITH_CUDA)
	
	INSTALL(TARGETS ${the_target}
	        RUNTIME DESTINATION ${GEDEON_BIN_INSTALL_PATH}
	        LIBRARY DESTINATION ${GEDEON_LIB_INSTALL_PATH}
	        ARCHIVE DESTINATION ${GEDEON_LIB_INSTALL_PATH})

ENDIF(lib_srcs OR lib_srcs_cuda)

IF(lib_hdrs OR lib_hdrs_cuda)
	SOURCE_GROUP("headers" FILES ${lib_hdrs} ${lib_hdrs_cuda})
	IF(GEDEON_BUILD_WITH_CUDA)
		INSTALL(FILES ${lib_hdrs} ${lib_hdrs_cuda} DESTINATION ${GEDEON_INCLUDE_INSTALL_PATH}/${PROJECT_NAME_SMALL}/${localname}/)
	ELSE(GEDEON_BUILD_WITH_CUDA)
		INSTALL(FILES ${lib_hdrs} DESTINATION ${GEDEON_INCLUDE_INSTALL_PATH}/${PROJECT_NAME_SMALL}/${localname}/)
	ENDIF(GEDEON_BUILD_WITH_CUDA)
ENDIF(lib_hdrs OR lib_hdrs_cuda)


IF(${PROJECT_NAME}_BUILD_TEST AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/test)
	create_test(${localname} ${ARGN})
ENDIF()
