#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * This file is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The HVRL Engine Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/


SET(localname calibration)

PROJECT(${PROJECT_NAME}_${localname})

SET(the_target "${PROJECT_NAME_SMALL}_${localname}")

INCLUDE_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/include")
INCLUDE_DIRECTORIES("${GEDEON_SOURCE_DIR}/modules/core/include/")
INCLUDE_DIRECTORIES("${GEDEON_SOURCE_DIR}/modules/datatypes/include/")
INCLUDE_DIRECTORIES("${OpenCV_INCLUDE_DIRS}")
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})

SET(local_include_dir "${GEDEON_SOURCE_DIR}/modules/${localname}/include/${PROJECT_NAME_SMALL}/${localname}")

SET(lib_srcs src/projector.cpp src/projectivecamera.cpp src/2d3d.cpp src/projectivecamera.cpp src/3d3d.cpp src/tools.cpp src/intrinsic.cpp)
SET(lib_hdrs ${local_include_dir}/projector.hpp ${local_include_dir}/projectivecamera.hpp ${local_include_dir}/2d3d.hpp ${local_include_dir}/3d3d.hpp ${local_include_dir}/tools.hpp 
	${local_include_dir}/intrinsic.hpp ${local_include_dir}/calibration.hpp)

IF(GEDEON_BUILD_WITH_CUDA)
	INCLUDE_DIRECTORIES(${CUDA_CUT_INCLUDE_DIR})
	CUDA_INCLUDE_DIRECTORIES(${CUDA_INCLUDE_DIRS})
	
	SET(lib_srcs_cuda src/backproject.cu src/3d3d.cu)
	SET(lib_hdrs_cuda ${local_include_dir}/backproject.cuh ${local_include_dir}/calibration.cuh ${local_include_dir}/3d3d.cuh)
	
	if (UNIX OR APPLE)
	    set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS}  "-Xcompiler;-fPIC;")
	endif()
	
	IF(GEDEON_BUILD_IN_DEBUG_MODE)
		set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "-g;-G;")
	ENDIF(GEDEON_BUILD_IN_DEBUG_MODE)
	
	set (CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "-arch;sm_20")
ENDIF(GEDEON_BUILD_WITH_CUDA)

IF(lib_srcs OR lib_srcs_cuda)

	SOURCE_GROUP("sources" FILES ${lib_srcs} ${lib_srcs_cuda})
	
	IF(GEDEON_BUILD_WITH_CUDA)
		IF(GEDEON_BUILD_SHARED_LIBS)
			CUDA_ADD_LIBRARY(${the_target} ${lib_srcs} ${lib_hdrs} ${lib_srcs_cuda} ${lib_hdrs_cuda})
		ELSE(GEDEON_BUILD_SHARED_LIBS)
			CUDA_ADD_LIBRARY(${the_target} STATIC ${lib_srcs} ${lib_hdrs} ${lib_srcs_cuda} ${lib_hdrs_cuda})
		ENDIF(GEDEON_BUILD_SHARED_LIBS)
	ELSE(GEDEON_BUILD_WITH_CUDA)
		IF(GEDEON_BUILD_SHARED_LIBS)
			ADD_LIBRARY(${the_target} SHARED ${lib_srcs} ${lib_hdrs})
		ELSE(GEDEON_BUILD_SHARED_LIBS)
			ADD_LIBRARY(${the_target} STATIC ${lib_srcs} ${lib_hdrs})
		ENDIF(GEDEON_BUILD_SHARED_LIBS)
	ENDIF(GEDEON_BUILD_WITH_CUDA)
	
	IF(GEDEON_BUILD_WITH_CUDA)
		TARGET_LINK_LIBRARIES(${the_target} ${CUDA_LIBRARIES} ${CUDA_CUT_LIBRARIES})
	ELSE(GEDEON_BUILD_WITH_CUDA)
		TARGET_LINK_LIBRARIES(${the_target})
	ENDIF(GEDEON_BUILD_WITH_CUDA)
	
	INSTALL(TARGETS ${the_target}
	        RUNTIME DESTINATION ${GEDEON_BIN_INSTALL_PATH}
	        LIBRARY DESTINATION ${GEDEON_LIB_INSTALL_PATH}
	        ARCHIVE DESTINATION ${GEDEON_LIB_INSTALL_PATH})

ENDIF(lib_srcs OR lib_srcs_cuda)

IF(lib_hdrs OR lib_hdrs_cuda)
	SOURCE_GROUP("headers" FILES ${lib_hdrs} ${lib_hdrs_cuda})
	IF(GEDEON_BUILD_WITH_CUDA)
		INSTALL(FILES ${lib_hdrs} ${lib_hdrs_cuda} DESTINATION ${GEDEON_INCLUDE_INSTALL_PATH}/${PROJECT_NAME_SMALL}/${localname}/)
	ELSE(GEDEON_BUILD_WITH_CUDA)
		INSTALL(FILES ${lib_hdrs} DESTINATION ${GEDEON_INCLUDE_INSTALL_PATH}/${PROJECT_NAME_SMALL}/${localname}/)
	ENDIF(GEDEON_BUILD_WITH_CUDA)
ENDIF(lib_hdrs OR lib_hdrs_cuda)


IF(${PROJECT_NAME}_BUILD_TEST AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/test)
	create_test(${localname} ${ARGN})
ENDIF()
