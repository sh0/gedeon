/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * converter.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * converter.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_DATATYPES__CONVERTER_HPP__
#define GEDEON_DATATYPES__CONVERTER_HPP__

#include "gedeon/datatypes/rgbimage.hpp"
#include "gedeon/datatypes/intensityimage.hpp"
#include "gedeon/datatypes/depthimage.hpp"
#include "gedeon/core/converter.hpp"

#include <opencv2/opencv.hpp>

namespace gedeon {

namespace Converter {

/**
 * \brief Convert a rgb image into an intensity image
 *
 * \param iimg the output intensity image
 * \param rgbimg the input rgb image
 * \return if conversion was fine or not
 */
inline bool convertImage(IntensityImage& iimg, const RGBImage& rgbimg) {

	iimg.setSize(rgbimg.getSize());
	const unsigned int size = iimg.getSize().width*iimg.getSize().height;
	unsigned char *tmp_buffer = new unsigned char[size];
	unsigned char* ptr_rgbimg = rgbimg.getData().get();
	for(unsigned int i = 0; i < size; ++i){
		tmp_buffer[i] = rgbToGrayscale(ptr_rgbimg + 3*i);
	}
	iimg.setData(tmp_buffer);
	delete tmp_buffer;
	return true;
}

/**
 * \brief Convert a rgb image into an intensity image
 *
 * \param rgbimg the output rgb image
 * \param iimg the input intensity image
 * \return if conversion was fine or not
 */
inline bool convertImage(RGBImage& rgbimg, const IntensityImage& iimg) {

	rgbimg.setSize(iimg.getSize());
	const unsigned int size = rgbimg.getSize().width*rgbimg.getSize().height;
	unsigned char *tmp_buffer = new unsigned char[size*3];
	unsigned char* ptr_iimg = iimg.getData().get();
	for(unsigned int i = 0; i < size; ++i){
		tmp_buffer[i*3] = tmp_buffer[i*3 + 1] = tmp_buffer[i*3 + 2] = ptr_iimg[i];
	}
	rgbimg.setData(tmp_buffer);
	delete tmp_buffer;
	return true;
}


namespace OpenCV {

/**
 * \brief Convert a intensity image into an opencv image
 *
 * \param cviimg the output opencv gray image (CV_8UC1)
 * \param iimg the input intensity image
 * \return if conversion was fine or not
 */
inline bool convertImage(cv::Mat& cviimg, const IntensityImage& iimg) {
	if(true == cviimg.empty() || cviimg.rows != int(iimg.getSize().height) || cviimg.cols != int(iimg.getSize().width) || cviimg.type() != CV_8UC1){
		cviimg.release();
		cviimg = cv::Mat(iimg.getSize().height,iimg.getSize().width,CV_8UC1);
	}
	const unsigned int size = iimg.getSize().width*iimg.getSize().height;
	unsigned char* ptr_iimg = iimg.getData().get();
	memcpy(cviimg.data,ptr_iimg,size*sizeof(unsigned char));
	return true;
}

/**
 * \brief Convert an opencv image into a intensity image
 *
 * \param cviimg the input opencv gray image (CV_8UC1)
 * \param iimg the output intensity image
 * \return if conversion was fine or not
 */
inline bool convertImage(IntensityImage& iimg, const cv::Mat& cviimg) {
	if(true == cviimg.empty() || cviimg.type() != CV_8UC1){
		return false;
	}
	iimg.setSize(gedeon::Size(cviimg.size().width,cviimg.size().height));
	const unsigned int size = iimg.getSize().width*iimg.getSize().height;
	unsigned char* ptr_iimg = iimg.getData().get();
	memcpy(ptr_iimg,cviimg.data,size*sizeof(unsigned char));
	return true;
}


/**
 * \brief Convert a color image into an opencv image
 *
 * \param cvimg the output opencv color image (CV_8UC3 - BGR)
 * \param img the input color image
 * \return if conversion was fine or not
 */
inline bool convertImage(cv::Mat& cvimg, const RGBImage& img) {
	if(true == cvimg.empty() || cvimg.rows != int(img.getSize().height) || cvimg.cols != int(img.getSize().width) || cvimg.type() != CV_8UC3){
		cvimg.release();
		cvimg = cv::Mat(img.getSize().height,img.getSize().width,CV_8UC3);
	}
	const unsigned int size = img.getSize().width*img.getSize().height*3;
	unsigned char* ptr_img = img.getData().get();
	memcpy(cvimg.data,ptr_img,size*sizeof(unsigned char));
	cv::cvtColor(cvimg,cvimg,CV_BGR2RGB);
	return true;
}


/**
 * \brief Convert an opencv image into a color image
 *
 * \param cvimg the input opencv color image (CV_8UC3 - BGR)
 * \param img the output color image
 * \return if conversion was fine or not
 */
inline bool convertImage(RGBImage& img, const cv::Mat& cvimg) {
	if(true == cvimg.empty() || cvimg.type() != CV_8UC3){
		return false;
	}
	img.setSize(gedeon::Size(cvimg.size().width,cvimg.size().height));
	const unsigned int size = 3*img.getSize().width*img.getSize().height;
	cv::Mat cvimg_tmp;
	cv::cvtColor(cvimg,cvimg_tmp,CV_BGR2RGB);
	unsigned char* ptr_img = img.getData().get();
	memcpy(ptr_img,cvimg_tmp.data,size*sizeof(unsigned char));
	return true;
}

/**
 * \brief Convert a depth image into an opencv image
 *
 * \param cvdimg the output opencv depth image (CV_32F)
 * \param dimg the input depth image
 * \return if conversion was fine or not
 */
inline bool convertImage(DepthImage& dimg, const cv::Mat& cvdimg) {
	if(true == cvdimg.empty() || cvdimg.type() != CV_32F){
		return false;
	}
	dimg.setSize(gedeon::Size(cvdimg.size().width,cvdimg.size().height));
	const unsigned int size = dimg.getSize().width*dimg.getSize().height;
	float* ptr_img = dimg.getData().get();
	memcpy(ptr_img,cvdimg.data,size*sizeof(float));
	return true;
}

/**
 * \brief Convert an opencv image into a depth image
 *
 * \param cvdimg the input opencv depth image (CV_32F)
 * \param dimg the output depth image
 * \return if conversion was fine or not
 */
inline bool convertImage(cv::Mat& cvdimg, const DepthImage& dimg) {
	if(true == cvdimg.empty() || cvdimg.rows != int(dimg.getSize().height) || cvdimg.cols != int(dimg.getSize().width) || cvdimg.type() != CV_32F){
		cvdimg.release();
		cvdimg = cv::Mat(dimg.getSize().height,dimg.getSize().width,CV_32F);
	}
	const unsigned int size = dimg.getSize().width*dimg.getSize().height;
	float* ptr_img = dimg.getData().get();
	memcpy(cvdimg.data,ptr_img,size*sizeof(float));
	return true;
}

}
}

}

#endif
