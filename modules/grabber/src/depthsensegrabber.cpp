/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opennigrabber.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opennigrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "gedeon/grabber/depthsensedriver.hpp"
#include "gedeon/grabber/depthsensegrabber.hpp"

namespace gedeon {

class DepthSenseGrabberParams {
public:
	DepthSenseGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~DepthSenseGrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;

};

class DepthSenseGrabberCapture {
public:
	DepthSenseGrabberCapture(void) :
			devicename(""), device_id(0) {
	}
	~DepthSenseGrabberCapture(void) {
	}
	DepthSense::DepthNode depth_node;
	DepthSense::ColorNode color_node;
	DepthSense::Context context;
	DepthSense::StereoCameraParameters cam_parameters;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

};

class DepthSenseGrabberThread {
public:
	DepthSenseGrabberThread(void) :
			pthread(0) {
	}
	~DepthSenseGrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

static void onNewDepthSample_cb(DepthSense::DepthNode node,
		DepthSense::DepthNode::NewSampleReceivedData data,
		DepthSenseGrabber* g);

static void onNewColorSample_cb(DepthSense::ColorNode node,
		DepthSense::ColorNode::NewSampleReceivedData data,
		DepthSenseGrabber* g);

DepthSenseGrabber::DepthSenseGrabber(void) :
		Grabber(), capturepimpl(new DepthSenseGrabberCapture()), paramspimpl(
				new DepthSenseGrabberParams()), threadpimpl(
				new DepthSenseGrabberThread()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool DepthSenseGrabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("DepthSenseDriver::init",
				"The grabber is already initialized");
		return false;
	}

	DepthSenseDriver *driver = 0;

	try {
		driver = dynamic_cast<DepthSenseDriver *>(d);
	} catch (const std::bad_cast& e) {
		Log::add().error("DepthSenseDriver::init",
				"The driver is not compatible");
		return false;
	}
	this->capturepimpl->context = driver->getContext();

	if (0 != driver->getCount() && id < driver->getCount()) {

		bool created = false;

		/*
		 * Find the device in the list of devices
		 */
		std::vector<DepthSense::Device> da =
				this->capturepimpl->context.getDevices();
		DepthSense::Device device = da[id];
		this->capturepimpl->devicename = d->populate()[id];

		this->capturepimpl->rgbdi.reset(new RGBDIImage);

		std::vector<DepthSense::Node> na = device.getNodes();
		for (int i = 0; i < (int) na.size(); i++) {
			DepthSense::Node node = na[i];
			if (node.is<DepthSense::DepthNode>()) {
				capturepimpl->depth_node = node.as<DepthSense::DepthNode>();
				capturepimpl->depth_node.newSampleReceivedEvent().connect(
						&onNewDepthSample_cb, this);
				try {
					this->capturepimpl->context.requestControl(
							capturepimpl->depth_node, 0);
				} catch (DepthSense::Exception e) {
					throw Exception("DepthSenseGrabber::init", e.getMessage());
				}
				capturepimpl->depth_node.setEnableDepthMapFloatingPoint(true);
				capturepimpl->depth_node.setEnableDenoising(true);
				this->paramspimpl->hasdepth = true;
				int width, height;
				DepthSense::DepthNode::Configuration config =
						capturepimpl->depth_node.getConfiguration();
				config.mode = DepthSense::DepthNode::CAMERA_MODE_LONG_RANGE;
				config.saturation = true;
				config.frameFormat = DepthSense::FRAME_FORMAT_QQVGA;
				config.framerate = 30;
				capturepimpl->depth_node.setConfiguration(config);
				config = capturepimpl->depth_node.getConfiguration();
				DepthSense::FrameFormat_toResolution(config.frameFormat, &width,
						&height);
				this->capturepimpl->rgbdi.get()->depth.setSize(
						Size(width, height));
				this->paramspimpl->framerate = config.framerate;
			}
			if (node.is<DepthSense::ColorNode>()) {
				capturepimpl->color_node = node.as<DepthSense::ColorNode>();
				capturepimpl->color_node.newSampleReceivedEvent().connect(
						&onNewColorSample_cb, this);
				try {
					this->capturepimpl->context.requestControl(
							capturepimpl->color_node, 0);
				} catch (DepthSense::Exception e) {
					throw Exception("DepthSenseGrabber::init", e.getMessage());
				}
				capturepimpl->color_node.setEnableColorMap(true);
				this->paramspimpl->hascolor = true;
				int width, height;
				DepthSense::ColorNode::Configuration config =
						capturepimpl->color_node.getConfiguration();
				//config.compression = DepthSense::COMPRESSION_TYPE_MJPEG;
				config.frameFormat = DepthSense::FRAME_FORMAT_VGA;
				config.powerLineFrequency =
						DepthSense::POWER_LINE_FREQUENCY_50HZ;
				config.framerate = 30;
				capturepimpl->color_node.setConfiguration(config);
				config = capturepimpl->color_node.getConfiguration();
				DepthSense::FrameFormat_toResolution(config.frameFormat, &width,
						&height);
				this->capturepimpl->rgbdi.get()->color.setSize(
						Size(width, height));
				this->paramspimpl->framerate = config.framerate;
			}

		}

		this->capturepimpl->cam_parameters = device.getStereoCameraParameters();

		created = true;

		if (created == false) {
			Log::add().error("OpenNIGrabber::init",
					"Failed finding the openni device "
							+ IO::numberToString(id));
			return false;
		}

		this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;

		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		this->paramspimpl->initialized = true;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("OpenNIGrabber::init", "No device found");
	return false;
}

bool DepthSenseGrabber::init(const std::string& source, Driver* d) {
	Log::add().warning("DepthSenseGrabber::init",
			"No internal format available");
	return false;
}

DepthSenseGrabber::~DepthSenseGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
	delete capturepimpl;
	delete paramspimpl;
	delete threadpimpl;
}

std::string DepthSenseGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int DepthSenseGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& DepthSenseGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool DepthSenseGrabber::getParameter(const GrabberParameter& param,
		float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (DEPTHSENSE_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}
	return false;
}

bool DepthSenseGrabber::getParameter(const GrabberParameter& param,
		float* values, unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool DepthSenseGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (DEPTHSENSE_FRAMERATE == param) {
		setFramerate(value);
		return true;
	}

	return false;
}

bool DepthSenseGrabber::setFramerate(const float& value) {
	DepthSense::DepthNode::Configuration configd =
			capturepimpl->depth_node.getConfiguration();
	configd.framerate = 30;
	capturepimpl->depth_node.setConfiguration(configd);
	configd = capturepimpl->depth_node.getConfiguration();
	if(value != configd.framerate){
		return false;
	}

	DepthSense::ColorNode::Configuration configc =
			capturepimpl->color_node.getConfiguration();
	configc.framerate = 30;
	capturepimpl->color_node.setConfiguration(configc);
	configc = capturepimpl->color_node.getConfiguration();
	if(value != configc.framerate){
		return false;
	}

	this->paramspimpl->framerate = value;
	this->paramspimpl->adaptative_framerate = value;
	return true;
}

bool DepthSenseGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (DEPTHSENSE_NEAR_MODE == trig) {
		DepthSense::DepthNode::Configuration config =
				capturepimpl->depth_node.getConfiguration();
		if (DepthSense::DepthNode::CAMERA_MODE_LONG_RANGE == config.mode) {
			config.mode = DepthSense::DepthNode::CAMERA_MODE_CLOSE_MODE;
		} else {
			config.mode = DepthSense::DepthNode::CAMERA_MODE_LONG_RANGE;
		}
		capturepimpl->depth_node.setConfiguration(config);
		return true;
	}

	return false;
}

bool DepthSenseGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool DepthSenseGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool DepthSenseGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool DepthSenseGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void DepthSenseGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void DepthSenseGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&DepthSenseGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void DepthSenseGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;
		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		emitEvent(this->threadpimpl->stopped);
	}
}

void DepthSenseGrabber::grabOneFrame(void){
	Log::add().warning("DepthSenseGrabber::grabOneFrame", "Not available");
}

void convertAndCopy(unsigned char* dest,
		DepthSense::ColorNode::NewSampleReceivedData* data) {
	unsigned char* tmp = dest;
	for (int i = 0; i < data->colorMap.size(); i += 4) {
		unsigned char y1 = data->colorMap[i];
		unsigned char u = data->colorMap[i + 1];
		unsigned char y2 = data->colorMap[i + 2];
		unsigned char v = data->colorMap[i + 3];

		Converter::yuv422ToRgb24(y1, u, y2, v, *tmp, *(tmp + 1), *(tmp + 2),
				*(tmp + 3), *(tmp + 4), *(tmp + 5));

		tmp += 6;
	}
}

void DepthSenseGrabber::onNewColorSample(
		DepthSense::ColorNode::NewSampleReceivedData data) {
	if (false == this->paramspimpl->paused) {
		if (DepthSense::COMPRESSION_TYPE_MJPEG
				== this->capturepimpl->color_node.getConfiguration().compression) {

		} else {
			convertAndCopy(
					this->capturepimpl->rgbdi.get()->color.getData().get(),
					&data);
		}
	}
}

void DepthSenseGrabber::onNewDepthSample(
		DepthSense::DepthNode::NewSampleReceivedData data) {
	if (false == this->paramspimpl->paused) {
		memcpy(this->capturepimpl->rgbdi.get()->depth.getData().get(),
				data.depthMapFloatingPoint,
				data.depthMapFloatingPoint.size() * sizeof(float));
		emitEvent(this->threadpimpl->updated);
	}

}

void DepthSenseGrabber::run(void) {

	if (true == this->paramspimpl->hascolor) {
		this->capturepimpl->context.registerNode(
				this->capturepimpl->color_node);
	}
	if (true == this->paramspimpl->hasdepth) {
		this->capturepimpl->context.registerNode(
				this->capturepimpl->depth_node);
	}

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();

		current_time = (1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	if (true == this->paramspimpl->hascolor) {
		this->capturepimpl->context.unregisterNode(
				this->capturepimpl->color_node);
	}
	if (true == this->paramspimpl->hasdepth) {
		this->capturepimpl->context.unregisterNode(
				this->capturepimpl->depth_node);
	}
	timer.stop();
	this->paramspimpl->finished = true;

}

void DepthSenseGrabber::setStreamFilename(const std::string& filename) {
}

std::string DepthSenseGrabber::getStreamFilename(void) const {
	return "";
}

static void onNewDepthSample_cb(DepthSense::DepthNode node,
		DepthSense::DepthNode::NewSampleReceivedData data,
		DepthSenseGrabber* g) {
	g->onNewDepthSample(data);
}

static void onNewColorSample_cb(DepthSense::ColorNode node,
		DepthSense::ColorNode::NewSampleReceivedData data,
		DepthSenseGrabber* g) {
	g->onNewColorSample(data);
}

}
