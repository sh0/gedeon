#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * main.cpp created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * main.cpp is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

#include "gedeon/calibration/3d3d.hpp"
#include "gedeon/core/core.hpp"
#include <cstdlib>
#include <iostream>

int main(int argc, char **argv) {

	float transformation[12] = { 0.75f, 0.433012701f, -0.5f, 1.5f,
				    -0.21650635f, 0.875f,  0.433012701f, -2.0f,
				     0.625f, -0.21650635f,  0.75f, 9.0f};

	std::vector<cv::Vec3f> reference;
	reference.push_back(cv::Vec3f(1.0f,0.5f,3.0f));
	reference.push_back(cv::Vec3f(8.3f,9.5f,-0.5f));
	reference.push_back(cv::Vec3f(10.2f,3.2f,6.1f));
	reference.push_back(cv::Vec3f(-3.0f,-4.8f,7.0f));
	reference.push_back(cv::Vec3f(4.0f,15.2f,-2.8f));
	reference.push_back(cv::Vec3f(1.5f,1.3f,-3.6f));
	reference.push_back(cv::Vec3f(5.1f,-12.7f,15.0f));

	std::vector<cv::Vec3f> target;
	for(unsigned int i = 0; i < reference.size(); ++i){
		cv::Vec3f v;
		for(unsigned int j = 0; j < 3; ++j){
			v[j] = reference[i][0]*transformation[j*4] + reference[i][1]*transformation[j*4+1] + reference[i][2]*transformation[j*4+2] + transformation[j*4+3];
		}
		target.push_back(v);
	}
	target[2][0] = 145.0f;
	target[4][0] = 12.0f;

	// Noise
	target[0][0] -= 0.0001f;
	target[1][1] -= 0.0002f;
	target[2][2] += 0.0001f;
	target[5][0] += 0.0002f;
	target[3][1] -= 0.0002f;
	target[5][2] += 0.0001f;


	std::string s("");
	for(unsigned int i = 0; i < reference.size(); ++i){
		s += gedeon::IO::numberToString(reference[i]) + " " + gedeon::IO::numberToString(target[i]) + "\n";
	}
	gedeon::Log::add().info("\n" + s);

	gedeon::Calibration3D3D::filterCorrespondences(reference,target);
	gedeon::Log::add().info("Filtered result: (Rows 3 and 5 should have been removed)");

	s = std::string("");
	for(unsigned int i = 0; i < reference.size(); ++i){
		s += gedeon::IO::numberToString(reference[i]) + " " + gedeon::IO::numberToString(target[i]) + "\n";
	}
	gedeon::Log::add().info("\n" + s);

	float T[12];
	//gedeon::Calibration3D3D::estimateTransformation(T, reference, target);
	gedeon::Calibration3D3D::estimateTransformationWithRansac(T, reference, target, 90);
	//gedeon::Calibration3D3D::estimateTransformationWithRansacInliers(T, reference, target, 0.9f);

	gedeon::Log::add().info("Ground truth rotation & translation:");
	s = std::string("");
	for(unsigned int i = 0; i < 3; ++i){
		for(unsigned int j = 0; j < 3; ++j){
			s += gedeon::IO::numberToString(transformation[i*4+j]) + " ";
		}
		s += gedeon::IO::numberToString(transformation[i*4+3]) + "\n";
	}
	gedeon::Log::add().info("\n" + s);

	s = std::string("");
	gedeon::Log::add().info("Estimated rotation & translation:" );
	for(unsigned int i = 0; i < 3; ++i){
		for(unsigned int j = 0; j < 3; ++j){
			s += gedeon::IO::numberToString(T[i*4+j]) + " ";
		}
		s += gedeon::IO::numberToString(T[i*4+3]) + "\n";
	}
	gedeon::Log::add().info("\n" + s);

	return EXIT_SUCCESS;
}

