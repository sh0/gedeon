/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/projector.hpp"
#include "gedeon/core/core.hpp"
#include <cstdlib>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace gedeon;

int main(int argc, char **argv) {

	if (3 != argc) {
		Log::add().error("main",
				"Please give the dimensions of the image [width height]");
		return EXIT_FAILURE;
	}

	int width = atoi(argv[1]);
	int height = atoi(argv[2]);

	if(0 >= width || 0 >= height){
		Log::add().error("main",
						"Dimension of the image should be strictly positive");
				return EXIT_FAILURE;
	}

	cv::Mat input(height, width, CV_8U);
	cv::Mat inputc3(height, width, CV_8UC3);

	int code_size_h;
	boost::shared_array<unsigned int> graycodeh = Projector::GrayCode::get(code_size_h, width);

	int code_size_v;
	boost::shared_array<unsigned int> graycodev = Projector::GrayCode::get(code_size_v, height);

	bool running = true;
	int level = code_size_h;
	bool horizontal = true;
	while (true == running) {

		if(true == horizontal){
			Projector::GrayCode::createImageC1(input.data, width, height, graycodeh, true, level);
			Projector::GrayCode::createImageC3(inputc3.data, width, height, graycodeh, true, level);
		}else{
			Projector::GrayCode::createImageC1(input.data, width, height, graycodev, false, level);
			Projector::GrayCode::createImageC3(inputc3.data, width, height, graycodev, false, level);
		}
		level--;
		if(0 > level){
			if(true == horizontal){
				level = code_size_h;
				horizontal = false;
			}else{
				level = code_size_v;
				horizontal = true;
			}
		}

		char k = cv::waitKey(1000);
		if (k == 27 || k == 'q') {
			running = false;
		}

		cv::imshow("Gray code", input);
		cv::imshow("Gray code C3", inputc3);
	}

	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

