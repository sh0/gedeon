/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * pointcloud.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * pointcloud.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/datatypes/pointcloud.hpp"

namespace gedeon {

PointCloud::PointCloud(void) :
		size(0, 0) {
	this->data.reset();
}

PointCloud::PointCloud(const Size &s, const float* data) :
		size(s) {

	assert(s.width*s.height > 0);

	this->data.reset(new float[3 * this->size.width * this->size.height]);
	if (data != 0) {
		setData(data);
	}else{
		memset(this->data.get(),0,3 * this->size.width * this->size.height*sizeof(float));
	}
}


PointCloud::PointCloud(const PointCloud& pc){
	this->size = pc.getSize();
	this->data.reset(new float[3 * this->size.width * this->size.height]);
	if (this->data != 0) {
		setData(pc.getData().get());
	}
}
PointCloud::~PointCloud(void) {
}

void PointCloud::setSize(const Size& s) {

	assert(s.width*s.height > 0);

	if (this->size != s) {
		boost::mutex::scoped_lock lock(mutex);
		this->size = s;
		this->data.reset();
		this->data.reset(new float[3 * this->size.width * this->size.height]);
	}
}

Size PointCloud::getSize(void) const {
	return this->size;
}

void PointCloud::setData(const float *d) {

	assert(size.width*size.height > 0);

	if (d != 0 && this->data.get() != 0) {
		boost::mutex::scoped_lock lock(mutex);
		memcpy(this->data.get(), d,
				3 * this->size.width * this->size.height * sizeof(float));
	}
}


PointCloudDataArr PointCloud::getData(void) const{
	boost::mutex::scoped_lock lock(mutex);
	return this->data;
}

PointCloud& PointCloud::operator=(const PointCloud& pc){
	if (this == &pc) return *this;
	setSize(pc.getSize());
	setData(pc.getData().get());
	return *this;
}

}
