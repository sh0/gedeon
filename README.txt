#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * README created in 05 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * This file is part of the GEDEON library.
# *
# * The GEDEON library is a free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

The library has been tested under Linux Ubuntu 12.04 LTS.

LIBRARIES
------------------
OpenCV 2.4.6.1
libBoost 1.46
zlib

cuda >=4.0 with cutil (optional)
OpenNI 1.5.2 (optional)
DepthSense 1.2.1-840(optional)
PCap 0.8 (optional)
libGLM

doxygen (for the doc)

Tested under Ubuntu LTS 12.04 and windows 7 with VS C++ 2010

HOW TO
------------------
The build of the GEDEON library requires cmake >=2.8

Inside of the GUI of cmake, you have access to several options:
GEDEON_BUILD_DOC (default true) : Generate the doc by calling using the rule doc (ex: make doc)
GEDEON_BUILD_TEST (default false) : Generate the tests (tests are no yet created)
GEDEON_BUILD_SAMPLES (default false) : Generate the samples realted to the library
GEDEON_BUILD_SHARED_LIB (default false) : Build the shared version of the library instead of static (.so or .dll)

GEDEON_BUILD_CUDA (default true) : Include cuda code during the compilation
GEDEON_BUILD_OPENMP (default false) : Use OpenMP to optimize so part of the code (Not yet used)

GEDEON_BUILD_IN_DEBUG_MODE (default false) : Compile the sources in debug mode
GEDEON_ENABLE_PROFILING (default false) : Activate the profiling mode

GEDEON_INSTALL_PATH (default the current directory) : Installation directory

GEDEON_BUILD_GRABBER (default true) : Build the module grabber
GEDEON_BUILD_IMAGE_PROCESSING (default true) : Build the module imageprocessing
GEDEON_BUILD_POINTCLOUD_PROCESSING (default true) : Build the module pointcloudprocessing
GEDEON_BUILD_CALIBRATION (default true) : Build the module calibration
GEDEON_BUILD_GRAPHICS (default true) : Build the module graphics (requires OpenGL and GLM)

Initialization and compilation (Linux):
mkdir build
cd build
cmake-gui ../
make
make install
make doc

HOW TO USE IN A NEW PROJECT
---------------------------

use the following code in cmake:

#-------------------------------------
# Look for Gedeon
#-------------------------------------
SET(GEDEON_CONFIG_PATH "" CACHE PATH "Path to the gedeon's build directory")
FIND_PATH( GEDEON_CONFIG_PATH gedeonconfig.cmake
	PATHS ${GEDEON_CONFIG_PATH}
	DOC "Path to Gedeon's configuration file")
IF(GEDEON_CONFIG_PATH)
	MESSAGE(STATUS "Looking for library GEDEON - found")
	INCLUDE(${GEDEON_CONFIG_PATH}/gedeonconfig.cmake REQUIRED)
ELSE(GEDEON_CONFIG_PATH)
	MESSAGE(SEND_ERROR "Looking for GEDEON - not found")
ENDIF(GEDEON_CONFIG_PATH)

Then with the cmake function TARGET_LINK_LIBRARIES you can use ${GEDEON_LIBRARIES}

You can find samples of how to include the library inside of the examples provided with the library

HOW TO RUN THE SAMPLES
----------------------

From the root of the project:
cd bin/samples/

Data samples can be downloaded from https://Frankoi@bitbucket.org/Frankoi/gedeon-data.git 

HOW TO INTEGRATE GEDEON TO A NEW PROJECT
----------------------------------------

In your cmake file:
	FIND_PATH( GEDEON_CONFIG_PATH gedeonconfig.cmake
			   PATHS ...
			   DOC "Path to Gedeon's configuration file")
	INCLUDE(${GEDEON_CONFIG_PATH}/gedeonconfig.cmake REQUIRED)
	
You can then use the variables GEDEON_LIBRARIES and GEDEON_INCLUDE_DIR


TODO
----


KNOWN ISSUES
------------
Access to firewire with linux:
in file /etc/udev/rules.d/51-local.rules add
KERNEL=="fw*", OWNER="root" GROUP="firewire" MODE="0660"
create group firewire: sudo groupadd firewire
add users to this group: sudo adduser name_user firewire

"The application was unable to start correctly (0xc000007b)" message under windows:
Check dependencies for the exe file with dependency walker, and check that everything is using the correct version of the dll (32/64 bits)

The number of cameras detected with opencv is wrong under Windows. This is due to a problem with OpenCV approach to manage webcam under Windows.

QUESTIONS
---------
fdesorbi@hvrl.ics.keio.ac.jp
