/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 3d3d.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 3d3d.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__3D3D_HPP__
#define GEDEON_CALIBRATION__3D3D_HPP__

#include <opencv2/core/core.hpp>

namespace gedeon{

namespace Calibration3D3D{


/*
 * \brief The function detect and remove the ouliers from a set of 3D-3D correspondences
 *
 * The two input sets of correspondences are
 *
 * Method inspired from "Visual Odometry and Mapping for Autonomous Flight Using an RGB-D Camera" (libfovis)
 *
 * \param reference_frame Input 3D points from the reference frame
 * \param target_frame Corresponding 3D points in the target frame
 *
 * \author Francois de Sorbier
 *
 */
void filterCorrespondences(std::vector<cv::Vec3f>& reference_frame,
		std::vector<cv::Vec3f>& target_frame);

/**
 * \brief compute the rigid transformation between two 3D point clouds.
 *
 * The method is described in "Least-squares estimation of transformation parameters between two point patterns" by Shinji Umeyama
 *
 * \param[out] T The estimated transformation
 * \param reference_frame the first input 3D point cloud
 * \param target_frame the second input with corresponding 3D points
 * \return true it has been successfully estimated, otherwise false
 * \author Francois de Sorbier
 */
bool estimateTransformation(float T[12], const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame );

/**
 * \brief compute the rigid transformation between two 3D point clouds minimized using Ransac.
 *
 * The Ransac minimization select randomly three correspondences and perform the rigid tranformation estimation
 *
 * \param[out] T The estimated transformation
 * \param reference_frame the first input 3D point cloud
 * \param target_frame the second input with corresponding 3D points
 * \param iterations The number of iteration for ransac
 * \return true it has been successfully estimated, otherwise false
 * \author Francois de Sorbier
 */
bool estimateTransformationWithRansac(float T[12],
		const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame,
		const unsigned int& iterations);

/**
 * \brief compute the rigid transformation between two 3D point clouds minimized using Ransac.
 *
 * The Ransac minimization select randomly three correspondences and perform the rigid tranformation estimation
 * The iterations stop when a minimum number of inliers is found
 *
 * \param[out] T The estimated transformation
 * \param reference_frame the first input 3D point cloud
 * \param target_frame the second input with corresponding 3D points
 * \param inliers Define the percentage of inliers thta has to be found until the ransac iterations stop
 * \return true it has been successfully estimated, otherwise false
 * \author Francois de Sorbier
 */
bool estimateTransformationWithRansacInliers(float T[12],
		const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame,
		const float& inliers);
}

}

#endif
