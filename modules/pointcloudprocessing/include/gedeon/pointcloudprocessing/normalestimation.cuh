/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * normalestimation.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * normalestimation.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_POINT_CLOUD_PROCESSING__NORMAL_ESTIMATION_CUH__
#define GEDEON_POINT_CLOUD_PROCESSING__NORMAL_ESTIMATION_CUH__

namespace gedeon {

namespace GPU {

namespace NormalEstimation {

/**
 * \brief Estimate the normals corresponding to the input point cloud
 *
 * The point cloud is supposed to be organized.
 *
 * \param d_output_normals the output buffer with normals
 * \param d_input_pointcloud the point cloud containing 3d coordinates
 * \param width the width of the image
 * \param height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 * \author Francois de Sorbier
 */
bool applySimple(float *d_output_normals, float *d_input_pointcloud,
							const unsigned int& width, const unsigned int& height,
							const unsigned int& block_x, const unsigned int& block_y);

/**
 * \brief Estimate the normals corresponding to the input point cloud
 *
 * The point cloud is supposed to be organized.
 *
 * The normal estimation search for a correct point in - and + direction of the x and y axis.
 *
 * \param d_output_normals the output buffer with normals
 * \param d_input_pointcloud the point cloud containing 3d coordinates
 * \param width the width of the image
 * \param height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 * \author Francois de Sorbier
 */
bool applyModerate(float *d_output_normals, float *d_input_pointcloud,
							const unsigned int& width, const unsigned int& height,
							const unsigned int& block_x, const unsigned int& block_y);
}

}

}

#endif
