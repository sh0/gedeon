/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

//#include "gedeon/core/core.hpp"
#include "gedeon/grabber/flycapturedriver.hpp"
#include "gedeon/grabber/flycapturegrabber.hpp"
#include "gedeon/datatypes/io.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

using namespace gedeon;
cv::Mat intensity;
cv::Mat color;
cv::Mat depth;
boost::mutex mutex;

FlyCaptureGrabber sensor;
IO::DataSaver saver;
IO::DataExporter exporter;
const std::string filename_base("flycapture-output");
const unsigned int compression_level = 4;

void displayImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				if (true == s->hasIntensity()) {
					if (0 == intensity.data) {
						intensity = cv::Mat(
								s->getImage()->intensity.getSize().height,
								s->getImage()->intensity.getSize().width,
								CV_8UC1);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(intensity.data, imgptr->intensity.getData().get(),
							s->getImage()->intensity.getSize().width
									* s->getImage()->intensity.getSize().height
									* sizeof(unsigned char));
				}
				if (true == s->hasColor()) {
					if (0 == color.data) {
						color = cv::Mat(s->getImage()->color.getSize().height,
								s->getImage()->color.getSize().width, CV_8UC3);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(color.data, imgptr->color.getData().get(),
							s->getImage()->color.getSize().width
									* s->getImage()->color.getSize().height * 3
									* sizeof(unsigned char));
					cvtColor(color, color, cv::COLOR_RGB2BGR);
				}
				if (true == s->hasDepth()) {
					if (0 == depth.data) {
						depth = cv::Mat(s->getImage()->depth.getSize().height,
								s->getImage()->depth.getSize().width, CV_32FC1);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(depth.data, imgptr->depth.getData().get(),
							s->getImage()->depth.getSize().height
									* s->getImage()->depth.getSize().width
									* sizeof(float));
					depth *= 1.0 / 5.0f;
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}


void recordImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				RGBDIImagePtr imgptr = s->getImage();

				if (true == exporter.isOpened()) {
					float fr = 30.0f;
					sensor.getParameter(FLYCAPTURE_FRAMERATE, fr);
					exporter.write(imgptr, fr);
				}

				if (true == saver.isOpened()) {
					std::string timestamp = IO::numberToString(getTimeStamp());
					saver.save(timestamp, imgptr);
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	FlyCaptureDriver &driver = FlyCaptureDriver::getInstance();

	std::cout << "Looking for " << driver.getName() << " devices..."
			<< std::endl;
	driver.refresh();

	std::cout << "Number of devices found: " << driver.getCount() << std::endl;
	if (driver.getCount() > 0) {

		std::vector<std::string> devices = driver.populate();
		std::vector<std::string>::iterator it = devices.begin();
		while (devices.end() != it) {
			std::cout << "* " << *it << std::endl;
			++it;
		}

		std::cout << "Access the first device..." << std::endl;

		if (false == sensor.init(0, &driver)) {
			return EXIT_FAILURE;
		}
	}else{
		return EXIT_FAILURE;
	}

	boost::signals2::connection c;
	try {
		if (false
				== sensor.connect("updated", boost::bind(displayImage, _1),
						c)) {
			std::cerr << "Error: Cannot connect to the envent of the sensor"
					<< std::endl;
		}
	} catch (Exception e) {
		std::cerr << e << std::endl;
	}

	boost::signals2::connection c2;
	try {
		if (false
				== sensor.connect("updated", boost::bind(recordImage, _1), c)) {
			std::cerr << "Error: Cannot connect to the event of the sensor"
					<< std::endl;
		}
	} catch (Exception e) {
		std::cerr << e << std::endl;
	}

	std::cout << "Start the capture..." << std::endl;
	sensor.play();

	secSleep(1);

	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}
		if( k == 'h'){
			std::cout << "Help" << std::endl;
			std::cout << "------------" << std::endl;
			std::cout << "q: quit the program" << std::endl;
			std::cout << "p: pause" << std::endl;
			std::cout << "g: export in a gmv file" << std::endl;
			std::cout << "d: export in directories" << std::endl;
			std::cout << "s: save images as png files" << std::endl;
			std::cout << "1: set 7.5fps" << std::endl;
			std::cout << "2: set 15fps" << std::endl;
			std::cout << "3: set 30fps" << std::endl;
			std::cout << "4: set 60fps" << std::endl;
			std::cout << "5: set QQVGA YUV444 resolution" << std::endl;
			std::cout << "6: set QVGA YUV422 resolution" << std::endl;
			std::cout << "7: set VGA YUV422 resolution" << std::endl;
			std::cout << "8: set VGA RGB resolution" << std::endl;
			std::cout << "9: set SVGA RGB resolution" << std::endl;

		}
		if (k == 'p') {
			sensor.pause();
		}
		if ('g' == k) {
			if (false == exporter.isOpened()) {
				exporter.open(filename_base + ".gmv", compression_level);
			} else {
				exporter.close();
			}
		}
		if ('d' == k) {
			if (false == saver.isOpened()) {
				saver.open(filename_base);
			} else {
				saver.close();
			}
		}
		if ('1' == k) {
					if (sensor.trigger(FLYCAPTURE_7_5FPS) == false){
						std::cerr << "unsupported framerate" << std::endl;
					}
				}
		if ('2' == k) {
			if (sensor.trigger(FLYCAPTURE_15FPS) == false){
				std::cerr << "unsupported framerate" << std::endl;
			}
		}
		if ('3' == k) {
			if (sensor.trigger(FLYCAPTURE_30FPS) == false){
				std::cerr << "unsupported framerate" << std::endl;
			}
		}
		if ('4' == k) {
			if (sensor.trigger(FLYCAPTURE_60FPS) == false){
				std::cerr << "unsupported framerate" << std::endl;
			}
		}
		if ('5' == k) {
			if (sensor.trigger(FLYCAPTURE_YUV444_QQVGA) == false){
				std::cerr << "unsupported resolution" << std::endl;
			}
			color.release();
		}
		if ('6' == k) {
			if (sensor.trigger(FLYCAPTURE_YUV422_QVGA) == false){
				std::cerr << "unsupported resolution" << std::endl;
			}
			color.release();
		}
		if ('7' == k) {
			if (sensor.trigger(FLYCAPTURE_YUV422_VGA) == false){
				std::cerr << "unsupported resolution" << std::endl;
			}
			color.release();
		}
		if ('8' == k) {
			if (sensor.trigger(FLYCAPTURE_RGB_VGA) == false){
				std::cerr << "unsupported resolution" << std::endl;
			}
		}
		if ('9' == k) {
			if (sensor.trigger(FLYCAPTURE_RGB_SVGA) == false){
				std::cerr << "unsupported resolution" << std::endl;
			}
			color.release();
		}
		if ('s' == k) {
			unsigned long long ts = getTimeStamp();
			if (intensity.data != 0) {
				cv::imwrite("intensity-" + IO::numberToString(ts) + ".png",
						intensity);
			}
			if (color.data != 0) {
				cv::imwrite("color-" + IO::numberToString(ts) + ".png", color);
			}
			if (depth.data != 0) {
				cv::imwrite("depth-" + IO::numberToString(ts) + ".png", depth);
			}
		}

		if (intensity.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Intensity", intensity);
		}
		if (color.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Color", color);
		}
		if (depth.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Depth", depth);
		}

	}

	std::cout << "Stop the capture..." << std::endl;
	sensor.stop();
	c.disconnect();
	c2.disconnect();
	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

