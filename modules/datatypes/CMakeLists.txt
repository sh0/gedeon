#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 05 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * This file is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The HVRL Engine Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/


SET(localname datatypes)

PROJECT(${PROJECT_NAME}_${localname})

SET(the_target "${PROJECT_NAME_SMALL}_${localname}")

INCLUDE_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/include")
INCLUDE_DIRECTORIES("${GEDEON_SOURCE_DIR}/modules/core/include/")
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${ZLIB_INCLUDE_DIRS})

SET(local_include_dir "${GEDEON_SOURCE_DIR}/modules/${localname}/include/${PROJECT_NAME_SMALL}/${localname}")

SET(lib_srcs src/depthimage.cpp src/pointcloud.cpp src/rgbimage.cpp src/size.cpp src/intensityimage.cpp src/io.cpp)
SET(lib_hdrs ${local_include_dir}/datatypes.hpp ${local_include_dir}/depthimage.hpp ${local_include_dir}/pointcloud.hpp 
	     ${local_include_dir}/rgbimage.hpp ${local_include_dir}/size.hpp 
	     ${local_include_dir}/rgbdimage.hpp ${local_include_dir}/intensityimage.hpp ${local_include_dir}/converter.hpp ${local_include_dir}/io.hpp)

SOURCE_GROUP("sources" FILES ${lib_srcs})
SOURCE_GROUP("headers" FILES ${lib_hdrs})

IF(GEDEON_BUILD_SHARED_LIBS)
	ADD_LIBRARY(${the_target} SHARED ${lib_srcs} ${lib_hdrs})
ELSE()
	ADD_LIBRARY(${the_target} STATIC ${lib_srcs} ${lib_hdrs})
ENDIF()

TARGET_LINK_LIBRARIES(${the_target} gedeon_core ${Boost_LIBRARIES} ${ZLIB_LIBRARIES})


INSTALL(TARGETS ${the_target}
	    RUNTIME DESTINATION ${GEDEON_BIN_INSTALL_PATH}
	    LIBRARY DESTINATION ${GEDEON_LIB_INSTALL_PATH}
	    ARCHIVE DESTINATION ${GEDEON_LIB_INSTALL_PATH})

INSTALL(FILES ${lib_hdrs} DESTINATION ${GEDEON_INCLUDE_INSTALL_PATH}/${PROJECT_NAME_SMALL}/${localname}/)

IF(${PROJECT_NAME}_BUILD_TEST AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/test)
	create_test(${localname} ${ARGN})
ENDIF()
