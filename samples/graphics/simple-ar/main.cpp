/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 11 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/log.hpp"
#include "gedeon/grabber/opencvgrabber.hpp"
#include "gedeon/grabber/opencvdriver.hpp"
#include "gedeon/graphics/vao.hpp"
#include "gedeon/graphics/glslshader.hpp"
#include "gedeon/calibration/tools.hpp"
#include "gedeon/calibration/intrinsic.hpp"
#include "gedeon/calibration/projectivecamera.hpp"
#include "gedeon/calibration/2d3d.hpp"

#include <cstdlib>

#include <cuda_runtime.h>
#include <cuda.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace gedeon;

static OpenCVGrabber sensor;

static unsigned int window_width = 800;
static unsigned int window_height = 600;

static bool running = true;
static bool calibrated = false;
static bool update_data = false;

/*
 * OpenGL
 */
static VAO fullscreen_quad_vao;
static VAO cube_vao;
static GLenum rendering_mode = GL_FILL;
static GLuint background_texture_pbos[2];
static GLuint background_texture = 0;
static GLuint texture_shader_object = 0;
static GLuint color_shader_object = 0;

/*
 * OpenGL matrices
 */
glm::mat4 projection_ortho = glm::mat4(1.0);
glm::mat4 modelview = glm::mat4(1.0);
glm::mat4 projection = glm::mat4(1.0);

/*
 * Calibration
 */
static cv::Size pattern_size(7, 4);
static std::vector<std::vector<cv::Point2f> > corners_calibration;
static std::vector<cv::Point3f> points3d;
static ProjectiveCamera calibration;
static float size_pattern_quad = 0.03f;

/*
 * Shaders
 */

static const char texture_vertex_shader[] =
		"#version 330 core\n\
#extension GL_EXT_gpu_shader4 : enable\n\
in vec3 vertexPositionIn;\n\
in vec2 vertexTextureIn;\n\
uniform mat4 mvpIn;\n\
invariant gl_Position;\n\
smooth out vec2 vertexTexOut;\n\
void main(void){\n\
	vertexTexOut = vertexTextureIn;\n\
	gl_Position = mvpIn*vec4(vertexPositionIn, 1.0);\n\
}";
static const char texture_fragment_shader[] =
		"#version 330 core\n\
#extension GL_EXT_gpu_shader4 : enable\n\
precision highp float;\n\
smooth in vec2 vertexTexOut;\n\
uniform sampler2D texture0In;\n\
out vec4 Color;\n\
void main(void){\n\
	 Color = vec4(texture2D(texture0In,vertexTexOut).rgb,1.0);\n\
}";
static const char color_vertex_shader[] =
		"#version 330 core\n\
#extension GL_EXT_gpu_shader4 : enable\n\
in vec3 vertexPositionIn;\n\
in vec4 vertexColorIn;\n\
uniform mat4 mvpIn;\n\
invariant gl_Position;\n\
smooth out vec4 colorOut;\n\
void main(void){\n\
	colorOut = vertexColorIn;\n\
	gl_Position = mvpIn*vec4(vertexPositionIn, 1.0);\n\
}";
static const char color_fragment_shader[] =
		"#version 330 core\n\
#extension GL_EXT_gpu_shader4 : enable\n\
precision highp float;\n\
smooth in vec4 colorOut;\n\
out vec4 Color;\n\
void main(void){\n\
	 Color = colorOut;\n\
}";

/*
 * Callback function called when a new image is received
 */
void captureImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				if (true == s->hasColor()) {
					update_data = true;
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}

/*
 * Initialize the vertex array objects
 */
static bool initVAO(void) {
	const float vertices[24] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			-size_pattern_quad, 0.0f, size_pattern_quad, -size_pattern_quad,
			0.0f, size_pattern_quad, 0.0f, size_pattern_quad, 0.0f, 0.0f,
			size_pattern_quad, 0.0f, -size_pattern_quad, size_pattern_quad,
			size_pattern_quad, -size_pattern_quad, size_pattern_quad,
			size_pattern_quad, 0.0f };
	const unsigned int vertex_indices[36] = { 0, 3, 2, 2, 1, 0, 5, 6, 7, 7, 4,
			5, 1, 2, 6, 6, 5, 1, 4, 7, 3, 3, 0, 4, 2, 3, 7, 7, 6, 2, 4, 5, 1, 1,
			0, 4 };
	const float colors[32] = { 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.5f,
			0.5f, 1.0f, };
	if (false
			== cube_vao.init(vertices, 8, vertex_indices, 36, 0, 0, colors,
					GL_STATIC_DRAW)) {
		return false;
	}
	return cube_vao.setProgramObject(color_shader_object);
}

/*
 * Initialize or update the vertex array objects for the fullscreen quad
 */
static void updateVAOFullScreenQuad(const unsigned int& width,
		const unsigned int& height) {

	const float vertices[12] = { 0.0f, 0.0f, 0.0f, 0.0f, float(height), 0.0f,
			float(width), float(height), 0.0f, float(width), 0.0f, 0.0f };
	if (false == fullscreen_quad_vao.isInitialized()) {
		const unsigned int vertex_indices[6] = { 0, 1, 2, 2, 3, 0 };
		const float texture_coordinates[12] = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
				1.0f, 1.0f, 0.0f };
		fullscreen_quad_vao.init(vertices, 4, vertex_indices, 6, 0,
				texture_coordinates, 0, GL_STATIC_DRAW);
		fullscreen_quad_vao.setProgramObject(texture_shader_object);
	} else {
		fullscreen_quad_vao.updateVertices(vertices, 4);
	}

}

/*
 * Load the GLSL shaders
 */
static bool loadShaders(void) {
	GLuint shaders[2] = { 0, 0 };
	shaders[0] = GLSLShader::loadVertexShader(texture_vertex_shader);
	if (false == GLSLShader::compile(shaders[0])) {
		return false;
	}
	shaders[1] = GLSLShader::loadFragmentShader(texture_fragment_shader);
	if (false == GLSLShader::compile(shaders[1])) {
		return false;
	}
	texture_shader_object = GLSLShader::link(shaders, 2);
	if (0 == texture_shader_object) {
		return false;
	}

	glDeleteShader(shaders[0]);
	glDeleteShader(shaders[1]);

	shaders[0] = GLSLShader::loadVertexShader(color_vertex_shader);
	if (false == GLSLShader::compile(shaders[0])) {
		return false;
	}
	shaders[1] = GLSLShader::loadFragmentShader(color_fragment_shader);
	if (false == GLSLShader::compile(shaders[1])) {
		return false;
	}
	color_shader_object = GLSLShader::link(shaders, 2);
	if (0 == texture_shader_object) {
		return false;
	}

	glDeleteShader(shaders[0]);
	glDeleteShader(shaders[1]);

	return true;
}

/*
 * Create the texture object of the background image and the Pixel buffer Object for fast transfers
 */
static bool loadTexture(void) {
	glGenTextures(1, &background_texture);
	if (0 == background_texture) {
		return false;
	}

	const unsigned int width_local = sensor.getImage()->color.getSize().width;
	const unsigned int height_local = sensor.getImage()->color.getSize().height;

	glBindTexture(GL_TEXTURE_2D, background_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width_local, height_local, 0, GL_RGB,
			GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (GL_NO_ERROR != glGetError()) {
		return false;
	}
	/*
	 * Initialize the Pixel Buffer Objects
	 */glGenBuffers(2, background_texture_pbos);
	if (0 == background_texture_pbos[0] || 0 == background_texture_pbos[1]) {
		return false;
	}
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, background_texture_pbos[0]);
	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width_local * height_local * 3, 0,
			GL_STREAM_DRAW_ARB);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, background_texture_pbos[1]);
	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width_local * height_local * 3, 0,
			GL_STREAM_DRAW_ARB);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

	if (GL_NO_ERROR != glGetError()) {
		return false;
	}
	return true;
}

/*
 * Callback function when the draw event is called
 */
static void display(void) {

	static std::vector<cv::Point2f> corners;
	static unsigned int copy_fbo = 0;
	static unsigned int read_fbo = 1;
	if (true == update_data) {
		corners.clear();
		const unsigned int width_local =
				sensor.getImage()->color.getSize().width;
		const unsigned int height_local =
				sensor.getImage()->color.getSize().height;
		cv::Mat image(height_local, width_local, CV_8UC3);
		memcpy(image.data, sensor.getImage()->color.getData().get(),
				width_local * height_local * 3);
		if (true == calibrated) {
			cv::Mat distorted_image;
			undistort(image, distorted_image, calibration.intrinsic_parameters,
					calibration.distortion_coefficients);
			distorted_image.copyTo(image);
		}
		bool detected = Tools::OpenCV::detectChessboard(corners, image,
				pattern_size);
		if (true == detected) {
			/*
			 * If the camera matrix is unknown the display corners, otherwise, compute the camera pose
			 */
			if (false == calibrated) {
				cv::drawChessboardCorners(image, pattern_size, corners,
						detected);
			} else {
				Calibration2D3D::calibrate(calibration, corners, points3d);
				cv::Mat opengl_modelview =
						Tools::OpenCV::convertOpenCVToOpenGLExtrinsic(
								calibration.extrinsic_parameters);
				modelview = glm::make_mat4((float*) opengl_modelview.data);
			}
		}else{
			corners.clear();
		}

		/*
		 * Copy the captured image in to the texture using two PBOs for async transfer
		 */
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,
				background_texture_pbos[read_fbo]);
		glBindTexture(GL_TEXTURE_2D, background_texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width_local, height_local,
				GL_RGB, GL_UNSIGNED_BYTE, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,
				background_texture_pbos[copy_fbo]);
		GLubyte* ptr = (GLubyte*) glMapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB,
				GL_WRITE_ONLY_ARB);
		memcpy(ptr, image.data, width_local * height_local * 3);
		glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

		++read_fbo %= 2;
		++copy_fbo %= 2;

		image.release();
		update_data = false;
	}


	glPointSize(3.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/*
	 * Draw the background image
	 */
	glDisable(GL_DEPTH_TEST);
	glBindTexture(GL_TEXTURE_2D, background_texture);
	glUseProgram(texture_shader_object);
	glUniformMatrix4fv(glGetUniformLocation(texture_shader_object, "mvpIn"), 1,
			GL_FALSE, glm::value_ptr(projection_ortho));
	glUniform1i(glGetUniformLocation(texture_shader_object, "texture0In"), 0);
	fullscreen_quad_vao.draw(GL_TRIANGLES);
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnable(GL_DEPTH_TEST);

	/*
	 * Draw cubes if the pattern is detected and calibration is available
	 */
	if (true == calibrated && 0 < corners.size()) {

		glPolygonMode(GL_FRONT_AND_BACK, rendering_mode);
		glUseProgram(color_shader_object);
		for (int i = 0; i < pattern_size.height + 1; ++i) {
			int start = 0;
			if (i % 2 == 1) {
				start = 1;
			}
			for (int j = start; j < pattern_size.width + 1; j += 2) {
				glUniformMatrix4fv(
						glGetUniformLocation(color_shader_object, "mvpIn"), 1,
						GL_FALSE,
						glm::value_ptr(
								projection * modelview
										* glm::translate(
												glm::vec3(
														(j - 1)
																* size_pattern_quad,
														(i - 1)
																* size_pattern_quad,
														0.0f))));
				cube_vao.draw(GL_TRIANGLES);
			}

		}
		glUseProgram(0);
	}

	glutSwapBuffers();
}



/*
 * Callback function called when the window is resized
 */
static void reshape(int width, int height) {
	glViewport(0, 0, width, height);
	window_width = width;
	window_height = height;
	projection_ortho = glm::ortho(0.0f, float(width), float(height), 0.0f,
			-1.0f, 1.0f);
	updateVAOFullScreenQuad(window_width, window_height);
}

/*
 * Callback function called when a key is pressed
 */
static void keyboard(unsigned char c, int mousex, int mousey) {

	switch (c) {
	case 'h': {
		std::cout << "Help" << std::endl;
		std::cout << "------------" << std::endl;
		std::cout << "q: quit the program" << std::endl;
		std::cout << "c: Grab points from the pattern for the calibration"
				<< std::endl;
		std::cout
				<< "C: Calibrate intrinsic and distortion parameters, then activate the tracking"
				<< std::endl;
		std::cout << "w: Change the rendering mode" << std::endl;
		std::cout << "s: Save data of the calibration into a file" << std::endl;
		std::cout << "s: Load data of the calibration from a file" << std::endl;
		std::cout << "Usage: graphics-simple-ar" << std::endl;
		std::cout << " -w <board_width>" << std::endl;
		std::cout << " -h <board_height>" << std::endl;
		std::cout << " -s <squareSize>" << std::endl;
		break;
	}
	case 'q':
	case 27: {
		running = false;
		break;
	}
	case 'c': {
		const unsigned int width_local =
				sensor.getImage()->color.getSize().width;
		const unsigned int height_local =
				sensor.getImage()->color.getSize().height;
		cv::Mat image(height_local, width_local, CV_8UC3);
		memcpy(image.data, sensor.getImage()->color.getData().get(),
				width_local * height_local * 3);
		std::vector<cv::Point2f> corners;
		if (false
				== Tools::OpenCV::detectChessboard(corners, image,
						pattern_size)) {
			Log::add().error("Detect Chessboard",
					"failed to detect the chessboard");
		} else {
			corners_calibration.push_back(corners);
			gedeon::Log::add().info("Captured point set");
		}
		image.release();
		break;
	}

	case 'C': {
		if (0 < corners_calibration.size()) {
			std::vector<std::vector<cv::Point3f> > points3d_calibration;
			for (unsigned int i = 0; i < corners_calibration.size(); ++i) {
				points3d_calibration.push_back(points3d);
			}
			if (false
					== Intrinsic::calibrateWithDistortion(
							calibration.intrinsic_parameters,
							calibration.distortion_coefficients,
							corners_calibration, points3d_calibration,
							sensor.getImage()->color.getSize(), false)) {
				Log::add().error("Calibration",
						"failed to calibrate the camera");
			} else {
				const unsigned int width_local =
						sensor.getImage()->color.getSize().width;
				const unsigned int height_local =
						sensor.getImage()->color.getSize().height;
				cv::Mat opengl_projection =
						Tools::OpenCV::convertOpenCVToOpenGLIntrinsic(
								calibration.intrinsic_parameters, width_local,
								height_local, 0.01f, 10.0f);
				projection = glm::make_mat4((float*) opengl_projection.data);
				gedeon::Log::add().info("Calibration OK");
				calibrated = true;
			}
			for (unsigned int i = 0; i < corners_calibration.size(); ++i) {
				corners_calibration[i].clear();
			}
			corners_calibration.clear();
		} else {
			Log::add().error("Calibration",
					"Points from the pattern need to be captured (key 'c')");
		}
		break;
	}
	case 'w': {
		if (GL_FILL == rendering_mode) {
			rendering_mode = GL_POINT;
			gedeon::Log::add().info("Set rendering mode to POINTS");
		} else {
			if (GL_POINT == rendering_mode) {
				rendering_mode = GL_LINE;
				gedeon::Log::add().info("Set rendering mode to LINES");
			} else {
				rendering_mode = GL_FILL;
				gedeon::Log::add().info("Set rendering mode to FILL");
			}
		}
		break;
	}

	case 's': {
		if (true == calibrated) {
			calibration.save("calibration.yml");
		}
		break;
	}

	case 'l': {
		if (true == calibration.load("calibration.yml")) {
			const unsigned int width_local =
					sensor.getImage()->color.getSize().width;
			const unsigned int height_local =
					sensor.getImage()->color.getSize().height;
			cv::Mat opengl_projection =
					Tools::OpenCV::convertOpenCVToOpenGLIntrinsic(
							calibration.intrinsic_parameters, width_local,
							height_local, 0.01f, 10.0f);
			projection = glm::make_mat4((float*) opengl_projection.data);
			gedeon::Log::add().info("Calibration loaded");
			calibrated = true;
		}
		break;
	}

	}
}

/*
 * Clean the data structures
 */
void clean(void) {
	corners_calibration.clear();
	points3d.clear();
	glDeleteTextures(1, &background_texture);
	glDeleteBuffers(2, background_texture_pbos);
	glDeleteProgram(texture_shader_object);
	glDeleteProgram(color_shader_object);
}


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	/*
	 * Check program parameters
	 */
	 for(int i = 1; i < argc; i++ ){
		 if("-w" == std::string(argv[i])){
			 ++i;
			 if( i < argc){
				 pattern_size.width = atoi(argv[i]);
			 }
			 continue;
		 }
		 if("-h" == std::string(argv[i])){
			 ++i;
			 if( i < argc){
				 pattern_size.height = atoi(argv[i]);
			 }
			 continue;
		 }
		 if("-s" == std::string(argv[i])){
			 ++i;
			 if( i < argc){
				size_pattern_quad = atof(argv[i]);
			 }
			 continue;
		 }
	 }

	/*
	 * Connect to the camera
	 */
	OpenCVDriver& driver = OpenCVDriver::getInstance();

	Log::add().info("Looking for " + driver.getName() + " devices...");
	driver.refresh();
	if (driver.getCount() == 0) {
		Log::add().error("", "No camera available");
		return EXIT_FAILURE;
	}
	Log::add().info("Access the first camera...");
	if (false == sensor.init(0, &driver)) {
		Log::add().error("", "Failed to connect to the camera");
		return EXIT_FAILURE;
	}

	/*
	 * Initialize the OpenGL context
	 */
	Log::add().info("Initialize OpenGL rendering context...");
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(window_width, window_height);
	int window_id = glutCreateWindow("OpenGL viewer");
	if (window_id == 0) {
		gedeon::Log::add().error("", "Failed to create the window");
		return EXIT_FAILURE;
	}

	GLenum err = glewInit();
	if (GLEW_OK != err) {
		gedeon::Log::add().error("",
				"Failed to initialize glew: "
						+ std::string((char*) glewGetErrorString(err)));
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	if (false == GLSLShader::checkAvailability()) {
		gedeon::Log::add().error("", "Shaders are not available");
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	if (false == loadShaders()) {
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	if (false == loadTexture()) {
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	if (false == initVAO()) {
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	boost::signals2::connection c;
	try {
		if (false
				== sensor.connect("updated", boost::bind(captureImage, _1),
						c)) {

			gedeon::Log::add().error("",
					"Cannot connect to the event of the sensor");
			glutDestroyWindow(window_id);
			return EXIT_FAILURE;
		}
	} catch (Exception e) {
		gedeon::Log::add().error("", e.errorString());
		glutDestroyWindow(window_id);
		return EXIT_FAILURE;
	}

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);

	Timer t;
	t.start();
	long int previous_time = t.get();

	/*
	 * Initialize 3D points of the pattern
	 */
	for (int i = 0; i < pattern_size.height; ++i) {
		for (int j = 0; j < pattern_size.width; ++j) {
			points3d.push_back(
					cv::Point3f(j * size_pattern_quad, i * size_pattern_quad,
							0.0f));
		}
	}

	sensor.play();

	while (true == running) {
		glutMainLoopEvent();
		if (t.get() - previous_time > 34) {
			glutPostRedisplay();
			previous_time = t.get();
		}
		millisecSleep(5);
	}

	for (unsigned int i = 0; i < corners_calibration.size(); ++i) {
		corners_calibration[i].clear();
	}
	corners_calibration.clear();

	sensor.stop();
	t.stop();

	clean();

	glutDestroyWindow(window_id);

	Log::add().info("Exiting...");
	return EXIT_SUCCESS;
}

