/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * io.cpp created in 11 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * io.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "gedeon/datatypes/io.hpp"
#include "gedeon/core/log.hpp"
#include <algorithm>
#include <stdint.h>
#include <zlib.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

static const int defaut_compression_level = 4;

namespace gedeon {

namespace IO {

const char* IMPORTEXPORT_FILENAME_EXTENSION = ".gmv";
const char* SAVERGB_FILENAME_EXTENSION = ".png";
const char* SAVEDEPTH_FILENAME_EXTENSION = ".yml";
const char* SAVEINTENSITY_FILENAME_EXTENSION = ".png";
const char* FOLDER_COLOR = "color/";
const char* FOLDER_DEPTH = "depth/";
const char* FOLDER_INTENSITY = "intensity/";

/*
 * DATAEXPORTER CLASS
 */

DataExporter::DataExporter(void) :
		compression_level(defaut_compression_level), nb_frames(0) {

}

DataExporter::~DataExporter(void) {
	this->close();
}

bool DataExporter::open(const std::string& filename, const int& level) {

	std::string name = filename;
	std::string namelower = filename;
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::IMPORTEXPORT_FILENAME_EXTENSION)) {
		name.append(IMPORTEXPORT_FILENAME_EXTENSION);
	}
	this->output_file.open(name.c_str(), std::ios::out | std::ios::binary);
	if (this->output_file.is_open() == false) {
		Log::add().error("DataExporter::open", "Unable to open file " + name);
		return false;
	}
	this->compression_level = level;
	this->nb_frames = 0;
	this->output_file.seekp(0);
	this->output_file.write(reinterpret_cast<const char *>(&(this->nb_frames)),
			sizeof(uint64_t));
	return true;
}

void DataExporter::close(void) {
	std::ifstream::pos_type pos = this->output_file.tellp();
	this->output_file.seekp(0);
	this->output_file.write(reinterpret_cast<const char *>(&(this->nb_frames)),
			sizeof(uint64_t));
	this->output_file.seekp(pos);
	this->output_file.close();
}

bool DataExporter::write(const RGBDIImagePtr& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = img.get()->color.getSize().width;
	unsigned int height_color = img.get()->color.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = img.get()->depth.getSize().width;
	unsigned int height_depth = img.get()->depth.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = img.get()->intensity.getSize().width;
	unsigned int height_intensity = img.get()->intensity.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t color_size = width_color * height_color * 3;
	size_t depth_size = width_depth * height_depth * sizeof(float);
	size_t intensity_size = width_intensity * height_intensity;
	size_t source_size = color_size + depth_size + intensity_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source, img.get()->color.getData().get(), color_size);
	memcpy(source + color_size,
			reinterpret_cast<const unsigned char *>(img.get()->depth.getData().get()),
			depth_size);
	memcpy(source + color_size + depth_size,
			img.get()->intensity.getData().get(), intensity_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const RGBDImagePtr& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = img.get()->color.getSize().width;
	unsigned int height_color = img.get()->color.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = img.get()->depth.getSize().width;
	unsigned int height_depth = img.get()->depth.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = 0;
	unsigned int height_intensity = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t color_size = width_color * height_color * 3;
	size_t depth_size = width_depth * height_depth * sizeof(float);
	size_t intensity_size = width_intensity * height_intensity;
	size_t source_size = color_size + depth_size + intensity_size;
	unsigned char * source = new unsigned char[source_size];
	memcpy(source, img.get()->color.getData().get(), color_size);
	memcpy(source + color_size,
			reinterpret_cast<const unsigned char *>(img.get()->depth.getData().get()),
			depth_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const RGBIImagePtr& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = img.get()->color.getSize().width;
	unsigned int height_color = img.get()->color.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = 0;
	unsigned int height_depth = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = img.get()->intensity.getSize().width;
	unsigned int height_intensity = img.get()->intensity.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t color_size = width_color * height_color * 3;
	size_t intensity_size = width_intensity * height_intensity;
	size_t source_size = color_size + intensity_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source, img.get()->color.getData().get(), color_size);
	memcpy(source + color_size, img.get()->intensity.getData().get(),
			intensity_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const DIImagePtr& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = 0;
	unsigned int height_color = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = img.get()->depth.getSize().width;
	unsigned int height_depth = img.get()->depth.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = img.get()->intensity.getSize().width;
	unsigned int height_intensity = img.get()->intensity.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t depth_size = width_depth * height_depth * sizeof(float);
	size_t intensity_size = width_intensity * height_intensity;
	size_t source_size = depth_size + intensity_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source,
			reinterpret_cast<const unsigned char *>(img.get()->depth.getData().get()),
			depth_size);
	memcpy(source + depth_size, img.get()->intensity.getData().get(),
			intensity_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const RGBImage& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = img.getSize().width;
	unsigned int height_color = img.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = 0;
	unsigned int height_depth = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = 0;
	unsigned int height_intensity = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t color_size = width_color * height_color * 3;
	size_t source_size = color_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source, img.getData().get(), color_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const DepthImage& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = 0;
	unsigned int height_color = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = img.getSize().width;
	unsigned int height_depth = img.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = 0;
	unsigned int height_intensity = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t depth_size = width_depth * height_depth;
	size_t source_size = depth_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source, img.getData().get(), depth_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

bool DataExporter::write(const IntensityImage& img, const float& framerate) {

	if (false == this->output_file.is_open()) {
		Log::add().error("DataExporter::write", "File is not opened yet");
		return false;
	}

	int nb_images = 3;
	this->output_file.write(reinterpret_cast<const char *>(&nb_images),
			sizeof(int));
	unsigned int width_color = 0;
	unsigned int height_color = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_color),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_color),
			sizeof(unsigned int));
	unsigned int width_depth = 0;
	unsigned int height_depth = 0;
	this->output_file.write(reinterpret_cast<const char *>(&width_depth),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_depth),
			sizeof(unsigned int));
	unsigned int width_intensity = img.getSize().width;
	unsigned int height_intensity = img.getSize().height;
	this->output_file.write(reinterpret_cast<const char *>(&width_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&height_intensity),
			sizeof(unsigned int));
	this->output_file.write(reinterpret_cast<const char *>(&framerate),
			sizeof(float));

	size_t intensity_size = width_intensity * height_intensity;
	size_t source_size = intensity_size;
	unsigned char * source = new unsigned char[source_size];

	memcpy(source, img.getData().get(), intensity_size);

	compressAndStore(source, source_size);
	delete[] source;
	return true;
}

void DataExporter::compressAndStore(const unsigned char* source,
		const size_t& source_size) {
	size_t compress_size = compressBound(source_size);
	unsigned char * output = new unsigned char[compress_size];
	if (Z_OK
			== compress2(output, (uLongf *)&compress_size, source, source_size,
					this->compression_level)) {
		this->output_file.write(reinterpret_cast<const char *>(&source_size),
				sizeof(uint64_t));
		this->output_file.write(reinterpret_cast<const char *>(&compress_size),
				sizeof(uint64_t));
		this->output_file.write(reinterpret_cast<const char *>(output),
				compress_size);
	} else {
		this->output_file.write(reinterpret_cast<const char *>(&source_size),
				sizeof(uint64_t));
		this->output_file.write(reinterpret_cast<const char *>(&source_size),
				sizeof(uint64_t));
		this->output_file.write(reinterpret_cast<const char *>(source),
				source_size);
	}
	delete[] output;

	this->nb_frames++;
	std::ifstream::pos_type pos = this->output_file.tellp();
	this->output_file.clear();
	this->output_file.seekp(0);
	this->output_file.write(reinterpret_cast<const char *>(&(this->nb_frames)),
			sizeof(uint64_t));
	this->output_file.seekp(pos);
}

/*
 * DATAIMPORTER CLASS
 */

DataImporter::DataImporter(void) :
		nb_frames(0), current_frame(0), frame_start(0), loop(true), finished(
				false) {

}

DataImporter::~DataImporter(void) {
	this->close();
}

bool DataImporter::open(const std::string& filename) {
	std::string name = filename;
	std::string namelower = filename;
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::IMPORTEXPORT_FILENAME_EXTENSION)) {
		name.append(IMPORTEXPORT_FILENAME_EXTENSION);
	}
	this->input_file.open(name.c_str(), std::ios::in | std::ios::binary);
	if (this->input_file.is_open() == false || this->input_file.eof()) {
		Log::add().error("DataImporter::open", "Unable to open file " + name);
		return false;
	}
		this->input_file.read(reinterpret_cast<char *>(&(this->nb_frames)),
			sizeof(uint64_t));
	this->current_frame = 0;
	this->frame_start = this->input_file.tellg();
	return true;
}

void DataImporter::close(void) {
	this->input_file.close();
}

void DataImporter::triggerLoop(void) {
	this->loop = !this->loop;
	if (true == this->loop) {
		this->finished = false;
	}
}

bool DataImporter::read(RGBDIImagePtr& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			Log::add().error("DataImporter::read",
				"Unable to read the header");
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);

		if (color_size.width * color_size.height > 0) {
			img.get()->color.setSize(color_size);
			img.get()->color.setData(source);
		}
		if (depth_size.width * depth_size.height > 0) {
			img.get()->depth.setSize(depth_size);
			unsigned char *tmp = source
					+ color_size.width * color_size.height * 3;
			img.get()->depth.setData(reinterpret_cast<const float *>(tmp));
		}
		if (intensity_size.width * intensity_size.height > 0) {
			img.get()->intensity.setSize(intensity_size);
			img.get()->intensity.setData(
					source + color_size.width * color_size.height
							+ depth_size.width * depth_size.height
									* sizeof(float));
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

bool DataImporter::read(RGBIImagePtr& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);
		if (color_size.width * color_size.height > 0) {
			img.get()->color.setSize(color_size);
			img.get()->color.setData(source);
		}
		if (intensity_size.width * intensity_size.height > 0) {
			img.get()->intensity.setSize(intensity_size);
			img.get()->intensity.setData(
					source + color_size.width * color_size.height * 3);
		}
		++this->current_frame;

		delete[] source;
	}
	return true;
}

bool DataImporter::read(RGBDImagePtr& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);
		if (color_size.width * color_size.height > 0) {
			img.get()->color.setSize(color_size);
			img.get()->color.setData(source);
		}
		if (depth_size.width * depth_size.height > 0) {
			img.get()->depth.setSize(depth_size);
			img.get()->depth.setData(
					reinterpret_cast<const float *>(source
							+ color_size.width * color_size.height * 3));
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

bool DataImporter::read(DIImagePtr& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);

		if (depth_size.width * depth_size.height > 0) {
			img.get()->depth.setSize(depth_size);
			img.get()->depth.setData(reinterpret_cast<const float *>(source));
		}
		if (intensity_size.width * intensity_size.height > 0) {
			img.get()->intensity.setSize(intensity_size);
			img.get()->intensity.setData(
					source
							+ depth_size.width * depth_size.height
									* sizeof(float));
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

bool DataImporter::read(RGBImage& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);
		if (color_size.width * color_size.height > 0) {
			img.setSize(color_size);
			img.setData(source);
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

bool DataImporter::read(DepthImage& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}
	if (false == this->finished) {

		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);

		if (depth_size.width * depth_size.height > 0) {
			img.setSize(depth_size);
			img.setData(reinterpret_cast<const float *>(source));
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

bool DataImporter::read(IntensityImage& img, float& framerate) {

	if (false == this->input_file.is_open() || this->nb_frames == 0) {
		Log::add().error("DataImporter::read",
				"File is not opened yet or empty");
		return false;
	}

	if (this->input_file.eof() || this->current_frame >= this->nb_frames) {
		this->reset();
		if (false == this->loop) {
			this->finished = true;
		}
	}

	if (false == this->finished) {
		Size color_size, depth_size, intensity_size;
		size_t size_data, size_stored;
		if (false
				== getHeaderData(color_size, depth_size, intensity_size,
						framerate, size_data, size_stored)) {
			this->reset();
			return false;
		}

		unsigned char * source = new unsigned char[size_data];

		decompressAndStore(source, size_stored, size_data);

		if (intensity_size.width * intensity_size.height > 0) {
			img.setSize(intensity_size);
			img.setData(source);
		}
		++this->current_frame;
		delete[] source;
	}
	return true;
}

void DataImporter::decompressAndStore(unsigned char* source,
		size_t& size_stored, size_t& size_data) {
	if (size_data == size_stored) {
		this->input_file.read(reinterpret_cast<char *>(source), size_stored);
	} else {
		unsigned char * source_tmp = new unsigned char[size_stored];
		this->input_file.read(reinterpret_cast<char *>(source_tmp),
				size_stored);
		uncompress(source, (uLongf *)&size_data, source_tmp, size_stored);

		delete[] source_tmp;
	}
}

bool DataImporter::getHeaderData(Size& color_size, Size& depth_size,
		Size& intensity_size, float& framerate, size_t& size_data,
		size_t& size_stored) {
	int nb_images = 0;
	this->input_file.read(reinterpret_cast<char *>(&nb_images), sizeof(int));
	if (nb_images != 3) {
		Log::add().error("DataImporter::getHeaderData",
				"Number of images is not 3");
		return false;
	}
	this->input_file.read(reinterpret_cast<char *>(&color_size.width),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&color_size.height),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&depth_size.width),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&depth_size.height),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&intensity_size.width),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&intensity_size.height),
			sizeof(unsigned int));
	this->input_file.read(reinterpret_cast<char *>(&framerate), sizeof(float));
	this->input_file.read(reinterpret_cast<char *>(&size_data),
			sizeof(uint64_t));
	this->input_file.read(reinterpret_cast<char *>(&size_stored),
			sizeof(uint64_t));

	return true;
}

/*
 * DATALOADER CLASS
 */

class DataLoaderPimpl {
public:
	DataLoaderPimpl(void) :
			nb_frames(0), current_frame(0), loop(true), open_stream(false) {
	}
	~DataLoaderPimpl(void) {
		this->file_list_color.clear();
		this->file_list_depth.clear();
		this->file_list_intensity.clear();
		this->file_list_local.clear();
	}

	std::vector<boost::filesystem::path> file_list_color;
	std::vector<boost::filesystem::path> file_list_depth;
	std::vector<boost::filesystem::path> file_list_intensity;
	std::vector<boost::filesystem::path> file_list_local;
	unsigned int nb_frames;
	unsigned int current_frame;
	bool loop;
	bool open_stream;
};

DataLoader::DataLoader(void) :
		pimpl(new DataLoaderPimpl()) {
}

DataLoader::~DataLoader(void) {
	this->close();
}

bool DataLoader::open(const std::string& directory) {

	this->close();
	this->pimpl->nb_frames = 0;
	this->pimpl->current_frame = 0;
	bool valid = false;

	std::string source_directory(directory);
	if (source_directory[source_directory.size() - 1] != '/') {
		source_directory += "/";
	}

	if (false == is_directory(source_directory)) {
		return false;
	} else {
		path my_dir(source_directory);
		directory_iterator end;
		for (directory_iterator iter(my_dir); iter != end; ++iter) {
			if (false == is_directory(*iter)) {
				this->pimpl->file_list_local.push_back(*iter);
			}
		}
		if (this->pimpl->file_list_local.size() > 0) {
			sort(this->pimpl->file_list_local.begin(),
					this->pimpl->file_list_local.end());
			this->pimpl->nb_frames = this->pimpl->file_list_local.size();
			valid = true;
		}
	}

	if (false == valid) {
		if (is_directory(source_directory + FOLDER_COLOR)) {
			path my_dir(source_directory + FOLDER_COLOR);
			directory_iterator end;
			for (directory_iterator iter(my_dir); iter != end; ++iter) {
				if (false == is_directory(*iter)) {
					this->pimpl->file_list_color.push_back(*iter);
				}
			}
			if (this->pimpl->file_list_color.size() > 0) {
				sort(this->pimpl->file_list_color.begin(),
						this->pimpl->file_list_color.end());
				valid |= true;
			}
		}
		if (is_directory(source_directory + FOLDER_DEPTH)) {
			path my_dir(source_directory + FOLDER_DEPTH);
			directory_iterator end;
			for (directory_iterator iter(my_dir); iter != end; ++iter) {
				if (false == is_directory(*iter)) {
					this->pimpl->file_list_depth.push_back(*iter);
				}
			}
			if (this->pimpl->file_list_depth.size() > 0) {
				sort(this->pimpl->file_list_depth.begin(),
						this->pimpl->file_list_depth.end());
				valid |= true;
			}
		}
		if (is_directory(source_directory + FOLDER_INTENSITY)) {
			path my_dir(source_directory + FOLDER_INTENSITY);
			directory_iterator end;
			for (directory_iterator iter(my_dir); iter != end; ++iter) {
				if (false == is_directory(*iter)) {
					this->pimpl->file_list_intensity.push_back(*iter);
				}
			}
			if (this->pimpl->file_list_intensity.size() > 0) {
				sort(this->pimpl->file_list_intensity.begin(),
						this->pimpl->file_list_intensity.end());
				valid |= true;
			}
		}

		if (true == valid) {
			unsigned int minimum[3] = { this->pimpl->file_list_color.size(),
					this->pimpl->file_list_depth.size(),
					this->pimpl->file_list_intensity.size() };
			unsigned int max_local = std::max(minimum[0],
					std::max(minimum[1], minimum[2]));

			if (0 == minimum[0]) {
				minimum[0] = max_local + 1;
			}
			if (0 == minimum[1]) {
				minimum[1] = max_local + 1;
			}
			if (0 == minimum[2]) {
				minimum[2] = max_local + 1;
			}
			unsigned int min_local = std::min(minimum[0],
					std::min(minimum[1], minimum[2]));
			this->pimpl->nb_frames = min_local;
		}
	}
	this->pimpl->open_stream = true;
	return valid;
}

void DataLoader::close(void) {
	this->pimpl->file_list_local.clear();
	this->pimpl->file_list_color.clear();
	this->pimpl->file_list_depth.clear();
	this->pimpl->file_list_intensity.clear();
	this->pimpl->open_stream = false;
}

unsigned int DataLoader::getTotalFrames(void) const {
	return this->pimpl->nb_frames;
}

unsigned int DataLoader::getCurrentFrame(void) const {
	return this->pimpl->current_frame;
}

void DataLoader::setCurrentFrame(const unsigned int& frame_index) {
	this->pimpl->current_frame = frame_index;
	if (this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
}

void DataLoader::triggerLoop(void) {
	this->pimpl->loop = !this->pimpl->loop;
}

void DataLoader::reset(void) {
	this->pimpl->current_frame = 0;
}

bool DataLoader::isOpened(void) const {
	return this->pimpl->open_stream;
}

bool DataLoader::load(RGBDIImagePtr& img) {

	if (this->pimpl->nb_frames == 0
			|| (this->pimpl->file_list_color.size() == 0
					&& this->pimpl->file_list_depth.size() == 0
					&& this->pimpl->file_list_intensity.size() == 0)) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		bool val = false;
		if (this->pimpl->file_list_color.size() > 0) {
			val |=
					loadColorImage(img.get()->color,
							this->pimpl->file_list_color[this->pimpl->current_frame].string());
		}
		if (this->pimpl->file_list_depth.size() > 0) {
			val |=
					loadDepthImage(img.get()->depth,
							this->pimpl->file_list_depth[this->pimpl->current_frame].string());
		}
		if (this->pimpl->file_list_intensity.size() > 0) {
			val |=
					loadIntensityImage(img.get()->intensity,
							this->pimpl->file_list_intensity[this->pimpl->current_frame].string());
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(RGBDImagePtr& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() > 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		bool val = false;
		if (this->pimpl->file_list_color.size() > 0) {
			val |=
					loadColorImage(img.get()->color,
							this->pimpl->file_list_color[this->pimpl->current_frame].string());
		}
		if (this->pimpl->file_list_depth.size() > 0) {
			val |=
					loadDepthImage(img.get()->depth,
							this->pimpl->file_list_depth[this->pimpl->current_frame].string());
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(DIImagePtr& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() > 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		bool val = false;
		if (this->pimpl->file_list_depth.size() > 0) {
			val |=
					loadDepthImage(img.get()->depth,
							this->pimpl->file_list_depth[this->pimpl->current_frame].string());
		}
		if (this->pimpl->file_list_intensity.size() > 0) {
			val |=
					loadIntensityImage(img.get()->intensity,
							this->pimpl->file_list_intensity[this->pimpl->current_frame].string());
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(RGBIImagePtr& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() > 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		bool val = false;
		if (this->pimpl->file_list_color.size() > 0) {
			val |=
					loadColorImage(img.get()->color,
							this->pimpl->file_list_color[this->pimpl->current_frame].string());
		}
		if (this->pimpl->file_list_intensity.size() > 0) {
			val |=
					loadIntensityImage(img.get()->intensity,
							this->pimpl->file_list_intensity[this->pimpl->current_frame].string());
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(RGBImage& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() == 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		if (false
				== loadColorImage(img,
						this->pimpl->file_list_local[this->pimpl->current_frame].string())) {
			return false;
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(DepthImage& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() == 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		if (false
				== loadDepthImage(img,
						this->pimpl->file_list_local[this->pimpl->current_frame].string())) {
			return false;
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

bool DataLoader::load(IntensityImage& img) {

	if (this->pimpl->nb_frames == 0
			|| this->pimpl->file_list_local.size() == 0) {
		return false;
	}

	if (this->pimpl->current_frame < this->pimpl->nb_frames) {
		if (false
				== loadIntensityImage(img,
						this->pimpl->file_list_local[this->pimpl->current_frame].string())) {
			return false;
		}
	}

	this->pimpl->current_frame++;
	if (true == this->pimpl->loop
			&& this->pimpl->current_frame >= this->pimpl->nb_frames) {
		this->pimpl->current_frame = 0;
	}
	return true;
}

/*
 * DATASAVER CLASS
 */

DataSaver::DataSaver(void) :
		created_directory(false), created_color_directory(0), created_depth_directory(
				0), created_intensity_directory(0), open_stream(false) {
}

DataSaver::~DataSaver(void) {
	this->close();
}

bool DataSaver::open(const std::string& directory) {

	if (directory == "") {
		return false;
	}

	this->close();

	if (false == is_directory(directory)) {
		if (false == create_directory(directory)) {
			return false;
		}
	}
	this->created_directory = true;
	this->destination_directory = directory;
	if (this->destination_directory[this->destination_directory.size() - 1]
			!= '/') {
		this->destination_directory += "/";
	}
	this->open_stream = true;
	return true;
}

void DataSaver::close(void) {
	this->open_stream = true;
}

bool DataSaver::save(const std::string& filename, const RGBDIImagePtr& img) {
	if (false == checkSubDirectories(true, true, true)) {
		return false;
	}
	bool ret = saveColorImage(img.get()->color,
			this->destination_directory + FOLDER_COLOR + filename);
	ret |= saveDepthImage(img.get()->depth,
			this->destination_directory + FOLDER_DEPTH + filename);
	ret |= saveIntensityImage(img.get()->intensity,
			this->destination_directory + FOLDER_INTENSITY + filename);
	return ret;
}

bool DataSaver::save(const std::string& filename, const RGBDImagePtr& img) {
	if (false == checkSubDirectories(true, true, false)) {
		return false;
	}
	bool ret = saveColorImage(img.get()->color,
			this->destination_directory + FOLDER_COLOR + filename);
	ret |= saveDepthImage(img.get()->depth,
			this->destination_directory + FOLDER_DEPTH + filename);
	return ret;
}

bool DataSaver::save(const std::string& filename, const RGBIImagePtr& img) {
	if (false == checkSubDirectories(true, false, true)) {
		return false;
	}
	bool ret = saveColorImage(img.get()->color,
			this->destination_directory + FOLDER_COLOR + filename);
	ret |= saveIntensityImage(img.get()->intensity,
			this->destination_directory + FOLDER_INTENSITY + filename);
	return ret;
}

bool DataSaver::save(const std::string& filename, const DIImagePtr& img) {
	if (false == checkSubDirectories(false, true, true)) {
		return false;
	}
	bool ret = saveDepthImage(img.get()->depth,
			this->destination_directory + FOLDER_DEPTH + filename);
	ret |= saveIntensityImage(img.get()->intensity,
			this->destination_directory + FOLDER_INTENSITY + filename);
	return ret;
}

bool DataSaver::save(const std::string& filename, const RGBImage& img) {
	return saveColorImage(img, this->destination_directory + filename);
}

bool DataSaver::save(const std::string& filename, const DepthImage& img) {
	return saveDepthImage(img, this->destination_directory + filename);
}

bool DataSaver::save(const std::string& filename, const IntensityImage& img) {
	return saveIntensityImage(img, this->destination_directory + filename);
}

bool DataSaver::checkSubDirectories(const bool& color, const bool& depth,
		const bool& intensity) {
	if (true == color && false == this->created_color_directory) {
		if (false
				== create_directory(
						this->destination_directory + FOLDER_COLOR)) {
			return false;
		}
		this->created_color_directory = true;
	}
	if (true == depth && false == this->created_depth_directory) {
		if (false
				== create_directory(
						this->destination_directory + FOLDER_DEPTH)) {
			return false;
		}
		this->created_depth_directory = true;
	}
	if (true == intensity && false == this->created_intensity_directory) {
		if (false
				== create_directory(
						this->destination_directory + FOLDER_INTENSITY)) {
			return false;
		}
		this->created_intensity_directory = true;
	}
	return true;
}

/*
 * VARIOUS FUNCTIONS
 */

bool saveColorImage(const RGBImage& input, const std::string& filename) {

	if (input.getSize().width == 0 || input.getSize().height == 0
			|| input.getData().get() == 0 || filename == "") {
		return false;
	}

	cv::Mat img(cv::Size(input.getSize().width, input.getSize().height),
			CV_8UC3);
	unsigned char *data = input.getData().get();
	unsigned int local_size = input.getSize().width * input.getSize().height
			* 3;
	memcpy(img.data, data, local_size * sizeof(unsigned char));
	cv::cvtColor(img, img, CV_BGR2RGB);

	bool val = false;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVERGB_FILENAME_EXTENSION)) {
		val = cv::imwrite(filename + SAVERGB_FILENAME_EXTENSION, img);
	} else {
		val = cv::imwrite(filename, img);

	}
	img.release();

	return val;
}

bool loadColorImage(RGBImage& output, const std::string& filename) {

	if (filename == "" && false == is_regular_file(filename)) {
		return false;
	}
	cv::Mat img;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVERGB_FILENAME_EXTENSION)) {
		img = cv::imread(filename + SAVERGB_FILENAME_EXTENSION,
				CV_LOAD_IMAGE_COLOR);
	} else {
		img = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
	}
	if (img.empty() == true) {
		return false;
	}
	cv::cvtColor(img, img, CV_BGR2RGB);
	output.setSize(Size(img.size().width, img.size().height));
	output.setData(img.data);
	img.release();
	return true;
}

bool saveDepthImage(const DepthImage& input, const std::string& filename) {

	if (input.getSize().width == 0 || input.getSize().height == 0
			|| input.getData().get() == 0 || filename == "") {
		return false;
	}
	cv::Mat mat(cv::Size(input.getSize().width, input.getSize().height),
			CV_32F);
	float * data = input.getData().get();
	unsigned int local_size = input.getSize().width * input.getSize().height;
	memcpy(mat.data, data, local_size * sizeof(float));

	cv::FileStorage fs;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVEDEPTH_FILENAME_EXTENSION)) {
		fs.open(filename + SAVEDEPTH_FILENAME_EXTENSION,
				cv::FileStorage::WRITE);
	} else {
		fs.open(filename, cv::FileStorage::WRITE);
	}
	if (fs.isOpened() == false) {
		return false;
	}
	fs << "depthmap" << mat;
	fs.release();
	mat.release();
	return true;
}

bool loadDepthImage(DepthImage& output, const std::string& filename) {

	if (filename == "" && false == is_regular_file(filename)) {
		return false;
	}

	cv::FileStorage fs;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVEDEPTH_FILENAME_EXTENSION)) {
		fs.open(filename + SAVEDEPTH_FILENAME_EXTENSION, cv::FileStorage::READ);
	} else {
		fs.open(filename, cv::FileStorage::READ);
	}
	if (fs.isOpened() == false) {
		std::cout << "not opened" << std::endl;
		return false;
	}
	cv::Mat mat;
	fs["depthmap"] >> mat;
	fs.release();
	if (mat.empty() == true) {
		return false;
	}
	output.setSize(Size(mat.size().width, mat.size().height));
	output.setData(reinterpret_cast<const float *>(mat.data));
	mat.release();
	return true;
}

bool saveIntensityImage(const IntensityImage& input,
		const std::string& filename) {

	if (input.getSize().width == 0 || input.getSize().height == 0
			|| input.getData().get() == 0 || filename == "") {
		return false;
	}
	cv::Mat img(cv::Size(input.getSize().width, input.getSize().height), CV_8U);

	unsigned char *data = input.getData().get();
	unsigned int local_size = input.getSize().width * input.getSize().height;
	memcpy(img.data, data, local_size * sizeof(unsigned char));

	bool val = true;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVEINTENSITY_FILENAME_EXTENSION)) {
		val = cv::imwrite(filename + SAVEINTENSITY_FILENAME_EXTENSION, img);
	} else {
		val = cv::imwrite(filename, img);
	}
	img.release();
	return val;
}

bool loadIntensityImage(IntensityImage& output, const std::string& filename) {

	if (filename == "" && false == is_regular_file(filename)) {
		std::cout << "invalid" << std::endl;
		return false;
	}
	cv::Mat img;
	std::string namelower(filename);
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (false
			== boost::algorithm::ends_with(namelower,
					IO::SAVEINTENSITY_FILENAME_EXTENSION)) {
		img = cv::imread(filename + SAVEINTENSITY_FILENAME_EXTENSION,
				CV_LOAD_IMAGE_GRAYSCALE);
	} else {
		img = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
	}

	if (img.empty() == true) {
		return false;
	}
	output.setSize(Size(img.size().width, img.size().height));
	output.setData(img.data);
	return true;
}

/*
 bool saveRGBDIImage(const RGBDIImage& input, const std::string& directory,
 const std::string& filenamebase) {

 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0) {
 if (directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 if (false == is_directory(directory_check)) {
 create_directory(directory_check);
 }
 }
 if (false == is_directory(directory_check + FOLDER_COLOR)) {
 create_directory(directory_check + FOLDER_COLOR);
 }
 if (false == is_directory(directory_check + FOLDER_DEPTH)) {
 create_directory(directory_check + FOLDER_DEPTH);
 }
 if (false == is_directory(directory_check + FOLDER_INTENSITY)) {
 create_directory(directory_check + FOLDER_INTENSITY);
 }
 saveColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 saveDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 saveIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;

 }

 bool loadRGBDIImage(RGBDIImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0
 || directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 loadColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 loadDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 loadIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;

 }

 bool saveRGBDImage(const RGBDImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0) {
 if (directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 if (false == is_directory(directory_check)) {
 create_directory(directory_check);
 }
 }
 if (false == is_directory(directory_check + FOLDER_COLOR)) {
 create_directory(directory_check + FOLDER_COLOR);
 }
 if (false == is_directory(directory_check + FOLDER_DEPTH)) {
 create_directory(directory_check + FOLDER_DEPTH);
 }

 saveColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 saveDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 return true;
 }

 bool loadRGBDImage(RGBDImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0
 || directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 loadColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 loadDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 return true;
 }

 bool saveRGBIImage(const RGBIImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0) {
 if (directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 if (false == is_directory(directory_check)) {
 create_directory(directory_check);
 }
 }
 if (false == is_directory(directory_check + FOLDER_COLOR)) {
 create_directory(directory_check + FOLDER_COLOR);
 }
 if (false == is_directory(directory_check + FOLDER_INTENSITY)) {
 create_directory(directory_check + FOLDER_INTENSITY);
 }
 saveColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 saveIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;

 }

 bool loadRGBIImage(RGBIImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0
 || directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 loadColorImage(input.color, directory_check + FOLDER_COLOR, filenamebase);
 loadIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;
 }

 bool saveDIImage(const DIImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0) {
 if (directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 if (false == is_directory(directory_check)) {
 create_directory(directory_check);
 }
 }
 if (false == is_directory(directory_check + FOLDER_DEPTH)) {
 create_directory(directory_check + FOLDER_DEPTH);
 }

 if (false == is_directory(directory_check + FOLDER_INTENSITY)) {
 create_directory(directory_check + FOLDER_INTENSITY);
 }
 saveDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 saveIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;
 }

 bool loadDIImage(DIImage& input, const std::string& directory,
 const std::string& filenamebase) {
 if (filenamebase == "") {
 return false;
 }
 std::string directory_check = directory;
 if (directory_check.size() > 0
 || directory_check[directory_check.size() - 1] != '/') {
 directory_check += "/";
 }
 loadDepthImage(input.depth, directory_check + FOLDER_DEPTH, filenamebase);
 loadIntensityImage(input.intensity, directory_check + FOLDER_INTENSITY,
 filenamebase);
 return true;
 }*/

}

}
