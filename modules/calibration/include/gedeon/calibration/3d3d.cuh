/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 3d3d.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 3d3d.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__3D3D_CUH__
#define GEDEON_CALIBRATION__3D3D_CUH__

namespace gedeon {

namespace GPU {

namespace Calibration3D3D {

bool computeSystem(float *A, float* b, float* previousvertexmap, float* previousnormalmap,
		float* inputvertexmap, float* inputnormalmap, float rotation_inverse[9],
		float translation_inverse[3], float intrinsic[9], const int& width,
		const int& height, const unsigned int& block_x, const unsigned int& block_y);
}

}

}

#endif
