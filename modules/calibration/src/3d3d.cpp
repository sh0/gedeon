/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 3d3d.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 3d3d.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/3d3d.hpp"
#include <time.h>

namespace gedeon {

namespace Calibration3D3D {

struct Match {

	std::vector<bool> consistency;
	float degree;
	cv::Vec3f reference;
	cv::Vec3f target;
	bool inlier;
	unsigned int id;

};

static bool compare(const Match &ca, const Match& cb) {
	return ca.degree > cb.degree;
}

void filterCorrespondences(std::vector<cv::Vec3f>& reference_frame,
		std::vector<cv::Vec3f>& target_frame) {

	Match *matches = new Match[reference_frame.size()];
	for (unsigned int i = 0; i < reference_frame.size(); ++i) {
		for (unsigned int j = 0; j < reference_frame.size(); ++j) {
			matches[j].consistency.push_back(false);
		}
		matches[i].degree = 0;
		matches[i].reference = reference_frame[i];
		matches[i].target = target_frame[i];
		matches[i].id = i;
		matches[i].inlier = false;
	}

	for (unsigned int i = 0; i < reference_frame.size(); ++i) {

		for (unsigned int j = i + 1; j < reference_frame.size(); ++j) {

			/*
			 * Check the distance between both correspondences
			 */
			cv::Vec3f diff_ref = reference_frame[i] - reference_frame[j];
			float length_ref = sqrtf(diff_ref.dot(diff_ref));
			cv::Vec3f diff_tar = target_frame[i] - target_frame[j];
			float length_tar = sqrtf(diff_tar.dot(diff_tar));

			if (fabs(length_ref - length_tar) < 0.005f) {
				matches[i].consistency[j] = true;
				matches[i].degree++;
				matches[j].consistency[i] = true;
				matches[j].degree++;
			}
		}
	}

	std::sort(matches, matches + reference_frame.size(), compare);

	Match &best_candidate = matches[0];
	best_candidate.inlier = true;
	unsigned int num_inliers = 1;

	bool * outliers = new bool[reference_frame.size()];
	memset(outliers, 0, reference_frame.size() * sizeof(bool));

	for (unsigned int i = 1; i < reference_frame.size(); ++i) {
		int id = matches[i].id;
		if (false == best_candidate.consistency[id]) {
			outliers[id] = true;
		}
	}

	for (unsigned int i = 1; i < reference_frame.size(); ++i) {

		Match& m = matches[i];

		if (m.degree < num_inliers) {
			break;
		}

		if (true == outliers[m.id]) {
			continue;
		}

		++num_inliers;

		for (unsigned int j = i + 1; j < reference_frame.size(); ++j) {

			int id = matches[j].id;
			if (false == outliers[id] && false == m.consistency[id]) {
				outliers[id] = true;
			}

		}
	}

	unsigned int i = 0;
	std::vector<cv::Vec3f>::iterator it1 = reference_frame.begin();
	std::vector<cv::Vec3f>::iterator it2 = target_frame.begin();
	while (it1 != reference_frame.end()) {
		if (true == outliers[i]) {
			it1 = reference_frame.erase(it1);
			it2 = target_frame.erase(it2);
		} else {
			++it1;
			++it2;
		}
		++i;
	}

	delete[] matches;
	delete[] outliers;

}

bool estimateTransformation(float T[12],
		const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame) {

	if (reference_frame.size() != target_frame.size() || 0 == T) {
		return false;
	}

	float nbelementsdiv = 1.0f / float(target_frame.size());

	// Compute the mean vectors
	cv::Vec3f meanA(0.0f, 0.0f, 0.0f);
	cv::Vec3f meanB(0.0f, 0.0f, 0.0f);
	for (unsigned int i = 0; i < target_frame.size(); ++i) {
		meanA += reference_frame[i];
		meanB += target_frame[i];
	}

	meanA *= nbelementsdiv;
	meanB *= nbelementsdiv;

	cv::Mat A(3, 1, CV_32F);
	cv::Mat B(1, 3, CV_32F);
	cv::Mat H(3, 3, CV_32F, 0.0f);
	for (unsigned int i = 0; i < target_frame.size(); ++i) {
		A.at<float>(0, 0) = reference_frame[i][0] - meanA[0];
		A.at<float>(1, 0) = reference_frame[i][1] - meanA[1];
		A.at<float>(2, 0) = reference_frame[i][2] - meanA[2];

		B.at<float>(0, 0) = target_frame[i][0] - meanB[0];
		B.at<float>(0, 1) = target_frame[i][1] - meanB[1];
		B.at<float>(0, 2) = target_frame[i][2] - meanB[2];
		H += A * B;
	}

	cv::Mat D, U, Vt;
	cv::SVD::compute(H, D, U, Vt); // D is assumed as sorted*/
	cv::Mat X = Vt.t() * U.t();

	float determinant = cv::determinant(X);

	if (determinant > 0) {

		for (unsigned int i = 0; i < 3; ++i) {
			for (unsigned int j = 0; j < 3; ++j) {
				T[i * 4 + j] = X.at<float>(i, j);
			}
			T[i * 4 + 3] = meanB[i]
					- (X.at<float>(i, 0) * meanA[0]
							+ X.at<float>(i, 1) * meanA[1]
							+ X.at<float>(i, 2) * meanA[2]);
		}

		return true;
	}
	return false;
}

bool estimateTransformationWithRansac(float T[12],
		const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame,
		const unsigned int& iterations) {

	if (reference_frame.size() != target_frame.size() || 0 == T) {
		return false;
	}

	srand(time(NULL));
	unsigned int pairs[3];

	std::vector<cv::Vec3f> reference;
	std::vector<cv::Vec3f> target;

	float tmp[12];
	unsigned int inliers = 0;
	for (unsigned int i = 0; i < iterations; ++i) {

		// select three different correspondences
		pairs[0] = rand() % reference_frame.size();
		do {
			pairs[1] = rand() % reference_frame.size();
		} while (pairs[1] == pairs[0]);
		do {
			pairs[2] = rand() % reference_frame.size();
			pairs[3] = rand() % reference_frame.size();
			pairs[4] = rand() % reference_frame.size();
		} while (pairs[2] == pairs[0] || pairs[2] == pairs[1]);

		for(unsigned int k = 0; k < 3; ++k){
			reference.push_back(reference_frame[pairs[k]]);
			target.push_back(target_frame[pairs[k]]);
		}

		if(true == estimateTransformation(tmp, reference, target)){

			/*
			 * Find the number of inliers
			 */
			unsigned int tmp_inliers = 0;
			cv::Vec3f res;
			for(unsigned int k = 0; k < target_frame.size(); ++k){
				res[0] = tmp[0]*reference_frame[k][0] * tmp[1]*reference_frame[k][1] * tmp[2]*reference_frame[k][2] + tmp[3];
				res[1] = tmp[4]*reference_frame[k][0] * tmp[5]*reference_frame[k][1] * tmp[6]*reference_frame[k][2] + tmp[7];
				res[2] = tmp[8]*reference_frame[k][0] * tmp[9]*reference_frame[k][1] * tmp[10]*reference_frame[k][2] + tmp[11];
				cv::Vec3f diffv = target_frame[k] - res;
				float diff = sqrtf(diffv.dot(diffv));
				if(diff > 0.001f){
					++tmp_inliers;
				}
			}

			if(tmp_inliers > inliers){
				memcpy(T,tmp,12*sizeof(float));
				inliers = tmp_inliers;
			}
		}

		reference.clear();
		target.clear();

	}

	if(inliers > 0){
		return true;
	}

	return false;
}



bool estimateTransformationWithRansacInliers(float T[12],
		const std::vector<cv::Vec3f>& reference_frame,
		const std::vector<cv::Vec3f>& target_frame,
		const float& inlierspercentage) {

	if (reference_frame.size() != target_frame.size() || 0 == T || inlierspercentage <= 0.0f || inlierspercentage > 1.0f ) {
		return false;
	}

	srand(time(NULL));
	unsigned int pairs[3];

	std::vector<cv::Vec3f> reference;
	std::vector<cv::Vec3f> target;

	float tmp[12];
	unsigned int inliers = static_cast<unsigned int>(inlierspercentage*target_frame.size());
	unsigned int tmp_inliers = 0;

	do{

		// select three different correspondences
		pairs[0] = rand() % reference_frame.size();
		do {
			pairs[1] = rand() % reference_frame.size();
		} while (pairs[1] == pairs[0]);
		do {
			pairs[2] = rand() % reference_frame.size();
			pairs[3] = rand() % reference_frame.size();
			pairs[4] = rand() % reference_frame.size();
		} while (pairs[2] == pairs[0] || pairs[2] == pairs[1]);

		for(unsigned int k = 0; k < 3; ++k){
			reference.push_back(reference_frame[pairs[k]]);
			target.push_back(target_frame[pairs[k]]);
		}

		if(true == estimateTransformation(tmp, reference, target)){

			/*
			 * Find the number of inliers
			 */
			tmp_inliers = 0;
			cv::Vec3f res;
			for(unsigned int k = 0; k < target_frame.size(); ++k){
				res[0] = tmp[0]*reference_frame[k][0] * tmp[1]*reference_frame[k][1] * tmp[2]*reference_frame[k][2] + tmp[3];
				res[1] = tmp[4]*reference_frame[k][0] * tmp[5]*reference_frame[k][1] * tmp[6]*reference_frame[k][2] + tmp[7];
				res[2] = tmp[8]*reference_frame[k][0] * tmp[9]*reference_frame[k][1] * tmp[10]*reference_frame[k][2] + tmp[11];
				cv::Vec3f diffv = target_frame[k] - res;
				float diff = sqrtf(diffv.dot(diffv));
				if(diff > 0.001f){
					++tmp_inliers;
				}
			}


		}

		reference.clear();
		target.clear();

	}while(tmp_inliers < inliers);

	if(tmp_inliers >= inliers){
			memcpy(T,tmp,12*sizeof(float));
			inliers = tmp_inliers;
	}
	if(inliers > 0){
		return true;
	}

	return false;
}



}

}
