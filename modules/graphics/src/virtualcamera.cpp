/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * virtualcamera.cpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * virtualcamera.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/graphics/virtualcamera.hpp"
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace gedeon {

VirtualCamera::VirtualCamera(void) :
		position(glm::vec3(0.0f, 0.0f, 0.0f)), direction(
				glm::vec3(0.0f, 0.0f, -1.0f)), orientation(
				glm::vec3(0.0f, 1.0f, 0.0f)), front(
				glm::vec3(0.0f, 0.0f, 1.0f)), up(glm::vec3(0.0f, 1.0f, 0.0f)), right(
				glm::vec3(1.0f, 0.0f, 0.0f)), znear(0.01f), zfar(100.0f), fovy(
				60.0f) {
	this->right = glm::cross(this->direction, this->orientation);
	float norm = 1.0f / float(glm::length(this->right));
	this->right *= norm;
}

VirtualCamera::~VirtualCamera(void) {

}

void VirtualCamera::setDirection(const glm::vec3& direction) {
	float norm = 1.0f / float(glm::length(direction));
	this->direction = direction * norm;

	this->right = glm::cross(this->direction, this->orientation);
	norm = 1.0f / float(glm::length(this->right));
	this->right *= norm;
}

void VirtualCamera::setOrientation(const glm::vec3& orientation) {
	float norm = 1.0f / float(glm::length(orientation));
	this->orientation = orientation * norm;

	this->right = glm::cross(this->direction, this->orientation);
	norm = 1.0f / float(glm::length(this->right));
	this->right *= norm;
}

void VirtualCamera::translate(const glm::vec3& direction) {
	this->position += direction;
}

void VirtualCamera::rotate(const float& roll, const float& pitch,
		const float& yaw) {
	glm::quat q = glm::angleAxis(roll, this->front);
	this->right = glm::rotate(q, this->right);
	this->up = glm::rotate(q, this->up);

	q = glm::angleAxis(pitch, this->right);
	this->up = glm::rotate(q, this->up);
	this->front = glm::rotate(q, this->front);

	q = glm::angleAxis(yaw, this->up);
	this->right = glm::rotate(q, this->right);
	this->front = glm::rotate(q, this->front);
}

glm::mat4 VirtualCamera::getPerspective(const float& aspect) const {
	return glm::perspective(this->fovy, aspect, this->znear, this->zfar);
}

glm::mat4 VirtualCamera::getOrthographic(const unsigned int& width,
		const unsigned int& height) const {
	return glm::ortho(0.0f, float(width), float(height), 0.0f, -1.0f, 1.0f);
}

glm::mat4 VirtualCamera::getLookAt(void) const {
	return glm::lookAt(position,
			position + direction, orientation);
}

glm::vec4 VirtualCamera::getVolume(const float& ratio) const
{
    float heightdiv2near = this->znear * tan(this->fovy*0.5f * 0.0174532925199432f);
    float widthdiv2 = ratio * heightdiv2near;
    return glm::vec4(-widthdiv2,widthdiv2,-heightdiv2near,heightdiv2near);
}

}
