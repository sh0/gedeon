/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * flycapturegrabber.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * flycapturegrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/flycapturedriver.hpp"
#include "gedeon/grabber/flycapturegrabber.hpp"
#include "gedeon/datatypes/io.hpp"

#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace gedeon {

enum PixelFormatLocal {
	YUV444 = 1, YUV422, YUV411, RGB, Y8, Y16, UNDEF
};

class FlyCaptureGrabberParams {
public:
	FlyCaptureGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE), pixfor(
					YUV422) {
	}
	~FlyCaptureGrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;
	PixelFormatLocal pixfor;

};

class FlyCaptureGrabberCapture {
public:
	FlyCaptureGrabberCapture(void) :
			devicename(""), device_id(0) {
	}
	~FlyCaptureGrabberCapture(void) {
	}

	FlyCapture2::PGRGuid guid;
	FlyCapture2::Camera cam;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

};

class FlyCaptureGrabberThread {
public:
	FlyCaptureGrabberThread(void) :
			pthread(0) {
	}
	~FlyCaptureGrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

bool convertVideoMode(unsigned int& width, unsigned int& height,
		PixelFormatLocal& px, FlyCapture2::VideoMode& vm);
bool convertFrameRate(float& output, FlyCapture2::FrameRate& fr);
std::string getPropertyName(const FlyCapture2::PropertyType& ptype);

FlyCaptureGrabber::FlyCaptureGrabber(void) :
		Grabber(), capturepimpl(new FlyCaptureGrabberCapture()), paramspimpl(
				new FlyCaptureGrabberParams()), threadpimpl(
				new FlyCaptureGrabberThread()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool FlyCaptureGrabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("FlyCaptureGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->capturepimpl->device_id = id;
	bool created = false;

	FlyCaptureDriver *driver = 0;

	try {
		driver = dynamic_cast<FlyCaptureDriver *>(d);
	} catch (const std::bad_cast& e) {
#ifdef WIN32
		UNUSED_VARIABLE(e);
#endif
		Log::add().error("FlyCaptureGrabber::init", "Incompatible driver");
		return false;
	}

	if (0 != driver->getCount() && id < driver->getCount()) {

		this->capturepimpl->devicename = driver->populate()[id];

		/*
		 * Access the camera
		 */
		FlyCapture2::BusManager m_BusManager;
		FlyCapture2::Error error = m_BusManager.GetCameraFromIndex(id,
				&(this->capturepimpl->guid));
		if (error != FlyCapture2::PGRERROR_OK) {
			Log::add().error("FlyCaptureGrabber::init",
					"Unable to access the camera " + IO::numberToString(id));
			error.PrintErrorTrace();
			return false;
		}

		/*
		 * Connect to the camera
		 */
		error = this->capturepimpl->cam.Connect(&(this->capturepimpl->guid));
		if (error != FlyCapture2::PGRERROR_OK) {
			Log::add().error("FlyCaptureGrabber::init",
					"Unable to connect with the camera "
							+ IO::numberToString(id));
			error.PrintErrorTrace();
			return false;
		}

		FlyCapture2::VideoMode pVideoMode;
		FlyCapture2::FrameRate pFrameRate;
		float fr = 60.0f;
		unsigned int w = 320, h = 240;
		convertFrameRate(fr, pFrameRate);
		convertVideoMode(w, h, this->paramspimpl->pixfor, pVideoMode);
		this->capturepimpl->cam.SetVideoModeAndFrameRate(pVideoMode,
				pFrameRate);

		/*triggerAutoManualProperty(FlyCapture2::AUTO_EXPOSURE);
		 triggerAutoManualProperty(FlyCapture2::BRIGHTNESS);
		 triggerAutoManualProperty(FlyCapture2::SHARPNESS);
		 triggerAutoManualProperty(FlyCapture2::WHITE_BALANCE);
		 triggerAutoManualProperty(FlyCapture2::GAIN);
		 triggerAutoManualProperty(FlyCapture2::SHUTTER);*/

		this->capturepimpl->rgbdi.reset(new RGBDIImage);

		if (true == this->capturepimpl->cam.IsConnected()) {
			this->capturepimpl->cam.StartCapture();
			FlyCapture2::Image img;
			unsigned int cnt = 0;
			do {
				millisecSleep(10);
				++cnt;
				error = this->capturepimpl->cam.RetrieveBuffer(&img);
			} while (error != FlyCapture2::PGRERROR_OK && cnt < 30);

			if (30 <= cnt) {
				Log::add().error("FlyCaptureGrabber::init",
						"Unable to change the settings of the camera "
								+ IO::numberToString(id));
				error.PrintErrorTrace();
				return false;
			}

			unsigned int width = 0;
			unsigned int height = 0;
			FlyCapture2::PixelFormat pf;
			unsigned int stride;
			img.GetDimensions(&(height), &(width), &stride, &pf);
			this->paramspimpl->hascolor = true;
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
			created = true;

			this->capturepimpl->cam.GetVideoModeAndFrameRate(&pVideoMode,
					&pFrameRate);

			if (false
					== convertFrameRate(this->paramspimpl->framerate,
							pFrameRate)) {
				this->paramspimpl->framerate = 15.0f;
			}
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
			millisecSleep(20);
			this->capturepimpl->cam.StopCapture();
		}
		if (false == created) {
			Log::add().error("FlyCaptureGrabber::init",
					"Failed finding the point grey device "
							+ IO::numberToString(id));
			return false;
		}

		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		this->paramspimpl->initialized = true;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("FlyCaptureGrabber::init", "No device found");
	return false;
}

bool FlyCaptureGrabber::init(const std::string& source, Driver* d) {
#ifdef WIN32
	UNUSED_VARIABLE(d);
	UNUSED_VARIABLE(source);
#endif
	Log::add().warning("FlyCaptureGrabber::init",
			"No internal format available");
	return false;
}

FlyCaptureGrabber::~FlyCaptureGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
	delete threadpimpl;
	delete paramspimpl;
	delete capturepimpl;
}

std::string FlyCaptureGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int FlyCaptureGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& FlyCaptureGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool FlyCaptureGrabber::getParameter(const GrabberParameter& param,
		float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (FLYCAPTURE_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}
	if (FLYCAPTURE_BRIGHTNESS == param) {
		return getProperty(value, FlyCapture2::BRIGHTNESS);
	}
	if (FLYCAPTURE_AUTO_EXPOSURE == param) {
		return getProperty(value, FlyCapture2::AUTO_EXPOSURE);
	}
	if (FLYCAPTURE_SHARPNESS == param) {
		return getProperty(value, FlyCapture2::SHARPNESS);
	}
	if (FLYCAPTURE_WHITE_BALANCE == param) {
		return getProperty(value, FlyCapture2::WHITE_BALANCE);
	}
	if (FLYCAPTURE_HUE == param) {
		return getProperty(value, FlyCapture2::HUE);
	}
	if (FLYCAPTURE_SATURATION == param) {
		return getProperty(value, FlyCapture2::SATURATION);
	}
	if (FLYCAPTURE_GAMMA == param) {
		return getProperty(value, FlyCapture2::GAMMA);
	}
	if (FLYCAPTURE_IRIS == param) {
		return getProperty(value, FlyCapture2::IRIS);
	}
	if (FLYCAPTURE_FOCUS == param) {
		return getProperty(value, FlyCapture2::FOCUS);
	}
	if (FLYCAPTURE_ZOOM == param) {
		return getProperty(value, FlyCapture2::ZOOM);
	}
	if (FLYCAPTURE_PAN == param) {
		return getProperty(value, FlyCapture2::PAN);
	}
	if (FLYCAPTURE_TILT == param) {
		return getProperty(value, FlyCapture2::TILT);
	}
	if (FLYCAPTURE_SHUTTER == param) {
		return getProperty(value, FlyCapture2::SHUTTER);
	}
	if (FLYCAPTURE_GAIN == param) {
		return getProperty(value, FlyCapture2::GAIN);
	}

	return false;
}

bool FlyCaptureGrabber::getParameter(const GrabberParameter& param,
		float* values, unsigned int& size) {
#ifdef WIN32
	UNUSED_VARIABLE(size);
	UNUSED_VARIABLE(values);
	UNUSED_VARIABLE(param);
#endif
	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool FlyCaptureGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (FLYCAPTURE_BRIGHTNESS == param) {
		return setProperty(value, FlyCapture2::BRIGHTNESS);
	}
	if (FLYCAPTURE_AUTO_EXPOSURE == param) {
		return setProperty(value, FlyCapture2::AUTO_EXPOSURE);
	}
	if (FLYCAPTURE_SHARPNESS == param) {
		return setProperty(value, FlyCapture2::SHARPNESS);
	}
	if (FLYCAPTURE_WHITE_BALANCE == param) {
		return setProperty(value, FlyCapture2::WHITE_BALANCE);
	}
	if (FLYCAPTURE_HUE == param) {
		return setProperty(value, FlyCapture2::HUE);
	}
	if (FLYCAPTURE_SATURATION == param) {
		return setProperty(value, FlyCapture2::SATURATION);
	}
	if (FLYCAPTURE_GAMMA == param) {
		return setProperty(value, FlyCapture2::GAMMA);
	}
	if (FLYCAPTURE_IRIS == param) {
		return setProperty(value, FlyCapture2::IRIS);
	}
	if (FLYCAPTURE_FOCUS == param) {
		return setProperty(value, FlyCapture2::FOCUS);
	}
	if (FLYCAPTURE_ZOOM == param) {
		return setProperty(value, FlyCapture2::ZOOM);
	}
	if (FLYCAPTURE_PAN == param) {
		return setProperty(value, FlyCapture2::PAN);
	}
	if (FLYCAPTURE_TILT == param) {
		return setProperty(value, FlyCapture2::TILT);
	}
	if (FLYCAPTURE_SHUTTER == param) {
		return setProperty(value, FlyCapture2::SHUTTER);
	}
	if (FLYCAPTURE_GAIN == param) {
		return setProperty(value, FlyCapture2::GAIN);
	}

	return false;
}

bool FlyCaptureGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (FLYCAPTURE_AUTO_EXPOSURE_ONOFF == trig) {
		return triggerProperty(FlyCapture2::AUTO_EXPOSURE);
	}
	if (FLYCAPTURE_BRIGHTNESS_ONOFF == trig) {
		return triggerProperty(FlyCapture2::BRIGHTNESS);
	}
	if (FLYCAPTURE_SHARPNESS_ONOFF == trig) {
		return triggerProperty(FlyCapture2::SHARPNESS);
	}
	if (FLYCAPTURE_WHITE_BALANCE_ONOFF == trig) {
		return triggerProperty(FlyCapture2::WHITE_BALANCE);
	}
	if (FLYCAPTURE_HUE_ONOFF == trig) {
		return triggerProperty(FlyCapture2::HUE);
	}
	if (FLYCAPTURE_SATURATION_ONOFF == trig) {
		return triggerProperty(FlyCapture2::SATURATION);
	}
	if (FLYCAPTURE_GAMMA_ONOFF == trig) {
		return triggerProperty(FlyCapture2::GAMMA);
	}
	if (FLYCAPTURE_IRIS_ONOFF == trig) {
		return triggerProperty(FlyCapture2::IRIS);
	}
	if (FLYCAPTURE_FOCUS_ONOFF == trig) {
		return triggerProperty(FlyCapture2::FOCUS);
	}
	if (FLYCAPTURE_ZOOM_ONOFF == trig) {
		return triggerProperty(FlyCapture2::ZOOM);
	}
	if (FLYCAPTURE_PAN_ONOFF == trig) {
		return triggerProperty(FlyCapture2::PAN);
	}
	if (FLYCAPTURE_TILT_ONOFF == trig) {
		return triggerProperty(FlyCapture2::TILT);
	}
	if (FLYCAPTURE_SHUTTER_ONOFF == trig) {
		return triggerProperty(FlyCapture2::SHUTTER);
	}
	if (FLYCAPTURE_GAIN_ONOFF == trig) {
		return triggerProperty(FlyCapture2::GAIN);
	}
	if (FLYCAPTURE_AUTO_EXPOSURE_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::AUTO_EXPOSURE);
	}
	if (FLYCAPTURE_BRIGHTNESS_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::BRIGHTNESS);
	}
	if (FLYCAPTURE_SHARPNESS_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::SHARPNESS);
	}
	if (FLYCAPTURE_WHITE_BALANCE_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::WHITE_BALANCE);
	}
	if (FLYCAPTURE_HUE_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::HUE);
	}
	if (FLYCAPTURE_SATURATION_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::SATURATION);
	}
	if (FLYCAPTURE_GAMMA_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::GAMMA);
	}
	if (FLYCAPTURE_IRIS_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::IRIS);
	}
	if (FLYCAPTURE_FOCUS_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::FOCUS);
	}
	if (FLYCAPTURE_ZOOM_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::ZOOM);
	}
	if (FLYCAPTURE_PAN_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::PAN);
	}
	if (FLYCAPTURE_TILT_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::TILT);
	}
	if (FLYCAPTURE_SHUTTER_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::SHUTTER);
	}
	if (FLYCAPTURE_GAIN_AUTOMANUAL == trig) {
		return triggerAutoManualProperty(FlyCapture2::GAIN);
	}
	if (FLYCAPTURE_1_875FPS == trig) {
		return setFramerate(1.875f);
	}
	if (FLYCAPTURE_3_75FPS == trig) {
		return setFramerate(3.75f);
	}
	if (FLYCAPTURE_7_5FPS == trig) {
		return setFramerate(7.5f);
	}
	if (FLYCAPTURE_15FPS == trig) {
		return setFramerate(15.0f);
	}
	if (FLYCAPTURE_30FPS == trig) {
		return setFramerate(30.0f);
	}
	if (FLYCAPTURE_60FPS == trig) {
		return setFramerate(60.0f);
	}
	if (FLYCAPTURE_120FPS == trig) {
		return setFramerate(120.0f);
	}
	if (FLYCAPTURE_240FPS == trig) {
		return setFramerate(240.0f);
	}
	if (FLYCAPTURE_RGB_VGA == trig) {
		return setResolution(640, 480, RGB);
	}
	if (FLYCAPTURE_RGB_SVGA == trig) {
		return setResolution(800, 600, RGB);
	}
	if (FLYCAPTURE_RGB_XGA == trig) {
		return setResolution(1024, 768, RGB);
	}
	if (FLYCAPTURE_RGB_SXGA_ == trig) {
		return setResolution(1280, 960, RGB);
	}
	if (FLYCAPTURE_RGB_UXGA == trig) {
		return setResolution(1600, 1200, RGB);
	}
	if (FLYCAPTURE_YUV444_QQVGA == trig) {
		return setResolution(160, 120, YUV444);
	}
	if (FLYCAPTURE_YUV422_QVGA == trig) {
		return setResolution(320, 240, YUV422);
	}
	if (FLYCAPTURE_YUV422_VGA == trig) {
		return setResolution(640, 480, YUV422);
	}
	if (FLYCAPTURE_YUV422_SVGA == trig) {
		return setResolution(800, 600, YUV422);
	}
	if (FLYCAPTURE_YUV422_XGA == trig) {
		return setResolution(1024, 768, YUV422);
	}
	if (FLYCAPTURE_YUV422_SXGA_ == trig) {
		return setResolution(1280, 960, YUV422);
	}
	if (FLYCAPTURE_YUV422_UXGA == trig) {
		return setResolution(1600, 1200, YUV422);
	}
	return false;
}

bool FlyCaptureGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool FlyCaptureGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool FlyCaptureGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool FlyCaptureGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void FlyCaptureGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void FlyCaptureGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&FlyCaptureGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void FlyCaptureGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		emitEvent(this->threadpimpl->stopped);
	}
}

void FlyCaptureGrabber::grabOneFrame(void){
	Log::add().warning("FlyCaptureGrabber::grabOneFrame", "Not available");
}

void FlyCaptureGrabber::callbackF(FlyCapture2::Image *img) {
	if (false == this->paramspimpl->paused) {
		boost::mutex::scoped_lock l(this->threadpimpl->mutex);
		if (true == this->paramspimpl->hascolor) {
			unsigned char *color =
					this->capturepimpl->rgbdi.get()->color.getData().get();
			if (RGB == this->paramspimpl->pixfor) {
				memcpy(color, img->GetData(),
						this->capturepimpl->rgbdi.get()->color.getSize().width
								* this->capturepimpl->rgbdi.get()->color.getSize().height
								* 3 * sizeof(unsigned char));
			} else {
				if (UNDEF != this->paramspimpl->pixfor) {
					FlyCapture2::Image img_tmp;
					img->Convert(FlyCapture2::PIXEL_FORMAT_RGB, &img_tmp);
					memcpy(color, img_tmp.GetData(),
							this->capturepimpl->rgbdi.get()->color.getSize().width
									* this->capturepimpl->rgbdi.get()->color.getSize().height
									* 3 * sizeof(unsigned char));
				}
			}
		}

		/*
		 * Emit a message to confirm the update
		 */
		emitEvent(this->threadpimpl->updated);
	}
}

static void callback(FlyCapture2::Image *img, const void *g) {
	void * g2 = const_cast<void *>(g);
	FlyCaptureGrabber * g3 = reinterpret_cast<FlyCaptureGrabber *>(g2);
	g3->callbackF(img);
}

void FlyCaptureGrabber::run(void) {

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	if (false == this->capturepimpl->cam.IsConnected()) {
		Log::add().error("FlyCaptureGrabber::run", "Unable to open the device");
		return;
	}

	this->capturepimpl->cam.StartCapture(&callback, this);

	FlyCapture2::Image img;

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {

		current_time = timer.get();

		current_time = static_cast<long>(1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);
		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;

		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	this->capturepimpl->cam.StopCapture();

	timer.stop();
	this->paramspimpl->finished = true;

}

bool FlyCaptureGrabber::setProperty(const float& value,
		const FlyCapture2::PropertyType& ptype) {

	FlyCapture2::PropertyInfo propinfo;
	propinfo.type = ptype;
	FlyCapture2::Error err = this->capturepimpl->cam.GetPropertyInfo(&propinfo);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::setProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}
	if (false == propinfo.present) {
		Log::add().error("FlyCaptureGrabber::setProperty",
				"The " + getPropertyName(ptype) + " property is not available");
		return false;
	}

	int min_ = propinfo.min;
	int max_ = propinfo.max;
	int max2_ = max_ - min_;

	float tmpvalue = value;
	if (tmpvalue < 0.0f) {
		tmpvalue = 0.0f;
	}
	if (tmpvalue > 1.0f) {
		tmpvalue = 1.0f;
	}

	int new_value = static_cast<int>(max2_ * tmpvalue);

	FlyCapture2::Property prop;
	prop.type = ptype;
	err = this->capturepimpl->cam.GetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::setProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}

	prop.valueA = min_ + new_value;
	prop.onOff = true;
	prop.autoManualMode = false;
	err = this->capturepimpl->cam.SetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::setProperty",
				"Unable to set the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}

	return true;
}

bool FlyCaptureGrabber::getProperty(float& value,
		const FlyCapture2::PropertyType& ptype) {

	value = 0.0f;
	FlyCapture2::PropertyInfo propinfo;
	propinfo.type = ptype;
	FlyCapture2::Error err = this->capturepimpl->cam.GetPropertyInfo(&propinfo);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::getProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}
	if (false == propinfo.present) {
		Log::add().error("FlyCaptureGrabber::getProperty",
				"The " + getPropertyName(ptype) + " property is not available");
		return false;
	}

	int min_ = propinfo.min;
	int max_ = propinfo.max;
	int max2_ = max_ - min_;

	FlyCapture2::Property prop;
	prop.type = ptype;
	err = this->capturepimpl->cam.GetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::getProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}

	value = static_cast<float>(prop.valueA - min_) / static_cast<float>(max2_);
	return true;
}

bool FlyCaptureGrabber::triggerProperty(
		const FlyCapture2::PropertyType& ptype) {

	FlyCapture2::Property prop;
	prop.type = ptype;
	FlyCapture2::Error err = this->capturepimpl->cam.GetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::triggerProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}

	if (false == prop.present) {
		Log::add().error("FlyCaptureGrabber::triggerProperty",
				"The " + getPropertyName(ptype) + " property is not available");
		return false;
	}

	prop.onOff = !prop.onOff;

	err = this->capturepimpl->cam.SetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::triggerProperty",
				"Unable to set the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}
	return true;
}

bool FlyCaptureGrabber::triggerAutoManualProperty(
		const FlyCapture2::PropertyType& ptype) {

	FlyCapture2::Property prop;
	prop.type = ptype;
	FlyCapture2::Error err = this->capturepimpl->cam.GetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::triggerAutoManualProperty",
				"Unable to get the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}

	if (false == prop.present) {
		Log::add().error("FlyCaptureGrabber::triggerAutoManualProperty",
				"The " + getPropertyName(ptype) + " property is not available");
		return false;
	}

	prop.autoManualMode = !prop.autoManualMode;

	err = this->capturepimpl->cam.SetProperty(&prop);
	if (err != FlyCapture2::PGRERROR_OK) {
		Log::add().error("FlyCaptureGrabber::triggerAutoManualProperty",
				"Unable to set the " + getPropertyName(ptype) + " property");
		err.PrintErrorTrace();
		return false;
	}
	return true;
}

std::string getPropertyName(const FlyCapture2::PropertyType& ptype) {

	if (FlyCapture2::BRIGHTNESS == ptype) {
		return std::string("brightness");
	}
	if (FlyCapture2::AUTO_EXPOSURE == ptype) {
		return std::string("auto exposure");
	}
	if (FlyCapture2::SHARPNESS == ptype) {
		return std::string("sharpness");
	}
	if (FlyCapture2::WHITE_BALANCE == ptype) {
		return std::string("white balance");
	}
	if (FlyCapture2::HUE == ptype) {
		return std::string("hue");
	}
	if (FlyCapture2::SATURATION == ptype) {
		return std::string("saturation");
	}
	if (FlyCapture2::GAMMA == ptype) {
		return std::string("gamma");
	}
	if (FlyCapture2::IRIS == ptype) {
		return std::string("iris");
	}
	if (FlyCapture2::FOCUS == ptype) {
		return std::string("focus");
	}
	if (FlyCapture2::ZOOM == ptype) {
		return std::string("zoom");
	}
	if (FlyCapture2::PAN == ptype) {
		return std::string("pan");
	}
	if (FlyCapture2::TILT == ptype) {
		return std::string("tilt");
	}
	if (FlyCapture2::SHUTTER == ptype) {
		return std::string("shutter");
	}
	if (FlyCapture2::GAIN == ptype) {
		return std::string("gain");
	}
	return std::string("unknown");
}

bool convertFrameRate(float& output, FlyCapture2::FrameRate& fr) {

	if (output == 0.0f) {
		if (FlyCapture2::FRAMERATE_1_875 == fr) {
			output = 1.875f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_3_75 == fr) {
			output = 3.75f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_7_5 == fr) {
			output = 7.5f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_15 == fr) {
			output = 15.0f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_30 == fr) {
			output = 30.0f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_60 == fr) {
			output = 60.0f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_120 == fr) {
			output = 120.0f;
			return true;
		}
		if (FlyCapture2::FRAMERATE_240 == fr) {
			output = 240.0f;
			return true;
		}
	} else {
		if (output == 1.875f) {
			fr = FlyCapture2::FRAMERATE_1_875;
			return true;
		}
		if (output == 3.75f) {
			fr = FlyCapture2::FRAMERATE_3_75;
			return true;
		}
		if (output == 7.5f) {
			fr = FlyCapture2::FRAMERATE_7_5;
			return true;
		}
		if (output == 15.0f) {
			fr = FlyCapture2::FRAMERATE_15;
			return true;
		}
		if (output == 30.0f) {
			fr = FlyCapture2::FRAMERATE_30;
			return true;
		}
		if (output == 60.0f) {
			fr = FlyCapture2::FRAMERATE_60;
			return true;
		}
		if (output == 120.0f) {
			fr = FlyCapture2::FRAMERATE_120;
			return true;
		}
		if (output == 240.0f) {
			fr = FlyCapture2::FRAMERATE_240;
			return true;
		}
	}
	return false;

}

bool convertVideoMode(unsigned int& width, unsigned int& height,
		PixelFormatLocal& px, FlyCapture2::VideoMode& vm) {

	if (width == 0 || height == 0 || px == UNDEF) {
		if (FlyCapture2::VIDEOMODE_160x120YUV444 == vm) {
			width = 160;
			height = 120;
			px = YUV444;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_320x240YUV422 == vm) {
			width = 320;
			height = 240;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_640x480YUV411 == vm) {
			width = 640;
			height = 480;
			px = YUV411;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_640x480YUV422 == vm) {
			width = 640;
			height = 480;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_640x480RGB == vm) {
			width = 640;
			height = 480;
			px = RGB;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_640x480Y8 == vm) {
			width = 640;
			height = 480;
			px = Y8;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_640x480Y16 == vm) {
			width = 640;
			height = 480;
			px = Y16;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_800x600YUV422 == vm) {
			width = 800;
			height = 600;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_800x600RGB == vm) {
			width = 800;
			height = 600;
			px = RGB;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_800x600Y8 == vm) {
			width = 800;
			height = 600;
			px = Y8;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_800x600Y16 == vm) {
			width = 800;
			height = 600;
			px = Y16;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1024x768YUV422 == vm) {
			width = 1024;
			height = 768;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1024x768RGB == vm) {
			width = 1024;
			height = 768;
			px = RGB;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1024x768Y8 == vm) {
			width = 1024;
			height = 768;
			px = Y8;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1024x768Y16 == vm) {
			width = 1024;
			height = 768;
			px = Y16;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1280x960YUV422 == vm) {
			width = 1280;
			height = 960;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1280x960RGB == vm) {
			width = 1280;
			height = 960;
			px = RGB;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1280x960Y8 == vm) {
			width = 1280;
			height = 960;
			px = Y8;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1280x960Y16 == vm) {
			width = 1280;
			height = 960;
			px = Y16;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1600x1200YUV422 == vm) {
			width = 1600;
			height = 1200;
			px = YUV422;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1600x1200RGB == vm) {
			width = 1600;
			height = 1200;
			px = RGB;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1600x1200Y8 == vm) {
			width = 1600;
			height = 1200;
			px = Y8;
			return true;
		}

		if (FlyCapture2::VIDEOMODE_1600x1200Y16 == vm) {
			width = 1600;
			height = 1200;
			px = Y16;
			return true;
		}
	} else {
		if (width == 160 && height == 120 && px == YUV444) {
			vm = FlyCapture2::VIDEOMODE_160x120YUV444;
			return true;
		}

		if (width == 320 && height == 240 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_320x240YUV422;
			return true;
		}

		if (width == 640 && height == 480 && px == YUV411) {
			vm = FlyCapture2::VIDEOMODE_640x480YUV411;
			return true;
		}

		if (width == 640 && height == 480 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_640x480YUV422;
			return true;
		}

		if (width == 640 && height == 480 && px == RGB) {
			vm = FlyCapture2::VIDEOMODE_640x480RGB;
			return true;
		}

		if (width == 640 && height == 480 && px == Y8) {
			vm = FlyCapture2::VIDEOMODE_640x480Y8;
			return true;
		}

		if (width == 640 && height == 480 && px == Y16) {
			vm = FlyCapture2::VIDEOMODE_640x480Y16;
			return true;
		}

		if (width == 800 && height == 600 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_800x600YUV422;
			return true;
		}

		if (width == 800 && height == 600 && px == RGB) {
			vm = FlyCapture2::VIDEOMODE_800x600RGB;
			return true;
		}

		if (width == 800 && height == 600 && px == Y8) {
			vm = FlyCapture2::VIDEOMODE_800x600Y8;
			return true;
		}

		if (width == 800 && height == 600 && px == Y16) {
			vm = FlyCapture2::VIDEOMODE_800x600Y16;
			return true;
		}

		if (width == 1024 && height == 768 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1024x768YUV422;
			return true;
		}

		if (width == 1024 && height == 768 && px == RGB) {
			vm = FlyCapture2::VIDEOMODE_1024x768RGB;
			return true;
		}

		if (width == 1024 && height == 768 && px == Y8) {
			vm = FlyCapture2::VIDEOMODE_1024x768Y8;
			return true;
		}

		if (width == 1024 && height == 768 && px == Y16) {
			vm = FlyCapture2::VIDEOMODE_1024x768Y16;
			return true;
		}

		if (width == 1280 && height == 960 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1280x960YUV422;
			return true;
		}

		if (width == 1280 && height == 960 && px == RGB) {
			vm = FlyCapture2::VIDEOMODE_1280x960RGB;
			return true;
		}

		if (width == 1280 && height == 960 && px == Y8) {
			vm = FlyCapture2::VIDEOMODE_1280x960Y8;
			return true;
		}

		if (width == 1280 && height == 960 && px == Y16) {
			vm = FlyCapture2::VIDEOMODE_1280x960Y16;
			return true;
		}

		if (width == 1600 && height == 1200 && px == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1600x1200YUV422;
			return true;
		}

		if (width == 1600 && height == 1200 && px == RGB) {
			vm = FlyCapture2::VIDEOMODE_1600x1200RGB;
			return true;
		}

		if (width == 1600 && height == 1200 && px == Y8) {
			vm = FlyCapture2::VIDEOMODE_1600x1200Y8;
			return true;
		}

		if (width == 1600 && height == 1200 && px == Y16) {
			vm = FlyCapture2::VIDEOMODE_1600x1200Y16;
			return true;
		}
	}
	return false;

}

bool FlyCaptureGrabber::setResolution(const unsigned int& width,
		const unsigned int& height, const int& pixfor) {

	unsigned int widthtmp = width;
	unsigned int heighttmp = height;
	PixelFormatLocal px = PixelFormatLocal(pixfor);
	FlyCapture2::VideoMode vm;

	if (true == convertVideoMode(widthtmp, heighttmp, px, vm)) {
		FlyCapture2::Error error;
		FlyCapture2::VideoMode pVideoMode;
		FlyCapture2::FrameRate pFrameRate;
		bool supported = false;
		error = this->capturepimpl->cam.GetVideoModeAndFrameRate(&pVideoMode,
				&pFrameRate);
		if (error != FlyCapture2::PGRERROR_OK) {
			error.PrintErrorTrace();
			return false;
		}
		error = this->capturepimpl->cam.GetVideoModeAndFrameRateInfo(vm,
				pFrameRate, &supported);
		if (error != FlyCapture2::PGRERROR_OK) {
			error.PrintErrorTrace();
			return false;
		}
		if (true == supported) {
			bool isr = this->paramspimpl->running;
			stop();
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			error = this->capturepimpl->cam.SetVideoModeAndFrameRate(vm,
					pFrameRate);
			if (error != FlyCapture2::PGRERROR_OK) {
				error.PrintErrorTrace();
				return false;
			}
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
			this->paramspimpl->pixfor = px;
			if (true == isr) {
				play();
			}

			return true;
		} else {
			Log::add().error("FlyCaptureGrabber::setResolution",
					"Resolution not supported by the camera");
		}
	} else {
		Log::add().error("FlyCaptureGrabber::setResolution",
				"Resolution not available");
	}
	return false;
}

bool FlyCaptureGrabber::setFramerate(const float& value) {
	FlyCapture2::FrameRate fr;
	float tmpvalue = value;
	if (true == convertFrameRate(tmpvalue, fr)) {
		FlyCapture2::Error error;
		FlyCapture2::VideoMode pVideoMode;
		FlyCapture2::FrameRate pFrameRate;
		bool supported = false;
		error = this->capturepimpl->cam.GetVideoModeAndFrameRate(&pVideoMode,
				&pFrameRate);
		if (error != FlyCapture2::PGRERROR_OK) {
			error.PrintErrorTrace();
			return false;
		}
		error = this->capturepimpl->cam.GetVideoModeAndFrameRateInfo(pVideoMode,
				fr, &supported);
		if (error != FlyCapture2::PGRERROR_OK) {
			error.PrintErrorTrace();
			return false;
		}
		if (true == supported) {
			bool isr = this->paramspimpl->running;
			stop();
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			error = this->capturepimpl->cam.SetVideoModeAndFrameRate(pVideoMode,
					fr);
			if (error != FlyCapture2::PGRERROR_OK) {
				error.PrintErrorTrace();
				return false;
			}
			this->paramspimpl->framerate = value;
			this->paramspimpl->adaptative_framerate = value;
			if (true == isr) {
				play();
			}
			return true;
		} else {
		}
	}
	return false;
}

void FlyCaptureGrabber::setStreamFilename(const std::string& filename) {
#ifdef WIN32
	UNUSED_VARIABLE(filename);
#endif
}

std::string FlyCaptureGrabber::getStreamFilename(void) const {
	return "";
}

}
