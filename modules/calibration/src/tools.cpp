/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tools.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tools.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/tools.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "gedeon/datatypes/converter.hpp"
#include <cassert>

#include<iostream>
namespace gedeon {

namespace Tools {

void setIntrinsicParametersMatrix(float *intrinsicparameters,
		const float& focal_x, const float& focal_y, const float& center_x,
		const float& center_y, const float& skew) {

	assert(0 != intrinsicparameters);

	intrinsicparameters[0] = focal_x;
	intrinsicparameters[1] = skew;
	intrinsicparameters[2] = center_x;
	intrinsicparameters[3] = 0.0f;
	intrinsicparameters[4] = focal_y;
	intrinsicparameters[5] = center_y;
	intrinsicparameters[6] = 0.0f;
	intrinsicparameters[7] = 0.0f;
	intrinsicparameters[8] = 1.0f;

}

namespace OpenCV {


bool detectChessboard(std::vector<cv::Point2f>& corners, const cv::Mat& image,
		const cv::Size& pattern) {
	bool detected = cv::findChessboardCorners(image, pattern, corners,
			cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_ADAPTIVE_THRESH
					| cv::CALIB_CB_NORMALIZE_IMAGE);
	if (true == detected) {
		cv::TermCriteria criteria = cv::TermCriteria(
				cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 10, 0.01f);
		cv::Mat gray;
		image.copyTo(gray);
		if (3 == image.channels()) {
			cvtColor(gray, gray, cv::COLOR_RGB2GRAY);
		}
		cv::cornerSubPix(gray, corners, cv::Size(5, 5), cv::Size(-1, -1),
				criteria);
	}
	return detected;
}

bool detectCirclesGrid(std::vector<cv::Point2f>& corners, const cv::Mat& image,
		const cv::Size& pattern, const cv::SimpleBlobDetector::Params& params,
		const bool& symmetric) {
	cv::Ptr<cv::FeatureDetector> blobDetector = new cv::SimpleBlobDetector(
			params);
	cv::Mat gray;
	image.copyTo(gray);
	if (3 == image.channels()) {
		cvtColor(gray, gray, cv::COLOR_RGB2GRAY);
	}
	bool detected = false;
	if (true == symmetric) {
		detected = cv::findCirclesGrid(gray, pattern, corners,
				cv::CALIB_CB_SYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING,
				blobDetector);
	} else {
		detected = cv::findCirclesGrid(gray, pattern, corners,
				cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING,
				blobDetector);
	}
	return detected;
}


cv::Mat convertOpenCVToOpenGLExtrinsic(const cv::Mat& opencv_mat) {

	cv::Mat opencv_mat32f;
	opencv_mat.convertTo(opencv_mat32f, CV_32F);
	cv::Mat opengl_mat = cv::Mat::zeros(4, 4, CV_32F);

	/*
	 * Setup transposed translation with -y and -z axis
	 */
	opengl_mat.at<float>(3, 0) =  opencv_mat32f.at<float>(0, 3);
	opengl_mat.at<float>(3, 1) = -opencv_mat32f.at<float>(1, 3);
	opengl_mat.at<float>(3, 2) = -opencv_mat32f.at<float>(2, 3);
	opengl_mat.at<float>(3, 3) = 1.0f;

	/*
	 * Setup transposed rotation with -y and -z axis
	 */
	opengl_mat.at<float>(0, 0) =  opencv_mat32f.at<float>(0, 0);
	opengl_mat.at<float>(1, 0) =  opencv_mat32f.at<float>(0, 1);
	opengl_mat.at<float>(2, 0) =  opencv_mat32f.at<float>(0, 2);

	opengl_mat.at<float>(0, 1) = -opencv_mat32f.at<float>(1, 0);
	opengl_mat.at<float>(1, 1) = -opencv_mat32f.at<float>(1, 1);
	opengl_mat.at<float>(2, 1) = -opencv_mat32f.at<float>(1, 2);

	opengl_mat.at<float>(0, 2) = -opencv_mat32f.at<float>(2, 0);
	opengl_mat.at<float>(1, 2) = -opencv_mat32f.at<float>(2, 1);
	opengl_mat.at<float>(2, 2) = -opencv_mat32f.at<float>(2, 2);

	return opengl_mat;

}

cv::Mat convertOpenCVToOpenGLIntrinsic(const cv::Mat& opencv_mat,
		const unsigned int& width, const unsigned int& height,
		const float& znear, const float& zfar) {

	cv::Mat opencv_mat32f;
	opencv_mat.convertTo(opencv_mat32f, CV_32F);
	cv::Mat opengl_mat = cv::Mat::zeros(4, 4, CV_32F);
	opengl_mat.at<float>(0, 0) = 2.0f * opencv_mat32f.at<float>(0, 0)
			/ static_cast<float>(width);
	opengl_mat.at<float>(1, 1) = 2.0f * opencv_mat32f.at<float>(1, 1)
			/ static_cast<float>(height);
	opengl_mat.at<float>(1, 0) = -2.0f* opencv_mat32f.at<float>(0, 1)/ static_cast<float>(width);
	opengl_mat.at<float>(2, 0) = 1.0f
			- 2.0f * opencv_mat32f.at<float>(0, 2) / static_cast<float>(width);
	opengl_mat.at<float>(2, 1) = -1.0f
			+ 2.0f * opencv_mat32f.at<float>(1, 2) / static_cast<float>(height);
	opengl_mat.at<float>(2, 2) = (znear + zfar) / (znear - zfar);
	opengl_mat.at<float>(2, 3) = -1.0f;
	opengl_mat.at<float>(3, 2) = (2.0f * znear * zfar) / (znear - zfar);
	return opengl_mat;
}

}

bool detectCirclesGrid(std::vector<cv::Point2f>& corners, const RGBImage& image,
		const cv::Size& pattern, const cv::SimpleBlobDetector::Params& params,
		const bool& symmetric) {

	cv::Mat color;
	if(false == Converter::OpenCV::convertImage(color,image)){
		return false;
	}

	return OpenCV::detectCirclesGrid(corners, color, pattern, params, symmetric);
}

bool detectChessboard(std::vector<cv::Point2f>& corners, const RGBImage& image,
		const cv::Size& pattern) {

	cv::Mat color;
	if(false == Converter::OpenCV::convertImage(color,image)){
		return false;
	}
	return OpenCV::detectChessboard(corners, color, pattern);
}

}

}

