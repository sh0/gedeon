/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * vao.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * vao.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRAPHICS__VAO_HPP__
#define GEDEON_GRAPHICS__VAO_HPP__

#include "gedeon/core/core.hpp"

#include <GL/glew.h>

#ifdef USE_CUDA
#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#endif

namespace gedeon {

/**
 * \brief This class manages a custom Vertex Array Object
 *
 * \author Francois de Sorbier
 */
class VAO {

public:

	GLuint vertexarrayid;
	GLuint verticesid;
	GLuint normalsid;
	GLuint texturesid;
	GLuint colorsid;
	GLuint indicesid;

private:

	GLint verticesattribid;
	GLint normalsattribid;
	GLint texturesattribid;
	GLint colorsattribid;

	unsigned int indices;
	bool initialized;
	GLenum memorystorage;
	bool hasshader;

public:
	/**
	 *
	 * \brief Constructor
	 *
	 */
	VAO(void);

	/**
	 *
	 * \brief Initialize the VAO
	 *
	 * \param vertexbuffer The buffer containing a set of 3D vertices
	 * \param sizevertices The number of 3D vertices
	 * \param indicebuffer The indices describing the geometry of the object and referring to the vertex buffer
	 * \param sizeindices The number of indices
	 * \param normalbuffer The array of normals (one per vertex)
	 * \param texturebuffer The array of texture coordinates (one per vertex)
	 * \param colorbuffer The array of RGBA colors (one per vertex)
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the creation was successful, otherwise false
	 */
	bool init(const float* vertexbuffer, const unsigned int& sizevertices,
			const unsigned int* indicebuffer, const unsigned int& sizeindices,
			const float* normalbuffer = 0, const float* texturebuffer = 0,
			const float* colorbuffer = 0, const GLenum& memorystorage =
					GL_DYNAMIC_DRAW);
	/**
	 *
	 * \brief Destructor
	 *
	 */
	~VAO(void);

	/**
	 *
	 * \brief Define the GLSL shader program object that will be attached to the VAO
	 *
	 * The corresponding input variables to use in the glsl shader are
	 * in vec3 vertexPositionIn for vertices,
	 * in vec2 vertexTextureIn for texture coordinates,
	 * in vec4 vertexColorIn for colors,
	 * in vec3 vertexNormalIn for normals.
	 *
	 * \param po The GLSL program object
	 *
	 * \return True if the attachment was successful, otherwise false
	 *
	 */
	bool setProgramObject(const GLuint& po);

	/**
	 *
	 * \brief Return if a program object is attached or not
	 *
	 * \return True if a program object is already attached, otherwise false
	 *
	 */
	bool hasProgramObject(void) {
		return this->hasshader;
	}

	/**
	 *
	 * \brief Update the buffer of vertices with new data
	 *
	 * \param vertexbuffer The buffer containing a set of 3D vertices
	 * \param sizevertices The number of 3D vertices
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the update was successful, otherwise false
	 *
	 */
	bool updateVertices(const float* vertexbuffer,
			const unsigned int& sizevertices, const GLenum& memorystorage =
					GL_DYNAMIC_DRAW);

	/**
	 *
	 * \brief Update the buffer of indices with new data
	 *
	 * \param indicebuffer The indices describing the geometry of the object and referring to the vertex buffer
	 * \param sizeindices The number of indices
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the update was successful, otherwise false
	 *
	 */
	bool updateIndices(const GLuint* indicebuffer,
			const unsigned int& sizeindices, const GLenum& memorystorage =
					GL_DYNAMIC_DRAW);

	/**
	 *
	 * \brief Update the buffer of normals with new data
	 *
	 * \param normalbuffer The buffer containing a set of 3D normals
	 * \param sizenormals The number of normals
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the update was successful, otherwise false
	 *
	 */
	bool updateNormals(const float* normalbuffer,
			const unsigned int& sizenormals, const GLenum& memorystorage =
					GL_DYNAMIC_DRAW);

	/**
	 *
	 * \brief Update the buffer of texture coordinates with new data
	 *
	 * \param texturebuffer The buffer containing a set of texture coordinates
	 * \param sizetextures The number of texture coordinates
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the update was successful, otherwise false
	 *
	 */
	bool updateTextures(const float* texturebuffer,
			const unsigned int& sizetextures, const GLenum& memorystorage =
					GL_DYNAMIC_DRAW);

	/**
	 *
	 * \brief Update the buffer of colors with new data
	 *
	 * \param colorbuffer The buffer containing a set of RGBA colors
	 * \param sizecolor The number of RGBA colors
	 * \param memorystorage describes the usage of the data (GL_STREAM_DRAW, GL_STATIC_DRAW, GL_STREAM_DRAW)  (see glBufferData for more details)
	 *
	 * \return True if the update was successful, otherwise false
	 *
	 */
	bool updateColors(const float* colorbuffer, const unsigned int& sizecolor,
			const GLenum& memorystorage = GL_DYNAMIC_DRAW);

	/**
	 *
	 * \brief Draw the currently defined VAO
	 *
	 * \param aspecttype The appearance of the geometry (GL_POINTS, GL_LINES, GL_TRIANGLES)
	 *
	 */
	void draw(const GLenum& aspecttype) const;

	/**
	 *
	 * \brief Read the currently defined buffer of vertices
	 *
	 * \param pointer Array that will be allocaed and filled with the data
	 *
	 */
	void getVerticesArray(GLvoid ** pointer);

	/**
	 *
	 * \brief Read the currently defined buffer of normals
	 *
	 * \param pointer Array that will be allocaed and filled with the data
	 *
	 */
	void getNormalsArray(GLvoid ** pointer);

	/**
	 *
	 * \brief Read the currently defined buffer of texture coordinates
	 *
	 * \param pointer Array that will be allocaed and filled with the data
	 *
	 */
	void getTexturesArray(GLvoid ** pointer);

	/**
	 *
	 * \brief Read the currently defined buffer of RGBA colors
	 *
	 * \param pointer Array that will be allocaed and filled with the data
	 *
	 */
	void getColorsArray(GLvoid ** pointer);

	/**
	 *
	 * \brief Return if a the VAO has been initialized or not
	 *
	 * \return True if the VAO is initialized, otherwise false
	 *
	 */
	bool isInitialized(void) const {
		return this->initialized;
	}

protected:

	void destroy(void);

};

}

#endif
