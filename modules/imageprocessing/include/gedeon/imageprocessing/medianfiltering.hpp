/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * medianfiltering.hpp created in 09 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * medianfiltering.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__MEDIAN_FILTERING_HPP__
#define GEDEON_IMAGE_PROCESSING__MEDIAN_FILTERING_HPP__

namespace gedeon {

namespace MedianFiltering {

/**
 * \brief Apply the median filter kernel.
 *
 * \param[out] d_output the result of the filter
 * \param d_input the input image already allocated
 * \param radius The radius of the filter
 * \param width the width of the image
 * \param height the height of the image
 * \param channels the number of channels in the image
 *
 * \author Francois de Sorbier
 */
bool apply(float *d_output, float *d_input,
		const unsigned int& radius,
		const unsigned int& width, const unsigned int& height, const unsigned int& channels = 3);

}

}

#endif
