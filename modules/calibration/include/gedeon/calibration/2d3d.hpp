/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 2d3d.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 2d3d.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__2D3D_HPP__
#define GEDEON_CALIBRATION__2D3D_HPP__

#include "gedeon/calibration/projectivecamera.hpp"
#include <opencv2/core/core.hpp>

namespace gedeon {

namespace Calibration2D3D {

/**
 * \brief Estimate the pose of the camera from a set of 2D/3D correspondences
 *
 *	The method is based on the EPnP solver from OpenCV (http://docs.opencv.org/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html?highlight=solvepnp#cv2.solvePnP)
 *
 * \param[out] camera Calibration parameters of the camera. Extrinsic parameters will be filled. Intrinsic and distortion parameters need to be set
 * \param[in] points2d A list of 2d coordinates from the camera
 * \param[in] points3d A list of 3d coordinates from the 3D space
 * \param[in] ransac_iterations The optional number of iterations for the RANSAC-based error minimization
 *
 * \author Francois de Sorbier
 */
void calibrate(ProjectiveCamera& camera, const std::vector< cv::Point2f >& points2d, const std::vector< cv::Point3f >& points3d,
		const unsigned int& ransac_iterations = 0);

}

}

#endif
