/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * texture.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * texture.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRAPHICS__TEXTURE_HPP__
#define GEDEON_GRAPHICS__TEXTURE_HPP__

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace gedeon{

class Texture2D{

public:
    Texture2D(void);

    ~Texture2D(void);

    bool setData(const glm::uvec2& size, const GLubyte *data, const GLenum& format);

    bool setData(const glm::uvec2& size, const GLfloat *data, const GLenum& format);

    void setParameter(const GLenum& pname, const GLint& param);

    void setParameter(const GLenum& pname, const GLfloat& param);

    GLuint getId(void) const
    {
        return this->id;
    }

    glm::uvec2 getSize(void) const
    {
        return this->size;
    }

    void getContent(GLubyte *content, const GLenum& format) const;

    void getContent(GLfloat *content, const GLenum& format) const;


private:
    GLuint id;
    glm::uvec2 size;

};

}


#endif
