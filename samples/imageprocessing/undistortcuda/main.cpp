/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/imageprocessing/undistort.cuh"
#include "gedeon/imageprocessing/undistort.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

#include <cuda_runtime.h>
#include <cuda.h>

using namespace gedeon;


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	if(argc != 2){
		Log::add().error("main","Execution context: undistortcuda [image file]");
		return EXIT_FAILURE;
	}

	/*
	 * Intrinsic parameters and distortion coefficients for the image "distorted.png"
	 */

	float K[9] = {748.3295675, 0., 317.649034635, 0.,
       750.31471157, 238.02775248, 0., 0., 1.};

	float distortion[5] = {-0.383842459, 0.373372748,
       0.002892372, .00071957048, -0.874445979};

	/*
	 * Load the image
	 */
	cv::Mat image = cv::imread(argv[1], cv::IMREAD_COLOR);
	int size = image.size().width * image.size().height;
	Size s(image.size().width,image.size().height);

	/*
	 * Create the output images
	 */
	cv::Mat ouput_image(image.size(),image.type());
	cv::Mat ouput_image_bilinear(image.size(),image.type());

	/*
	 * Compute the undistort map
	 */
	boost::shared_array<float> undistort_map = Undistort::computeDistortionMap(distortion,K,s);
	float *d_undistort_map = 0; 
	GPU::Undistort::updateUndistortMap(&d_undistort_map, undistort_map.get(),s);

	/*
	 * Compute the bilinear interpolation map
	 */
	boost::shared_array<float> bilinear_map = Undistort::computeBilinearInterpolation(undistort_map.get(), s);
	float *d_bilinear_map = 0; 
	GPU::Undistort::updateBilinearInterpolationMap(&d_bilinear_map, bilinear_map.get(), s);


	/*
	 * copy data onto the device and create memory space
	 */
	unsigned char * input_image_device = 0;
	unsigned char * output_image_device = 0;
	cudaMalloc((void**) &(input_image_device), 3 * size * sizeof(unsigned char));
	cudaMemcpy(input_image_device, image.data, 3 * size * sizeof(unsigned char), cudaMemcpyHostToDevice);
	cudaMalloc((void**) &(output_image_device), 3 * size * sizeof(unsigned char));

	unsigned int block_x = 16;
	unsigned int block_y = 16;

	/*
	 * Apply the undistortion without bilinear interpolation
	 */
	if(false == GPU::Undistort::undistort(output_image_device, input_image_device,
			d_undistort_map, s.width, s.height,
			block_x, block_y)){
		return EXIT_FAILURE;
	}
	cudaMemcpy(ouput_image.data, output_image_device, 3 * size * sizeof(unsigned char), cudaMemcpyDeviceToHost);

	/*
	 * Apply the undistortion with bilinear interpolation
	 */
	if(false == GPU::Undistort::undistortBilinear(output_image_device, input_image_device,
			d_undistort_map, d_bilinear_map, s.width, s.height,
			block_x, block_y)){
		return EXIT_FAILURE;
	}
	cudaMemcpy(ouput_image_bilinear.data, output_image_device, 3 * size * sizeof(unsigned char), cudaMemcpyDeviceToHost);

	cudaFree(input_image_device);
	cudaFree(output_image_device);
	cudaFree(d_undistort_map);
	cudaFree(d_bilinear_map);

	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}

		cv::imshow("Input image",image);
		cv::imshow("Ouput image - Undistort",ouput_image);
		cv::imshow("Ouput image - Undistort Bilinear",ouput_image_bilinear);
	}

	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

