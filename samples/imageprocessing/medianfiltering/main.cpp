/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/imageprocessing/medianfiltering.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

using namespace gedeon;


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	if(argc != 2){
		Log::add().error("main","Execution context: medianfiltering [image file]");
		return EXIT_FAILURE;
	}

	/*
	 * Load the image in grayscale
	 */
	cv::Mat image = cv::imread(argv[1],cv::IMREAD_UNCHANGED);
	cv::Mat input_image;
	cv::Mat ouput_image;
	if(image.channels() == 1){
		input_image = cv::Mat(image.size(),CV_32F);
		ouput_image = cv::Mat(image.size(),CV_32F);
		for(int i = 0; i < image.size().height; ++i){
			for(int j = 0; j < image.size().width; ++j){
				input_image.at<float>(i,j) = image.at<unsigned char>(i,j) / 255.0f;
			}
		}
	}else{
		input_image = cv::Mat(image.size(),CV_32FC3);
		ouput_image = cv::Mat(image.size(),CV_32FC3);
		for(int i = 0; i < image.size().height; ++i){
			for(int j = 0; j < image.size().width; ++j){
				cv::Vec3f color;
				color[0] = image.data[(i*image.size().width+j)*3]/255.0f;
				color[1] = image.data[(i*image.size().width+j)*3+1]/255.0f;
				color[2] = image.data[(i*image.size().width+j)*3+2]/255.0f;
				input_image.at<cv::Vec3f>(i,j) = color;
			}
		}
	}

	/*
	 * Apply the median filtering
	 */
	unsigned int radius_kernel = 3;
	if(false == MedianFiltering::apply((float*)ouput_image.data, (float*)input_image.data, radius_kernel,
		input_image.size().width, input_image.size().height,input_image.channels())){
		return EXIT_FAILURE;
	}

	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}

		cv::imshow("Input image",input_image);
		cv::imshow("Ouput image",ouput_image);
	}

	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

