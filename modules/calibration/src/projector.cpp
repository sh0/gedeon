/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * projector.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * projector.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/projector.hpp"
#include <cstring>

namespace gedeon {

namespace Projector {

namespace GrayCode {

unsigned int valueToGrayCode(const unsigned int& num) {
	return (num >> 1) ^ num;
}

unsigned int toValue(const unsigned int& value) {
	unsigned int mask;
	unsigned int num = value;
	for (mask = num >> 1; mask != 0; mask = mask >> 1) {
		num = num ^ mask;
	}
	return num;
}

boost::shared_array<unsigned int> get(int& code_size,
		const unsigned int& size) {
	boost::shared_array<unsigned int> graycode(new unsigned int[size]);

	unsigned int *graycode_ptr = graycode.get();
	for (unsigned int i = 0; i < size; ++i) {
		graycode_ptr[i] = valueToGrayCode(i);
	}

	/*
	 * Find the max level of the gray code
	 */
	unsigned int value = size - 1;
	code_size = 0;
	while (value >>= 1) {
		++code_size;
	}

	return graycode;
}

void encode(unsigned int& code, const bool& value, const unsigned int& level){
	code = code ^ (value << level);
}

unsigned int decode(const unsigned int& code){
	return toValue(code);
}

bool isWhite(const unsigned int& value, const unsigned int& index) {
	return (value >> index) & 1;
}

void createImageC1(unsigned char *grayimage, const unsigned int& width,
		const unsigned int& height,
		const boost::shared_array<unsigned int> &graycode,
		const bool& horizontal_axis, const unsigned int& bit) {

	if (true == horizontal_axis) {
		unsigned int * graycode_ptr = graycode.get();
		for (unsigned int i = 0; i < width; ++i) {
			grayimage[i] = static_cast<unsigned char>(isWhite(graycode_ptr[i],
					bit) * 255);
		}
		for (unsigned int i = 1; i < height; ++i) {
			memcpy(grayimage + width * i, grayimage,
					width * sizeof(unsigned char));
		}
	} else {
		unsigned int * graycode_ptr = graycode.get();
		for (unsigned int i = 0; i < height; ++i) {
			for (unsigned int j = 0; j < width; ++j) {
				grayimage[i * width + j] = static_cast<unsigned char>(isWhite(
						graycode_ptr[i], bit) * 255);
			}
		}
	}

}

void createImageC3(unsigned char *grayimage, const unsigned int& width,
		const unsigned int& height,
		const boost::shared_array<unsigned int> &graycode,
		const bool& horizontal_axis, const unsigned int& bit) {

	if (true == horizontal_axis) {
		unsigned int * graycode_ptr = graycode.get();
		for (unsigned int i = 0; i < width; ++i) {
			grayimage[i * 3] = static_cast<unsigned char>(isWhite(
					graycode_ptr[i], bit) * 255);
			grayimage[i * 3 + 1] = grayimage[i * 3];
			grayimage[i * 3 + 2] = grayimage[i * 3];
		}
		for (unsigned int i = 1; i < height; ++i) {
			memcpy(grayimage + width * i * 3, grayimage,
					3 * width * sizeof(unsigned char));
		}
	} else {
		unsigned int * graycode_ptr = graycode.get();
		for (unsigned int i = 0; i < height; ++i) {
			for (unsigned int j = 0; j < width; ++j) {
				grayimage[(i * width + j) * 3] =
						static_cast<unsigned char>(isWhite(graycode_ptr[i], bit)
								* 255);
				grayimage[(i * width + j) * 3 + 1] = grayimage[(i * width + j)
						* 3];
				grayimage[(i * width + j) * 3 + 2] = grayimage[(i * width + j)
						* 3];
			}
		}
	}

}

}

}

}

