/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereyegrabber.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereyegrabber.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_GRABBER__TIGEREYEGRABBER_HPP__
#define GEDEON_GRABBER__TIGEREYEGRABBER_HPP__

#include "gedeon/grabber/grabbergeneric.hpp"
#include "gedeon/core/core.hpp"
#include <pcap.h>
#include <boost/thread.hpp>

namespace gedeon{

class TigerEyeGrabberCapture;
class TigerEyeGrabberParams;
class TigerEyeGrabberThread;
class TigerEyeGrabberIO;


/**
 * \brief Class for communicating with the ASC TigerEye Camera
 *
 * The class was written based on the specifications described in the version 2.2 of EtherIO manual
 *
 * \author Francois de Sorbier
 */

class TigerEyeGrabber: public Grabber, public EventSender, public EventReceiver{

public:

	/**
	 * \brief Constructor of the class
	 *
	 */
	TigerEyeGrabber(void);

	/**
	 * \brief Initialize the grabber
	 *
	 * \param id the id of the device based on its position in the list returned by the driver
	 * \param driver the driver for the ASC Tiger Eye Camera
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	bool init(const unsigned int & id, Driver* driver);

	/**
	 * \brief Initialize the grabber with a file or directory
	 *
	 * \param source the name of a valid file or directory
	 * \param driver the driver for the ASC Tiger Eye Camera
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	bool init(const std::string& source, Driver* driver);

	/**
	 * \brief Destructor of the class
	 */
	~TigerEyeGrabber(void);

	/**
	 * \brief Return the specific name of the device defined as the mac address
	 *
	 * \return the name of the device
	 */
	std::string getName(void) const;

	/**
	 * \brief Return the id of the device referencing its position in the driver's list
	 *
	 * \return the id
	 */
	unsigned int getID(void) const;

	/**
	 * \brief Return the rgbdi image captured by the device
	 *
	 * There is no color for this camera
	 *
	 * \return The RGBDI image
	 */
	const RGBDIImagePtr& getImage(void) const;

	/**
	 * \brief Return the value related to one parameter of the device
	 *
	 * The parameters can be
	 * TIGEREYE_FRAMERATE: The current framerate of the device (Hz)
	 * TIGEREYE_DETECTOR_GAIN: The Detector Gain controls the bias on the detector. In general the best signal-tonoise-ratio (SNR) is obtained with a high gain in the detector. High gain in the detector increases the detector bandwidth (frequency response of the detector) butreduces gain uniformity across the detector. (percentage)
	 * TIGEREYE_AMPLIFIER_GAIN: Signals from the detector are amplified in the readout integrated circuit (ROIC). The Amplifier Gain controls this gain. (percentage)
	 * TIGEREYE_THRESHOLD_FILTER: The Threshold Level defines the threshold at which a return is detected. Setting this parameter too low will keep any return from being detected at all. On the other hand, noise will be detected if this parameter is set too high. (percentage)
	 * TIGEREYE_THRESHOLD_LEVEL: The Threshold Filter filters out short trigger pulses by requiring input signals to remain above the threshold longer. Higher settings of this parameter filter less and result in increased sensitivity. (percentage)
	 * TIGEREYE_LASER_DRIVE: Laser drive shows how much time the camera needs to fire the laser. (percentage)
	 * TIGEREYE_SENSOR_TEMPERATURE: The temperature of the sensor. Camera operation range is from 20 ~ 35. (celsius)
	 * TIGEREYE_LASER_TEMPERATURE: The temperature of the laser. (celsius)
	 * TIGEREYE_SENSOR_VOLTAGE: Voltage of the sensor. It should be very close to 6.3 Volt. (Volt)
	 * TIGEREYE_SENSOR_HEAT_SINK_TEMPERATURE: This number is information only because the camera cannot control this temperature. The normal expected values are < 40 degrees. (celsius)
	 * TIGEREYE_LASER_HEAT_SINK_TEMPERATURE: This number is information and the normal expected values are < 40 degrees. (celsius)
	 * TIGEREYE_LASER_DIODE_TEMPERATURE: This number should closely follow Laser Temperature with slight variations because this number represents data from an alternative laser temperature sensor. (celsius)
	 * TIGEREYE_LASER_CAPACITY_VOLTAGE: The operation range is from 16 ~ 32. (Volt)
	 * TIGEREYE_PROCESS_BOARD_TEMPERATURE: The operation range is from 0 ~ 60.
	 * TIGEREYE_RESET_COUNTDOWN: Number of remaining seconds to have the device operational.
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] value is set with the value of the parameter
	 *
	 * \sa GrabberParameter
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool getParameter(const GrabberParameter& param, float& value);

	/**
	 * \brief Return the value related to several parameters of the device
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] values is set with the values of the parameter. The array has to be allocated beforehand.
	 * \param[out] size size of the array
	 *
	 * \sa GrabberParameter
	 * \return true if the parameter is found, false otherwise
	 */
	bool getParameter(const GrabberParameter& param, float* values, unsigned int& size);

	/**
	 * \brief Set a parameter of the device with a given value
	 *
	 * The parameters can be
	 * TIGEREYE_FRAMERATE: The current framerate of the device (Hz)
	 * TIGEREYE_DETECTOR_GAIN: The Detector Gain controls the bias on the detector. In general the best signal-tonoise-ratio (SNR) is obtained with a high gain in the detector. High gain in the detector increases the detector bandwidth (frequency response of the detector) butreduces gain uniformity across the detector. (percentage)
	 * TIGEREYE_AMPLIFIER_GAIN: Signals from the detector are amplified in the readout integrated circuit (ROIC). The Amplifier Gain controls this gain. (percentage)
	 * TIGEREYE_THRESHOLD_FILTER: The Threshold Level defines the threshold at which a return is detected. Setting this parameter too low will keep any return from being detected at all. On the other hand, noise will be detected if this parameter is set too high. (percentage)
	 * TIGEREYE_THRESHOLD_LEVEL: The Threshold Filter filters out short trigger pulses by requiring input signals to remain above the threshold longer. Higher settings of this parameter filter less and result in increased sensitivity. (percentage)
	 *
	 * \param param defines the parameter we want to set with the value
	 * \param value used for setting the parameter
	 *
	 * \sa GrabberParameter
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool setParameter(const GrabberParameter& param, const float& value);

	/**
	 * \brief Trigger a functionality of the device
	 *
	 * The parameters can be
	 * TIGEREYE_NUC: The Non-Uniformity Correction (NUC) command is used to alleviate background noise effect.
	 * TIGEREYE_RESET: The Reset command can restore the camera back to its power-up status. All parameters will be reset after the camera receives this command. The camera will be ready for other commands and capture after 15 seconds.
	 * TIGEREYE_RECORD: Activate or desactivate the recording state of the device. Data are recorded in the original data format of tiger eye.
	 * \param trig defines the parameter we want to trigger
	 *
	 * \sa GrabberTrigger
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool trigger(const GrabberTrigger& trig);

	/**
	 * \brief Return if the capture is currently active or not
	 *
	 * \return true if capture is active, false otherwise
	 */
	bool isGrabbing(void) const;

	/**
	 * \brief Return if the camera has a color image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasColor(void) const;

	/**
	 * \brief Return if the camera has a intensity image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasIntensity(void) const;

	/**
	 * \brief Return if the camera has a depth image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasDepth(void) const;



public:

	/**
	 * \brief Pause the capture.
	 *
	 * The device is still active.
	 * the function can be called again to unpause the capture.
	 */
	void pause(void);

	/**
	 * \brief Start the capture.
	 *
	 * The function can also unpause the device.
	 */
	void play(void);

	/**
	 * \brief Stop the capture.
	 *
	 * The device is no more active.
	 */
	void stop(void);

	/**
	 * \brief Grab one single image
	 *
	 * This functionality is only working with recorded data and may not be always available
	 *
	 * While grabbing one single image any currently playing is stopped
	 *
	 */
	void grabOneFrame(void);

	/**
	 * \brief set the stream filename for reading or recording
	 *
	 * \param filename The name of the recording/reading file
	 */
	void setStreamFilename(const std::string& filename);

	/**
	 * \brief get the stream filename for reading or recording
	 *
	 * \return the current filename
	 */
	std::string getStreamFilename(void) const;

private:
	void run(void);

	bool setFrameRate(const float& frequency);
	bool setDetectorGain(const float& voltage);
	bool setAmplifierGain(const float& voltage);
	bool setThresholdFilter(const float& voltage);
	bool setThresholdLevel(const float& voltage);
	bool takeOneImage(void);
	bool connectCommand(void);
	bool setNUC(void);
	void reset(void);

	void updateStatus(const unsigned char* data);

	/*
	 * Read a given file in the txt tigereye format
	 */
	void readLocalFile(void);

	TigerEyeGrabberCapture *capturepimpl;
	TigerEyeGrabberParams *paramspimpl;
	TigerEyeGrabberThread *threadpimpl;
	TigerEyeGrabberIO *iopimpl;


};

}


#endif
