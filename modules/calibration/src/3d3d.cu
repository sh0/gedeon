/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 3d3d.cu created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 3d3d.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/core/core.cuh"

#include "gedeon/calibration/3d3d.cuh"

namespace gedeon {

namespace GPU {

namespace Calibration3D3D {

__device__ bool projectivePointPlaneAssociationKernel(float3& previous_vertex,
		float3& previous_normal, float3& current_vertex, const int& x,
		const int& y, const int& width, const int& height, float* input_v_map,
		float * input_n_map, float* previous_v_map, float * previous_n_map,
		float *rotation, float *translation, float *intrinsic) {

	float3 input_normal = makeFloat3FromArray(input_n_map, x, y, width);

	if (true == isnan(input_normal.x)) {
		return false;
	}

	/*
	 * Transform the input vertex into the previous view
	 */
	float3 input_vertex = makeFloat3FromArray(input_v_map, x, y, width);
	float3 trans = make_float3(translation[0], translation[1], translation[2]);

	current_vertex = rotation * input_vertex + trans;

	if (true == isnan(current_vertex.x)) {
		return false;
	}

	/*
	 * Project the vertex
	 */
	int2 projected_vertex;
	projected_vertex.x = -__float2int_rn(
			intrinsic[0] * current_vertex.x / current_vertex.z + intrinsic[2]
					- width);
	projected_vertex.y = -__float2int_rn(
			intrinsic[4] * current_vertex.y / current_vertex.z + intrinsic[5]
					- height);


	if (projected_vertex.x < 0 || projected_vertex.x >= width
			|| projected_vertex.y < 0 || projected_vertex.y >= height) {
		return false;
	}

	previous_normal = makeFloat3FromArray(previous_n_map, projected_vertex.x,
			projected_vertex.y, width);

	if (true == isnan(previous_normal.x)) {
		return false;
	}

	previous_vertex = makeFloat3FromArray(previous_v_map, projected_vertex.x,
			projected_vertex.y, width);
	if (true == isnan(previous_vertex.x)) {
		return false;
	}

	/*
	 * Check the distance between the current and the previous vertices
	 */
	float dist = norm(previous_vertex - current_vertex);
	if (dist > 0.05f) { // 10cm
		return false;
	}

	/*
	 * Check the angle between the normals
	 */
	float sine = norm(cross(input_normal, previous_normal));
	if (sine > 0.003f) {  //sin(10*3.14159254/180)
		return false;
	}

	return true;
}

template<unsigned int blockSize>
__global__ void ICPKernel(float* outputbuffer, float* previousvertexmap,
		float* previousnormalmap, float* inputvertexmap, float* inputnormalmap,
		float *rotation, float *translation, float *intrinsic,
		const int width, const int height) {

	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width && y < height) {

		float3 previous_vertex, previous_normal, current_vertex;

		//
		// Find the correspondence between the input and the stored image
		//
		bool found_corresp = projectivePointPlaneAssociationKernel(
				previous_vertex, previous_normal, current_vertex, x, y, width,
				height, inputvertexmap, inputnormalmap, previousvertexmap,
				previousnormalmap, rotation, translation,
				intrinsic);

		//
		// Estimate the system (idea of combined computation based on Kinfu from PCL)
		//
		float row[7];

		if (true == found_corresp) {
			*(float3*) &row[0] = cross(current_vertex, previous_normal);
			*(float3*) &row[3] = previous_normal;
			row[6] = dot(previous_normal, previous_vertex - current_vertex);
		} else {
			row[0] = row[1] = row[2] = row[3] = row[4] = row[5] = row[6] = 0.f;
		}

		__shared__ float smem[blockSize];
		int tid = threadIdx.z * blockDim.x * blockDim.y
				+ threadIdx.y * blockDim.x + threadIdx.x;
		int shift = 0;
		//
		// See KinectFusion: Real-Time Dense Surface Mapping and Tracking for details
		// Compute the parts of the system Sum((A^T) * (A)) x = sum((A^T) * b) where x is the unknown
		// with A^T = (G^T) * N_prev
		// b = (N_prev^T) * (V_prev-Vcurrent)
		// ang G = [{V_current} | I] where {.} defines the skew-symmetric matrix
		// Compute only the upper triangular part of the system since it is symmetric
		//
		for (int i = 0; i < 6; ++i) { //rows
#pragma unroll
			for (int j = i; j < 7; ++j) { // cols + b
				__syncthreads();
				smem[tid] = row[i] * row[j];
				__syncthreads();

				reduceSumInPlace<blockSize>(smem);

				if (tid == 0) {
					int size = gridDim.x * gridDim.y;
					outputbuffer[size * (shift++)
							+ (blockIdx.x + gridDim.x * blockIdx.y)] = smem[0];
					// 0<= shift < 27

				}
			}
		}
	}
}

template<unsigned int blockSize>
__global__ void ReduceSumKernel(float* result, float* array,
		const int size_array) {

	ReduceSum<blockSize>(result, array + size_array * blockIdx.x, size_array);

}

bool computeSystem(float *A, float* b, float* previousvertexmap,
		float* previousnormalmap, float* inputvertexmap, float* inputnormalmap,
		float *rotation, float *translation, float *intrinsic, const int& width,
		const int& height, const unsigned int& block_x,
		const unsigned int& block_y) {

	const dim3 blockSize(block_x, block_y);
	const dim3 gridSize((width + blockSize.x - 1) / blockSize.x,
			(height + blockSize.y - 1) / blockSize.y);

	float* outputbuffer = 0;
	cudaMalloc((void**) &(outputbuffer),
			27 * gridSize.x * gridSize.y * sizeof(float));

	const unsigned int blocksize = block_x * block_y;
	switch (blocksize) {
	case 1024:
		ICPKernel<1024><<< gridSize, blockSize>>>(outputbuffer, previousvertexmap,
				previousnormalmap, inputvertexmap, inputnormalmap,
				rotation, translation, intrinsic, width, height);
		break;
	case 512:
		ICPKernel<512><<< gridSize, blockSize>>>(outputbuffer, previousvertexmap,
				previousnormalmap, inputvertexmap, inputnormalmap,
				rotation, translation, intrinsic, width, height);
		break;
	case 256:
		ICPKernel<256><<< gridSize, blockSize>>>(outputbuffer, previousvertexmap,
				previousnormalmap, inputvertexmap, inputnormalmap,
				rotation, translation, intrinsic, width, height);
		break;
	case 128:
		ICPKernel<128><<< gridSize, blockSize>>>(outputbuffer, previousvertexmap,
				previousnormalmap, inputvertexmap, inputnormalmap,
				rotation, translation, intrinsic, width, height);
		break;
	}

	float *d_sum = 0;
	cudaMalloc((void**) &(d_sum), 27 * sizeof(float));

	const unsigned int size = gridSize.x * gridSize.y;
	if (size > 512) {
		ReduceSumKernel<1024> <<< 27, 1024>>>(d_sum, outputbuffer, size);
	} else {
		if(size > 256) {
			ReduceSumKernel<512> <<< 27, 512>>>(d_sum, outputbuffer, size);
		} else {
			if(size > 128) {
				ReduceSumKernel<256> <<< 27, 256>>>(d_sum, outputbuffer, size);
			} else {
				ReduceSumKernel<128> <<< 27, 128>>>(d_sum, outputbuffer, size);
			}
		}
	}

	float sum[27];
	cudaMemcpy(sum, d_sum, 27 * sizeof(float), cudaMemcpyDeviceToHost);


	int shift = 0;
	for (int i = 0; i < 6; ++i) {
		for (int j = i; j < 7; ++j) {
			float value = sum[shift++];
			if (j == 6) {
				b[i] = value;
			} else {
				A[j * 6 + i] = A[i * 6 + j] = value;
			}
		}
	}

	cudaFree(d_sum);
	cudaFree(outputbuffer);

	return true;
}

}

}

}
