/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * gedeonfilegrabber.cpp created in 11 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * gedeonfilegrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/gedeonfilegrabber.hpp"
#include "gedeon/datatypes/io.hpp"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace gedeon {

class GedeonFileGrabberParams {
public:
	GedeonFileGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~GedeonFileGrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;

};

class GedeonFileGrabberIO {
public:
	GedeonFileGrabberIO(void) :
			local_streaming(false) {
	}

	~GedeonFileGrabberIO(void) {
		importer.close();
		loader.close();
	}

	bool local_streaming;

	IO::DataImporter importer;
	IO::DataLoader loader;
};

class GedeonFileGrabberCapture {
public:
	GedeonFileGrabberCapture(void) :
			devicename("Gedeon-File"), device_id(0) {
	}
	~GedeonFileGrabberCapture(void) {
	}

	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;
};

class GedeonFileGrabberThread {
public:
	GedeonFileGrabberThread(void) :
			pthread(0) {
	}
	~GedeonFileGrabberThread(void) {
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

GedeonFileGrabber::GedeonFileGrabber(void) :
		Grabber(), capturepimpl(new GedeonFileGrabberCapture()), paramspimpl(
				new GedeonFileGrabberParams()), threadpimpl(
				new GedeonFileGrabberThread()), iopimpl(
				new GedeonFileGrabberIO()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool GedeonFileGrabber::init(const unsigned int & id, Driver* d) {
#ifdef WIN32
	UNUSED_VARIABLE(d);
	UNUSED_VARIABLE(id);
#endif

	Log::add().error("GedeonFileGrabber::init",
			"Not available for this grabber");
	return false;
}

bool GedeonFileGrabber::init(const std::string& source, Driver* d) {
#ifdef WIN32
	UNUSED_VARIABLE(d);
#endif

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("GedeonFileGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->paramspimpl->hascolor = false;
	this->paramspimpl->hasdepth = false;
	this->paramspimpl->hasintensity = false;

	if (true == is_directory(source)) {
		if (false == this->iopimpl->loader.open(source)) {
			Log::add().error("GedeonFileGrabber::init",
					"Failed to open the directory " + source);
			return false;
		}
	} else {
		std::string namelower = source;
		std::transform(namelower.begin(), namelower.end(), namelower.begin(),
				::tolower);
		if (true
				== boost::algorithm::ends_with(namelower,
						IO::IMPORTEXPORT_FILENAME_EXTENSION)) {
			if (false == this->iopimpl->importer.open(source)) {
				Log::add().error("GedeonFileGrabber::init",
						"Failed to open the file " + source);
				return false;
			}
		} else {
			Log::add().warning("GedeonFileGrabber::init",
					"Format not supported");
			return false;

		}
	}

	this->capturepimpl->rgbdi.reset(new RGBDIImage);

	if (this->iopimpl->importer.isOpened() == true) {
		updateFromImporter();
	} else {
		updateFromDirectory();
	}

	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
	this->iopimpl->local_streaming = true;

	this->capturepimpl->devicename = std::string("File (" + source + ")");

	this->threadpimpl->updated->sender = this;
	this->threadpimpl->ready->sender = this;
	this->threadpimpl->stopped->sender = this;
	this->threadpimpl->playing->sender = this;
	this->threadpimpl->paused->sender = this;
	this->paramspimpl->initialized = true;
	emitEvent(this->threadpimpl->ready);

	return true;
}

GedeonFileGrabber::~GedeonFileGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}

	delete paramspimpl;
	delete threadpimpl;
	delete iopimpl;
	delete capturepimpl;

}

std::string GedeonFileGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int GedeonFileGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& GedeonFileGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool GedeonFileGrabber::getParameter(const GrabberParameter& param,
		float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (GEDEONFILE_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}

	return false;
}

bool GedeonFileGrabber::getParameter(const GrabberParameter& param,
		float* values, unsigned int& size) {
#ifdef WIN32
	UNUSED_VARIABLE(size);
	UNUSED_VARIABLE(values);
	UNUSED_VARIABLE(param);
#endif

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool GedeonFileGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (GEDEONFILE_FRAMERATE == param) {
		this->paramspimpl->framerate = value;
		return true;
	}

	return false;
}

bool GedeonFileGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (GEDEONFILE_LOOP == trig) {
		this->iopimpl->importer.triggerLoop();
		this->iopimpl->loader.triggerLoop();
	}

	return false;
}

bool GedeonFileGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool GedeonFileGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool GedeonFileGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool GedeonFileGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void GedeonFileGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void GedeonFileGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&GedeonFileGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void GedeonFileGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
			emitEvent(this->threadpimpl->stopped);
		}
	}
}

void GedeonFileGrabber::grabOneFrame(void) {
	if (false == this->paramspimpl->paused) {
		if(true == this->iopimpl->loader.isOpened()){
			stop();
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			updateFromDirectory();
			emitEvent(this->threadpimpl->updated);
		}
		if (true == this->iopimpl->importer.isOpened()) {
			stop();
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			updateFromImporter();
			emitEvent(this->threadpimpl->updated);
		}
	}
}

void GedeonFileGrabber::updateFromImporter(void) {
	this->iopimpl->importer.read(this->capturepimpl->rgbdi,
			this->paramspimpl->framerate);
	if (this->capturepimpl->rgbdi.get()->color.getData() == 0) {
		this->paramspimpl->hascolor = false;
	} else {
		this->paramspimpl->hascolor = true;
	}
	if (this->capturepimpl->rgbdi.get()->depth.getData() == 0) {
		this->paramspimpl->hasdepth = false;
	} else {
		this->paramspimpl->hasdepth = true;
	}
	if (this->capturepimpl->rgbdi.get()->intensity.getData() == 0) {
		this->paramspimpl->hasintensity = false;
	} else {
		this->paramspimpl->hasintensity = true;
	}
	if (this->paramspimpl->framerate <= 0.0f) {
		this->paramspimpl->framerate = DEFAULT_FRAMERATE;
	}
	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
}

void GedeonFileGrabber::updateFromDirectory(void) {
	this->iopimpl->loader.load(this->capturepimpl->rgbdi);
	if (this->capturepimpl->rgbdi.get()->color.getData() == 0) {
		this->paramspimpl->hascolor = false;
	} else {
		this->paramspimpl->hascolor = true;
	}
	if (this->capturepimpl->rgbdi.get()->depth.getData() == 0) {
		this->paramspimpl->hasdepth = false;
	} else {
		this->paramspimpl->hasdepth = true;
	}
	if (this->capturepimpl->rgbdi.get()->intensity.getData() == 0) {
		this->paramspimpl->hasintensity = false;
	} else {
		this->paramspimpl->hasintensity = true;
	}
	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
}

void GedeonFileGrabber::run(void) {

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();
		if (false == this->paramspimpl->paused) {

			bool send_update = false;

			if (this->iopimpl->importer.isOpened() == true
					&& false == this->paramspimpl->paused) {

				boost::mutex::scoped_lock l(this->threadpimpl->mutex);
				updateFromImporter();
				send_update = true;
			}

			if (this->iopimpl->loader.isOpened() == true
					&& false == this->paramspimpl->paused) {

				boost::mutex::scoped_lock l(this->threadpimpl->mutex);
				updateFromDirectory();
				send_update = true;
			}

			/*
			 * Emit a message to confirm the update
			 */
			if (true == send_update) {
				emitEvent(this->threadpimpl->updated);
			}
		}

		current_time = static_cast<long>(1000.0
				/ this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	timer.stop();
	this->paramspimpl->finished = true;

}

void GedeonFileGrabber::setStreamFilename(const std::string& filename) {
#ifdef WIN32
	UNUSED_VARIABLE(filename);
#endif
}

std::string GedeonFileGrabber::getStreamFilename(void) const {
	return "";
}

}
