/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/openni2driver.hpp"
#include "gedeon/grabber/openni2grabber.hpp"
#include "gedeon/datatypes/io.hpp"
#include "gedeon/datatypes/converter.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace gedeon;
cv::Mat intensity;
cv::Mat color;
cv::Mat depth;
boost::mutex mutex;

OpenNI2Grabber sensor;
IO::DataSaver saver;
IO::DataExporter exporter;
const std::string filename_base("openni2-output");
const unsigned int compression_level = 4;

void displayImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				RGBDIImage *imgptr = s->getImage().get();
				if (true == s->hasIntensity()) {
					boost::mutex::scoped_lock l(mutex);
					Converter::OpenCV::convertImage(intensity,imgptr->intensity);
				}
				if (true == s->hasColor()) {
					boost::mutex::scoped_lock l(mutex);
					Converter::OpenCV::convertImage(color,imgptr->color);
				}
				if (true == s->hasDepth()) {
					boost::mutex::scoped_lock l(mutex);
					Converter::OpenCV::convertImage(depth,imgptr->depth);
				}
			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}


void recordImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				RGBDIImagePtr imgptr = s->getImage();

				if (true == exporter.isOpened()) {
					float fr = 30.0f;
					sensor.getParameter(OPENNI2_FRAMERATE, fr);
					exporter.write(imgptr, fr);
				}

				if (true == saver.isOpened()) {
					std::string timestamp = IO::numberToString(getTimeStamp());
					saver.save(timestamp, imgptr);
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}



int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	OpenNI2Driver& driver = OpenNI2Driver::getInstance();

	std::cout << "Looking for " << driver.getName() << " devices..."
			<< std::endl;
	driver.refresh();

	if (2 == argc) {
		if (false == sensor.init(argv[1], &driver)) {
			return EXIT_FAILURE;
		}
	} else {
		std::cout << "Number of devices found: " << driver.getCount()
				<< std::endl;
		if (driver.getCount() == 0) {
			std::cerr << "No device available" << std::endl;
			return EXIT_FAILURE;
		}

		std::vector<std::string> devices = driver.populate();
		std::vector<std::string>::iterator it = devices.begin();
		while (devices.end() != it) {
			std::cout << "* " << *it << std::endl;
			++it;
		}
		std::cout << "Access the first device..." << std::endl;
		if (false == sensor.init(0, &driver)) {
			return EXIT_FAILURE;
		}
	}

	boost::signals2::connection c;
	boost::signals2::connection c2;
	try {
		if (false
				== sensor.connect("updated", boost::bind(displayImage, _1),
						c)) {
			std::cerr << "Error: Cannot connect to the event of the sensor"
					<< std::endl;
		}
		if (false
				== sensor.connect("updated", boost::bind(recordImage, _1),
						c2)) {
			std::cerr << "Error: Cannot connect to the event of the sensor"
					<< std::endl;
		}

	} catch (Exception e) {
		std::cerr << e << std::endl;
	}

	Log::add().info(sensor.getName() + " Connected");

	sensor.play();

	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}
		if( k == 'h'){
			std::cout << "Help" << std::endl;
			std::cout << "------------" << std::endl;
			std::cout << "q: quit the program" << std::endl;
			std::cout << "p: pause" << std::endl;
			std::cout << "g: export in a gmv file" << std::endl;
			std::cout << "d: export in directories" << std::endl;
			std::cout << "s: save images as png files" << std::endl;
			std::cout << "t: export in openni format (.oni)" << std::endl;
			std::cout << "i: show intensity" << std::endl;
			std::cout << "c: show color" << std::endl;
			std::cout << "w: trigger auto white balance" << std::endl;
			std::cout << "e: trigger auto exposure" << std::endl;
			std::cout << "v: VGA resolution" << std::endl;
			std::cout << "b: QVGA resolution" << std::endl;
			std::cout << "3: 30 fps framerate" << std::endl;
			std::cout << "6: 60 fps framerate" << std::endl;

		}
		if (k == 'r') {
			sensor.play();
		}
		if (k == 'o') {
			sensor.grabOneFrame();
		}
		if (k == 'p') {
			sensor.pause();
		}
		if ('g' == k) {
			if (false == exporter.isOpened()) {
				exporter.open(filename_base + ".gmv", compression_level);
			} else {
				exporter.close();
			}
		}
		if ('d' == k) {
			if (false == saver.isOpened()) {
				saver.open(filename_base);
			} else {
				saver.close();
			}
		}

		if (k == 't') {
			sensor.trigger(OPENNI2_INTERNAL_RECORD);
		}
		if (k == 'i') {
			sensor.trigger(OPENNI2_GRAB_INTENSITY);
			cv::destroyWindow("Color");
			color.release();
		}
		if (k == 'c') {
			sensor.trigger(OPENNI2_GRAB_COLOR);
			cv::destroyWindow("Intensity");
			intensity.release();
		}
		if (k == 'w') {
			sensor.trigger(OPENNI2_AUTO_WHITE_BALANCE);
		}
		if (k == 'e') {
			sensor.trigger(OPENNI2_AUTO_EXPOSURE);
		}
		if (k == 'v') {
			sensor.trigger(OPENNI2_VGA);
		}
		if (k == 'b') {
			sensor.trigger(OPENNI2_QVGA);
		}
		if (k == '3') {
			sensor.trigger(OPENNI2_30HZ_FRAMERATE);
		}
		if (k == '6') {
			sensor.trigger(OPENNI2_60HZ_FRAMERATE);
		}
		if ('s' == k) {
			unsigned long long ts = getTimeStamp();
			if (intensity.data != 0) {
				cv::imwrite("intensity-" + IO::numberToString(ts) + ".png",
						intensity);
			}
			if (color.data != 0) {
				cv::imwrite("color-" + IO::numberToString(ts) + ".png", color);
			}
			if (depth.data != 0) {
				cv::imwrite("depth-" + IO::numberToString(ts) + ".png", depth);
			}
		}
		if (intensity.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Intensity", intensity);
		}
		if (color.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Color", color);
		}
		if (depth.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			double minVal, maxVal;
			cv::minMaxLoc(depth, &minVal, &maxVal);
			cv::imshow("Depth", depth*1.0f/maxVal);
		}
	}

	std::cout << "Stop the capture..." << std::endl;
	sensor.stop();
	c.disconnect();
	c2.disconnect();
	cv::destroyAllWindows();
	return EXIT_SUCCESS;
}

