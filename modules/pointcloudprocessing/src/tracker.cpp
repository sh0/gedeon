/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tracker.hpp created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tracker.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/pointcloudprocessing/tracker.hpp"
#include "gedeon/calibration/3d3d.cuh"
#include "gedeon/pointcloudprocessing/normalestimation.cuh"
#include "gedeon/calibration/calibration.cuh"
#include "gedeon/imageprocessing/bilateralfiltering.cuh"
#include "gedeon/imageprocessing/downsampling.cuh"
#include "gedeon/core/core.hpp"
#include <cuda.h>
#include <cuda_runtime.h>

#include <opencv2/highgui/highgui.hpp>
namespace gedeon {

namespace GPU {

static const unsigned int SIZE_MAT_A = 36;
static const unsigned int SIZE_VEC_B = 6;

static const unsigned int iterations[3] = { 4, 5, 10 }; // From "KinectFusion: Real-time Dense Surface Mapping and Tracking"
static const unsigned int NB_LEVELS = 3;

static const unsigned int radius = 3;
static const float space_coefficient = 18.0f;
static const float range_coefficient = 0.1f;

static const int block_x = 16;
static const int block_y = 8;

Tracker::Tracker(const unsigned int& width_, const unsigned int& height_,
		float intrinsic_[9]) {

	this->firstframe = true;
	cudaMalloc((void**) &(this->d_rotation), 9 * sizeof(float));
	cudaMalloc((void**) &(this->d_translation), 3 * sizeof(float));
	this->Amat = cv::Mat(SIZE_VEC_B, SIZE_VEC_B, CV_32F);
	this->bmat = cv::Mat(SIZE_VEC_B, 1, CV_32F);
	this->rotation_inc = cv::Mat::eye(3,3,CV_32F);
	this->translation_inc = cv::Mat::zeros(3,1,CV_32F);


	this->d_intrinsic = new float *[NB_LEVELS];
	this->intrinsic = new float*[NB_LEVELS];
	float tmpintrinsic[9];
	memcpy(tmpintrinsic, intrinsic_, 9 * sizeof(float));
	if (0 != intrinsic) {
		for (int i = NB_LEVELS - 1; i >= 0; --i) {
			cudaMalloc((void**) &(this->d_intrinsic[i]), 9 * sizeof(float));
			cudaMemcpy(this->d_intrinsic[i], tmpintrinsic, 9 * sizeof(float),
					cudaMemcpyHostToDevice);
			this->intrinsic[i] = new float[9];
			memcpy(intrinsic[i], tmpintrinsic, 9 * sizeof(float));
			for(int i = 0; i < 9; ++i){
				tmpintrinsic[i] /= 2.0f;
			}
		}


	}

	this->width = width_;
	this->height = height_;

	this->d_p_vertexmaps = new float*[NB_LEVELS];
	this->d_p_normalmaps = new float*[NB_LEVELS];
	this->d_c_vertexmaps = new float*[NB_LEVELS];
	this->d_c_normalmaps = new float*[NB_LEVELS];
	this->d_depthmaps = new float*[NB_LEVELS];
	int w = width_;
	int h = height_;
	for (int i = NB_LEVELS - 1; i >= 0; --i) {
		cudaMalloc((void**) &(this->d_p_vertexmaps[i]),
				w * h * 3 * sizeof(float));
		cudaMalloc((void**) &(this->d_p_normalmaps[i]),
				w * h * 3 * sizeof(float));
		cudaMalloc((void**) &(this->d_c_vertexmaps[i]),
				w * h * 3 * sizeof(float));
		cudaMalloc((void**) &(this->d_c_normalmaps[i]),
				w * h * 3 * sizeof(float));
		cudaMalloc((void**) &(this->d_depthmaps[i]), w * h * 3 * sizeof(float));
		w /= 2;
		h /= 2;
	}

}

Tracker::~Tracker(void) {

	cudaFree(d_rotation);
	cudaFree(d_translation);
	cudaFree(d_intrinsic);
	for (int i = NB_LEVELS - 1; i >= 0; --i) {
		cudaFree(this->d_p_vertexmaps[i]);
		cudaFree(this->d_p_normalmaps[i]);
		cudaFree(this->d_depthmaps[i]);
		cudaFree(this->d_c_normalmaps[i]);
		cudaFree(this->d_c_vertexmaps[i]);
	}
}

bool Tracker::estimatePose(float* d_depthmap) {

	if (0 == d_depthmap) {
		return false;
	}

	GPU::BilateralFiltering::bilateralFilterGray(
			this->d_depthmaps[NB_LEVELS - 1], d_depthmap, radius,
			space_coefficient, range_coefficient, this->width, this->height,
			block_x, block_y);

	// Downsampling
	int w = this->width;
	int h = this->height;
	for (int i = NB_LEVELS - 1; i > 0; --i) {
		DownSample::apply(this->d_depthmaps[i - 1], this->d_depthmaps[i],
				w, h, block_x, block_y);
		w /= 2;
		h /= 2;
	}

	w = this->width;
	h = this->height;
	for (int i = NB_LEVELS - 1; i >= 0; --i) {
		BackProject::apply(this->d_c_vertexmaps[i], this->d_depthmaps[i],
				intrinsic[i], w, h, block_x, block_y);
		NormalEstimation::applySimple(this->d_c_normalmaps[i],
				this->d_c_vertexmaps[i], w, h, block_x,
				block_y);
		w /= 2;
		h /= 2;
	}

/*	cv::Mat tmp = cv::Mat(120,160,CV_32FC3);
	cudaMemcpy(tmp.data, this->d_c_normalmaps[0],
					160 * 120 * 3 * sizeof(float), cudaMemcpyDeviceToHost);
	cv::imshow("test",tmp);*/

	float *rotation_inc_ptr = (float*) this->rotation_inc.data;
	float *translation_inc_ptr = (float*) this->translation_inc.data;

	this->rotation = cv::Mat::eye(3, 3, CV_32F);
	this->translation = cv::Mat::zeros(3, 1, CV_32F);

	if (false == this->firstframe) {

		cv::Mat Amat = cv::Mat(SIZE_VEC_B, SIZE_VEC_B, CV_32F);
		cv::Mat bmat = cv::Mat(SIZE_VEC_B, 1, CV_32F);

		float *A = (float*) Amat.data;
		float *b = (float*) bmat.data;

		w = this->width/4;
		h = this->height/4;

		bool stop = false;

		for (unsigned int i = 0; i < NB_LEVELS && false == stop; ++i) {


			for (unsigned int j = 0; j < iterations[i] && false == stop; ++j) {

				cudaMemcpy(d_rotation, rotation.data, 9 * sizeof(float),
						cudaMemcpyHostToDevice);
				cudaMemcpy(d_translation, translation.data, 3 * sizeof(float),
						cudaMemcpyHostToDevice);

				Calibration3D3D::computeSystem(A, b, this->d_p_vertexmaps[i],
						this->d_p_normalmaps[i], this->d_c_vertexmaps[i],
						this->d_c_normalmaps[i], d_rotation, d_translation,
						d_intrinsic[i], w, h, block_x,
						block_y);

				cv::solve(Amat, bmat, this->res, cv::DECOMP_CHOLESKY);

				std::cout << Amat << bmat << std::endl;

				float* res_array = (float*) this->res.data;
				rotation_inc_ptr[0] = 1.0f;
				rotation_inc_ptr[1] = res_array[2];
				rotation_inc_ptr[2] = -res_array[1];
				rotation_inc_ptr[3] = -res_array[2];
				rotation_inc_ptr[4] = 1.0f;
				rotation_inc_ptr[5] = res_array[0];
				rotation_inc_ptr[6] = res_array[1];
				rotation_inc_ptr[7] = -res_array[0];
				rotation_inc_ptr[8] = 1.0f;
				translation_inc_ptr[0] = res_array[3];
				translation_inc_ptr[1] = res_array[4];
				translation_inc_ptr[2] = res_array[5];

				translation = rotation_inc * translation + translation_inc;
				rotation = rotation_inc * rotation;

				std::cout << translation << std::endl;
				std::cout << rotation << std::endl;

				if(res_array[0] < 0.0005 && res_array[1] < 0.0005 && res_array[2] < 0.0005 &&
						res_array[3] < 0.0005 && res_array[4] < 0.0005 && res_array[5] < 0.0005) {
					stop = true;
				}

			}
			std::cout << "------------------------" << std::endl;
			w*=2;
			h*=2;
		}
		std::cout << "*******************************" << std::endl;
	}
	w = this->width;
	h = this->height;
	for (int i = NB_LEVELS - 1; i >= 0; --i) {
		cudaMemcpy(this->d_p_vertexmaps[i], this->d_c_vertexmaps[i],
				w * h * 3 * sizeof(float), cudaMemcpyDeviceToDevice);
		cudaMemcpy(this->d_p_normalmaps[i], this->d_c_normalmaps[i],
				w * h * 3 * sizeof(float), cudaMemcpyDeviceToDevice);
		w /= 2;
		h /= 2;
	}
	this->firstframe = false;

	std::cout << std::endl;

	return true;
}



















namespace Calibration3D3D {

bool performICP(float* output_rotation, float* output_translation, float* d_previousvertexmap,
		float* d_previousnormalmap, float* d_inputvertexmap, float* d_inputnormalmap,
		float *intrinsic, const int& width, const int& height,
		const unsigned int& block_x, const unsigned int& block_y) {

	if (0 == output_rotation || 0 == output_translation
			|| 0 == d_previousvertexmap || 0 == d_previousnormalmap
			|| 0 == d_inputvertexmap || 0 == d_inputnormalmap) {
		Log::add().error("Calibration3D3D::performICP", "Invalid arguments");
		return false;
	}

	cv::Mat rotation = cv::Mat::eye(3,3,CV_32F);
	cv::Mat translation = cv::Mat::zeros(3,1,CV_32F);

	cv::Mat rotation_inc = cv::Mat::eye(3,3,CV_32F);
	cv::Mat translation_inc = cv::Mat::zeros(3,1,CV_32F);

	float *rotation_inc_ptr = (float*) rotation_inc.data;
	float *translation_inc_ptr = (float*) translation_inc.data;

	float *d_rotation = 0;
	float *d_translation = 0;
	float *d_intrinsic = 0;

	cv::Mat res;
	cv::Mat Amat = cv::Mat(SIZE_VEC_B, SIZE_VEC_B, CV_32F);
	cv::Mat bmat = cv::Mat(SIZE_VEC_B, 1, CV_32F);

	float *A = (float*) Amat.data;
	float *b = (float*) bmat.data;

	cudaMalloc((void**) &(d_rotation), 9 * sizeof(float));
	cudaMalloc((void**) &(d_translation), 3 * sizeof(float));
	cudaMalloc((void**) &(d_intrinsic), 9 * sizeof(float));

	unsigned int iters = iterations[0];

	bool stop = false;

	for(unsigned int i = 0; i < iters && false == stop; ++i) {

		cudaMemcpy(d_rotation, rotation.data, 9 * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(d_translation, translation.data, 3 * sizeof(float),
				cudaMemcpyHostToDevice);
		cudaMemcpy(d_intrinsic, intrinsic, 9 * sizeof(float),
				cudaMemcpyHostToDevice);

		computeSystem(A, b, d_previousvertexmap, d_previousnormalmap,
				d_inputvertexmap, d_inputnormalmap, d_rotation,
				d_translation, d_intrinsic, width, height, block_x, block_y);

		cv::solve(Amat, bmat, res, cv::DECOMP_CHOLESKY);

		float* res_array = (float*) res.data;

		if(res_array[0] < 0.001 && res_array[1] < 0.001 && res_array[2] < 0.001 &&
				res_array[3] < 0.001 && res_array[4] < 0.001 && res_array[5] < 0.001) {
			stop = true;
		}

		rotation_inc_ptr[1] = res_array[2];
		rotation_inc_ptr[2] = -res_array[1];
		rotation_inc_ptr[3] = -res_array[2];
		rotation_inc_ptr[5] = res_array[0];
		rotation_inc_ptr[6] = res_array[1];
		rotation_inc_ptr[7] = -res_array[0];
		translation_inc_ptr[0] = res_array[3];
		translation_inc_ptr[1] = res_array[4];
		translation_inc_ptr[2] = res_array[5];

		translation = rotation_inc*translation+translation_inc;
		rotation = rotation_inc*rotation;

	}

	memcpy(output_rotation, rotation.data, 9 * sizeof(float));
	memcpy(output_translation, translation.data, 3 * sizeof(float));

	return true;
}

}

}

}

