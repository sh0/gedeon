/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * intrinsic.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * intrinsic.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__INTRINSIC_HPP__
#define GEDEON_CALIBRATION__INTRINSIC_HPP__

#include <gedeon/datatypes/rgbimage.hpp>

#include <opencv2/core/core.hpp>

namespace gedeon {

namespace Intrinsic {

/**
 * \brief Compute the intrinsic parameters of a camera
 *
 * \param intrinsic The matrix that will be filled with the intrinsic parameters
 * \param image multiple sets of image points (2D)
 * \param object multiple sets of object points in 3D space corresponding to the image points
 * \param image_size The size of the image captured by the camera
 * \param fixed_principal_point Set the principal point at the center of the image or not
 *
 * \return True if the calibration was successful, otherwise false
 *
 */
bool calibrate(cv::Mat& intrinsic,
		const std::vector<std::vector<cv::Point2f> >& image,
		const std::vector<std::vector<cv::Point3f> >& object,
		const Size& image_size, const bool& fixed_principal_point);

/**
 * \brief Compute the intrinsic parameters of a camera with the distortion coefficients
 *
 * \param intrinsic The matrix that will be filled with the intrinsic parameters
 * \param distortion The matrix that will be filled with the distortion coefficients
 * \param image multiple sets of image points (2D)
 * \param object multiple sets of object points in 3D space corresponding to the image points
 * \param image_size The size of the image captured by the camera
 * \param fixed_principal_point Set the principal point at the center of the image or not
 *
 * \return True if the calibration was successful, otherwise false
 *
 */
bool calibrateWithDistortion(cv::Mat& intrinsic, cv::Mat& distortion,
		const std::vector<std::vector<cv::Point2f> >& image,
		const std::vector<std::vector<cv::Point3f> >& object,
		const Size& image_size, const bool& fixed_principal_point);

}

}

#endif
