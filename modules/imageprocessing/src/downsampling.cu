/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * downsampling.cu created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * downsampling.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/imageprocessing/downsampling.cuh"
#include "gedeon/core/log.hpp"
#include "gedeon/core/core.cuh"

namespace gedeon {

namespace GPU {

namespace DownSample {

__device__ const float gaussiankernel[9] = {41.0/273.0,26.0/273.0,7.0/273.0,
										26.0/273.0,16.0/273.0,4.0/273.0,
										7.0/273.0,4.0/273.0,1.0/273.0};
__device__ const int m = 2;

__global__
void downSampleGaussianKernel(unsigned char *d_output, const unsigned char *d_input,
		const unsigned int width_div2, const unsigned int height_div2) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width_div2 && y < height_div2) {

		int2 center = make_int2(x*2,y*2);

		int2 minimum;
		minimum.x= max(0,center.x - m);
		minimum.y = max(0,center.y - m);
		int2 maximum;
		maximum.x= min(width_div2*2 - 1,center.x + m);
		maximum.y = min(height_div2*2 - 1,center.y + m);

		int sum = 0;
		for(int i = minimum.y; i <= maximum.y; ++i){
			for(int j = minimum.x; j <= maximum.x; ++j){
				int2 pos = make_int2(abs(center.x-j),abs(center.y-i));
				sum += (unsigned char)(gaussiankernel[pos.y*(m+1)+pos.x]*d_input[i*width_div2*2 + j]);
			}
		}
		d_output[y*width_div2+x] = sum;
	}
}


__global__
void downSampleGaussianKernel(float *d_output, const float *d_input,
		const unsigned int width_div2, const unsigned int height_div2) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width_div2 && y < height_div2) {

		int2 center = make_int2(x*2,y*2);

		int2 minimum;
		minimum.x= max(0,center.x - m);
		minimum.y = max(0,center.y - m);
		int2 maximum;
		maximum.x= min(width_div2*2 - 1,center.x + m);
		maximum.y = min(height_div2*2 - 1,center.y + m);

		float sum = 0;
		for(int i = minimum.y; i <= maximum.y; ++i){
			for(int j = minimum.x; j <= maximum.x; ++j){
				int2 pos = make_int2(abs(center.x-j),abs(center.y-i));
				sum += gaussiankernel[pos.y*(m+1)+pos.x]*d_input[i*width_div2*2 + j];
			}
		}
		d_output[y*width_div2+x] = sum;
	}
}


bool apply(float *d_output, const float *d_input,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y){
	if (0 == d_output || 0 == d_input){
		Log::add().error("DownSample::apply","Input arrays are not valid");
		return false;
	}

	if(0 != d_width%2 || 0 != d_height%2){
		Log::add().error("DownSample::apply","Size should be a multiple of 2");
		return false;
	}

	unsigned int d_width_div2 = d_width/2;
	unsigned int d_height_div2 = d_height/2;
	dim3 blockSize(block_x, block_y);
	dim3 gridSize((d_width_div2 + blockSize.x - 1) / blockSize.x,(d_height_div2 + blockSize.y - 1) / blockSize.y);
	downSampleGaussianKernel<<< gridSize, blockSize>>>(d_output, d_input, d_width_div2, d_height_div2);
	return true;
}


}

}

}

