/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthsensedriver.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthsensedriver.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRABBER__DEPTHSENSEDRIVER_HPP__
#define GEDEON_GRABBER__DEPTHSENSEDRIVER_HPP__

#include "gedeon/grabber/driver.hpp"
#include <DepthSense.hxx>
#include <boost/thread.hpp>

namespace gedeon {

/**
 * \brief Class for finding the available DepthSense cameras
 *
 * \author Francois de Sorbier
 */
class DepthSenseDriver: public Driver {

private:

	/**
	 * \brief Constructor
	 *
	 */
	DepthSenseDriver(void);

	/**
	 * \brief Copy constructor
	 * \param dsd the object to use for construction
	 *
	 */
	DepthSenseDriver(const DepthSenseDriver& dsd){}

	/**
	 * \brief Destructor
	 *
	 */
	~DepthSenseDriver(void);

	/**
	 * \brief Copy operator
	 * \param dsd the object to copy
	 * \return this updated object
	 *
	 */
	DepthSenseDriver& operator=(const DepthSenseDriver& dsd){ return *this;}

public:

	/**
	 * \brief Return a unique instance of this object
	 *
	 * \return An unique instance of this object
	 *
	 */
	static inline DepthSenseDriver& getInstance(void) {
		return driver_singleton;
	}

	/**
	 * \brief Ask to scan the network interface for finding the available cameras
	 *
	 * \return true or false if something went wrong or not
	 *
	 */
	bool refresh(void);

	/**
	 * \brief Give the name of the driver
	 *
	 * \return the name of the driver
	 *
	 */
	std::string getName(void) const {
		return std::string("DepthSense");
	}

	/**
	 * \brief Return the list of available grabbers
	 *
	 * \return Return a list of grabber names defined by
	 *
	 */
	std::vector<std::string> populate(void) const {
		if(false == this->initialized){
			Log::add().warning("DepthSenseDriver::populate","The driver has not been initialized yet");
		}
		return this->names;
	}

	/**
	 * \brief Return the number of available grabbers
	 *
	 * \return Return the number of available grabbers
	 *
	 */
	unsigned int getCount(void) const {
		if(false == this->initialized){
			Log::add().warning("DepthSenseDriver::getCount","The driver has not been initialized yet");
		}
		return this->names.size();
	}

	DepthSense::Context getContext(void) const{
		if(false == this->initialized){
			Log::add().warning("DepthSenseDriver::getContext","The driver has not been initialized yet");
		}
		return this->g_context;
	}

private:
	void run(void);

protected:

	std::vector<std::string> names;
	bool initialized;
	DepthSense::Context g_context;
	boost::thread *pthread;

	static DepthSenseDriver driver_singleton;
};

}

#endif
