/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * normalestimation.cu created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * normalestimation.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/pointcloudprocessing/normalestimation.cuh"

#include "gedeon/core/log.hpp"
#include "gedeon/core/core.cuh"

#include <cutil.h>
#include <cutil_math.h>

namespace gedeon {

namespace GPU {

namespace NormalEstimation {

__global__ void estimateNormalsSimpleKernel(float *d_output_normals, const float *d_input_pointcloud,
		 const unsigned int w, const unsigned int h) {

	const int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	const int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	const int pos = (y * w + x) * 3;
	const int stride = w * 3;

	if (x < (w - 1) && y < (h - 1)) {

		float3 p = make_float3(d_input_pointcloud[pos],
				d_input_pointcloud[pos + 1], d_input_pointcloud[pos + 2]);

		float3 vv, hv;
		hv.x = p.x - d_input_pointcloud[pos + 3];
		hv.y = p.y - d_input_pointcloud[pos + 4];
		hv.z = p.z - d_input_pointcloud[pos + 5];
		vv.x = p.x - d_input_pointcloud[pos + stride];
		vv.y = p.y - d_input_pointcloud[pos + stride + 1];
		vv.z = p.z - d_input_pointcloud[pos + stride + 2];

		if (true == isnan(hv.x) || true == isinf(hv.x) ||
			true == isnan(vv.x) || true == isinf(vv.x)) {
			d_output_normals[pos] = quiet_NaN();
			d_output_normals[pos + 1] = quiet_NaN();
			d_output_normals[pos + 2] = quiet_NaN();
		} else {
			float3 cross_p = normalize(cross(vv, hv));
			d_output_normals[pos] = cross_p.x;
			d_output_normals[pos + 1] = cross_p.y;
			d_output_normals[pos + 2] = cross_p.z;
		}
	}else{
		if(x < w  || y < h){
			d_output_normals[pos] = quiet_NaN();
			d_output_normals[pos + 1] = quiet_NaN();
			d_output_normals[pos + 2] = quiet_NaN();
		}
	}

}

bool applySimple(float *d_output_normals, float *d_input_pointcloud,
		const unsigned int& width, const unsigned int& height,
		const unsigned int& block_x, const unsigned int& block_y) {

	dim3 blockSize(block_x, block_y);
	dim3 gridSize((width + blockSize.x - 1) / blockSize.x,
			(height + blockSize.y - 1) / blockSize.y);
	estimateNormalsSimpleKernel<<< gridSize, blockSize>>>( d_output_normals, d_input_pointcloud, width, height);

	return true;
}

__global__ void estimateNormalsModerateKernel(const float *d_input_pointcloud,
		float *d_output_normals, const unsigned int w, const unsigned int h) {

	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < w && x >= 0 && y < h && y >= 0) {
		bool correct = true;
		float3 vv, hv;

		float3 p = make_float3(d_input_pointcloud[(y * w + x) * 3],
				d_input_pointcloud[(y * w + x) * 3 + 1],
				d_input_pointcloud[(y * w + x) * 3 + 2]);

		if (true == isnan(p.x) || (x == (w - 1) && true == isnan(
				d_input_pointcloud[(y * w + x - 1) * 3]))
				|| (x == 0 && true == isnan(
						d_input_pointcloud[(y * w + x + 1) * 3]))
				|| (y == (h - 1) && true == isnan(
						d_input_pointcloud[((y - 1) * w + x) * 3]))
				|| (y == 0 && true == isnan(
						d_input_pointcloud[((y + 1) * w + x) * 3]))
				|| (true == isnan(d_input_pointcloud[(y * w + x + 1) * 3])
						&& true == isnan(
								d_input_pointcloud[(y * w + x - 1) * 3]))
				|| (true == isnan(d_input_pointcloud[((y + 1) * w + x) * 3])
						&& true == isnan(
								d_input_pointcloud[((y - 1) * w + x) * 3]))) {
			correct = false;
		}

		if (true == correct) {
			if (false == isnan(d_input_pointcloud[(y * w + x + 1) * 3])) {
				hv.x = p.x - d_input_pointcloud[(y * w + x + 1) * 3];
				hv.y = p.y - d_input_pointcloud[(y * w + x + 1) * 3 + 1];
				hv.z = p.z - d_input_pointcloud[(y * w + x + 1) * 3 + 2];
			} else {
				hv.x = d_input_pointcloud[(y * w + x - 1) * 3] - p.x;
				hv.y = d_input_pointcloud[(y * w + x - 1) * 3 + 1] - p.y;
				hv.z = d_input_pointcloud[(y * w + x - 1) * 3 + 2] - p.z;
			}

			if (false == isnan(d_input_pointcloud[((y + 1) * w + x) * 3])) {
				vv.x = p.x - d_input_pointcloud[((y + 1) * w + x) * 3];
				vv.y = p.y - d_input_pointcloud[((y + 1) * w + x) * 3 + 1];
				vv.z = p.z - d_input_pointcloud[((y + 1) * w + x) * 3 + 2];
			} else {
				vv.x = d_input_pointcloud[((y - 1) * w + x) * 3] - p.x;
				vv.y = d_input_pointcloud[((y - 1) * w + x) * 3 + 1] - p.y;
				vv.z = d_input_pointcloud[((y - 1) * w + x) * 3 + 2] - p.z;
			}

			float3 cross_p = normalize(cross(vv, hv));

			d_output_normals[(y * w + x) * 3] = cross_p.x;
			d_output_normals[(y * w + x) * 3 + 1] = cross_p.y;
			d_output_normals[(y * w + x) * 3 + 2] = cross_p.z;

		} else {
			d_output_normals[(y * w + x) * 3] = quiet_NaN();
			d_output_normals[(y * w + x) * 3 + 1] = quiet_NaN();
			d_output_normals[(y * w + x) * 3 + 2] = quiet_NaN();
		}
	}
}

bool applyModerate(float *d_output_normals, float *d_input_pointcloud,
		const unsigned int& width, const unsigned int& height,
		const unsigned int& block_x, const unsigned int& block_y) {

	dim3 blockSize(block_x, block_y);
	dim3 gridSize((width + blockSize.x - 1) / blockSize.x,
			(height + blockSize.y - 1) / blockSize.y);
	estimateNormalsModerateKernel<<< gridSize, blockSize>>>(d_input_pointcloud, d_output_normals, width, height);

	return true;
}

}

}

}

