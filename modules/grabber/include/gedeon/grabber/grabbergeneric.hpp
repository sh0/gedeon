/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * grabbergeneric.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * grabbergeneric.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_GRABBER__GRABBERGENERIC_HPP__
#define GEDEON_GRABBER__GRABBERGENERIC_HPP__

#include "gedeon/grabber/driver.hpp"
#include "gedeon/grabber/enums.hpp"

#include "gedeon/core/core.hpp"
#include "gedeon/datatypes/rgbdimage.hpp"


namespace gedeon{

/**
 * \brief Abstract class for defining a grabber
 *
 * \author Francois de Sorbier
 */
class Grabber{

public:

	/**
	 * \brief Initialize the grabber
	 *
	 * \param id the id of the device based on its position in the list returned by the driver
	 * \param driver the driver corresponding to the grabber
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	virtual bool init(const unsigned int & id, Driver* driver) = 0;

	/**
	 * \brief Initialize the grabber with a file or directory
	 *
	 * \param source the name of a valid file or directory
	 * \param driver the driver corresponding to the grabber
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	virtual bool init(const std::string& source, Driver* driver) = 0;

	/**
	 * \brief Destructor of the class
	 */
	virtual ~Grabber(void) {}

	/**
	 * \brief Return the name of the grabber
	 *
	 * \return the name of the grabber
	 */
	virtual std::string getName(void) const = 0;

	/**
	 * \brief Return the id of the grabber referencing its position in the driver's list
	 *
	 * \return the id
	 */
	virtual unsigned int getID(void) const = 0;

	/**
	 * \brief Return the rgbdi image captured by the device
	 *
	 * \return The RGBDI image
	 */
	virtual const RGBDIImagePtr& getImage(void) const = 0;

	/**
	 * \brief Return the value related to one parameter of the device
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] value is set with the value of the parameter
	 *
	 * \sa GrabberParameter
	 * \return true if the parameter is found, false otherwise
	 */
	virtual bool getParameter(const GrabberParameter& param, float& value) = 0;

	/**
	 * \brief Return the value related to several parameters of the device
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] values is set with the values of the parameter. The array has to be allocated beforehand.
	 * \param[out] size size of the array
	 *
	 * \sa GrabberParameter
	 * \return true if the parameter is found, false otherwise
	 */
	virtual bool getParameter(const GrabberParameter& param, float* values, unsigned int& size) = 0;

	/**
	 * \brief Set a parameter of the device with a given value
	 *
	 * \param param defines the parameter we want to set with the value
	 * \param value used for setting the parameter
	 *
	 * \sa GrabberParameter
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	virtual bool setParameter(const GrabberParameter& param, const float& value) = 0;

	/**
	 * \brief Trigger a functionality of the device
	 *
	 * \sa GrabberTrigger
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	virtual bool trigger(const GrabberTrigger& trig) = 0;

	/**
	 * \brief Return if the capture is currently active or not
	 *
	 * \return true if capture is active, false otherwise
	 */
	virtual bool isGrabbing(void) const = 0;

	/**
	 * \brief Return if the grabber has a color image
	 *
	 * \return true if there is one, false otherwise
	 */
	virtual bool hasColor(void) const = 0;

	/**
	 * \brief Return if the grabber has a intensity image
	 *
	 * \return true if there is one, false otherwise
	 */
	virtual bool hasIntensity(void) const = 0;

	/**
	 * \brief Return if the grabber has depth data
	 *
	 * \return true if there is, false otherwise
	 */
	virtual bool hasDepth(void) const = 0;

public:

	/**
	 * \brief Pause the capture.
	 *
	 * The device is still active.
	 * the function can be called again to unpause the capture.
	 */
	virtual void pause(void) = 0;

	/**
	 * \brief Start the capture.
	 *
	 * The function can also unpause the device.
	 */
	virtual void play(void) = 0;

	/**
	 * \brief Stop the capture or the recording.
	 *
	 * The device is no more active.
	 */
	virtual void stop(void) = 0;

	/**
	 * \brief Grab one single image
	 *
	 * This functionality is only working with recorded data and may not be always available
	 *
	 * While grabbing one single image any currently playing is stopped
	 *
	 */
	virtual void grabOneFrame(void) = 0;

	/**
	 * \brief set the stream filename for reading or recording
	 *
	 * \param filename The name of the recording/reading file
	 */
	virtual void setStreamFilename(const std::string& filename) = 0;

	/**
	 * \brief get the stream filename for reading or recording
	 *
	 * \return the current filename
	 */
	virtual std::string getStreamFilename(void) const = 0;

};


/**
 * \brief Class for holding event identified by a name and a grabber
 *
 * The class can also have a smart pointer to the sending class if there is one
 *
 * \author Francois de Sorbier
 */
class GrabberEventData: public EventData{

public:

	/**
	 * \brief Constructor of the class
	 *
	 * With inheritance, it is possible to add extra data that can be processed by the receiver
	 *
	 * \param g The type of the grabber defined in the enumeration Grabbers
	 * \param evname The name of the event
	 *
	 * \sa Grabbers
	 *
	 */
	GrabberEventData(const Grabbers& g, const std::string& evname = std::string("unknown")):EventData(evname), grabber(g){}

	/**
	 * \brief Constructor of the class
	 *
	 * With inheritance, it is possible to add extra data that can be processed by the receiver
	 *
	 * \param g The type of the grabber defined in the enumeration Grabbers
	 * \param evname The name of the event
	 * \param evsender A pointer one the class sending the event
	 *
	 * \sa Grabbers
	 *
	 */
	GrabberEventData(const Grabbers& g, const std::string& evname, EventSender* evsender):EventData(evname,evsender), grabber(g){}

	/**
	 * \brief Destructor of the class
	 *
	 */
	~GrabberEventData(){}

	Grabbers grabber;
};

typedef boost::shared_ptr<GrabberEventData> GrabberEventDataPtr;
typedef boost::weak_ptr<GrabberEventData> GrabberEventDataWPtr;





}


#endif
