/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * cudaio.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * cudaio.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/
#ifndef GEDEON_CORE__CUDAIO_HPP__
#define GEDEON_CORE__CUDAIO_HPP__

#include "gedeon/core/log.hpp"

#include <cuda_runtime.h>
#include <cuda.h>

namespace gedeon {

namespace GPU {

static inline bool checkStatus(const cudaError& err) {
	if (cudaSuccess != err) {
		Log::add().error("GPU::checkStatus", cudaGetErrorString(err));
		return false;
	}
	return true;
}

}

}

#endif
